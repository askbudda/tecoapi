﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TecoRFC.Startup))]
namespace TecoRFC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
