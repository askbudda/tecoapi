﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SAP.Middleware.Connector;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Http;

namespace TecoRFC.Controllers
{
    #region Controller

    public class BASEController : ApiController
    {
        [HttpGet]
        public string Connection()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_KNA1_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KNA1");
                //string companyName = detail.GetString("KUNNR");
                //return data.GetElementMetadata(0);
                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                        // Console.WriteLine("{0} is {1}", rfcEMD.Documentation, dr[rfcEMD.Name]);
                    }

                    dtRet.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet, "C:\\LogFiles\\connection.csv");

                return $"總筆數:{dtRet.Rows.Count} {dtRet.Rows[0]["KUNNR"].ToString()} {dtRet.Rows[0]["NAME1"].ToString()}";

            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpGet]
        public string Connection2()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_MARA_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_MARA");
                //string companyName = detail.GetString("KUNNR");
                //return data.GetElementMetadata(0);
                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                        // Console.WriteLine("{0} is {1}", rfcEMD.Documentation, dr[rfcEMD.Name]);
                    }

                    dtRet.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet, "C:\\LogFiles\\connection2.csv");

                return $"總筆數:{dtRet.Rows.Count} {dtRet.Rows[0]["MAKTX"].ToString()}";

            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpGet]
        public string Connection3()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_CREATE_ORDER_HA");

                companyBapi.SetValue("ZAUART", "OR");
                companyBapi.SetValue("ZVKORG", "R000");
                companyBapi.SetValue("ZVTWEG", "18");
                companyBapi.SetValue("ZSPART", "24");
                companyBapi.SetValue("ZKUNNR", "D01189353");
                companyBapi.SetValue("ZKUNWE", "D01189353");
                companyBapi.SetValue("ZBSTKD", "");
                companyBapi.SetValue("ZCITY1", "");
                companyBapi.SetValue("ZSTREET", "");
                companyBapi.SetValue("ZTELF1", "");
                companyBapi.SetValue("ZTEL_NUMBER", "");
                companyBapi.SetValue("ZANRED", "");
                companyBapi.SetValue("ZMARK", " 20200907 送貨");
                companyBapi.Invoke(prd);

                //建立 IRfcTable 結構
                IRfcTable Itab = companyBapi.GetTable("ZITEMREC");  // 取得 SAP 中定義的表格結構

                //5.設定 IRfcTalbe 參數
                Itab.Append();	// 加入一筆record 到表格中
                Itab[0].SetValue("POSNR", "10");
                Itab[0].SetValue("MATNR", "MA-GS36FC");
                Itab[0].SetValue("WERKS", "15R0");
                Itab[0].SetValue("LGORT", "K500");
                Itab[0].SetValue("KWMENG", "1");
                Itab[0].SetValue("CO_NO", "202009071423130901");
                Itab[0].SetValue("CO_SEQ", "40");

                Itab.Append();	// 加入一筆record 到表格中
                Itab[1].SetValue("POSNR", "20");
                Itab[1].SetValue("MATNR", "MS-GS36FC");
                Itab[1].SetValue("WERKS", "15R0");
                Itab[1].SetValue("LGORT", "K500");
                Itab[1].SetValue("KWMENG", "1");
                Itab[1].SetValue("CO_NO", "202009071423130901");
                Itab[1].SetValue("CO_SEQ", "50");

                //6.呼叫 SAP RFC 返回值
                companyBapi.SetValue("ZITEMREC", Itab);
                companyBapi.Invoke(prd);

                //7.取得 SAP RFC 返回值(成功)
                string Success = companyBapi.GetValue(15).ToString().Replace(" ", ";");
                var SuccessList = Success.Split(';');

                string CO_NO = "";
                string VBELN = "";
                string VBELN_L = "";
                string MBLNR = "";
                string VBELN_F = "";
                foreach (var SuccessItem in SuccessList)
                {
                    if (SuccessItem.Contains("CO_NO="))
                    {
                        CO_NO = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                    }
                    else if (SuccessItem.Contains("VBELN="))
                    {
                        VBELN = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                    }
                    else if (SuccessItem.Contains("VBELN_L="))
                    {
                        VBELN_L = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                    }
                    else if (SuccessItem.Contains("MBLNR="))
                    {
                        MBLNR = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                    }
                    else if (SuccessItem.Contains("VBELN_F="))
                    {
                        VBELN_F = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                    }
                }

                //7.取得 SAP RFC 返回值(失敗)
                IRfcTable Fail = companyBapi.GetTable("ZRETURN");
                DataTable dtRet3 = new DataTable();

                for (int liElement = 0; liElement < Fail.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = Fail.GetElementMetadata(liElement);
                    dtRet3.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in Fail)
                {
                    DataRow dr = dtRet3.NewRow();

                    for (int liElement = 0; liElement < Fail.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = Fail.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                        // Console.WriteLine("{0} is {1}", rfcEMD.Documentation, dr[rfcEMD.Name]);
                    }

                    dtRet3.Rows.Add(dr);
                }

                //billy test 轉成 csv
                SaveToCSV(dtRet3, "C:\\LogFiles\\connection3.csv");

                return $"{(VBELN_L != "" ? "Success" : "Fail")} - CO_NO:{CO_NO} , VBELN:{VBELN} , VBELN_L:{VBELN_L} , MBLNR:{MBLNR} , VBELN_F:{VBELN_F} , List:{Success}";
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpGet]
        public string Connection4()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_CUST_PRICE_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.SetValue("ZVKORG", "R000");
                companyBapi.SetValue("ZVTWEG", "01");
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KONP");
                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet, "C:\\LogFiles\\connection4_R000_01.csv");

                //第二次
                //IRfcFunction companyBapi2 = repo.CreateFunction("Z_RFC_GET_CUST_PRICE_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.SetValue("ZVKORG", "R000");
                companyBapi.SetValue("ZVTWEG", "18");
                companyBapi.Invoke(prd);
                IRfcTable data2 = companyBapi.GetTable("O_KONP");
                DataTable dtRet2 = new DataTable();

                for (int liElement = 0; liElement < data2.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data2.GetElementMetadata(liElement);
                    dtRet2.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data2)
                {
                    DataRow dr = dtRet2.NewRow();

                    for (int liElement = 0; liElement < data2.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data2.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet2.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet2, "C:\\LogFiles\\connection4_R000_18.csv");

                //第三次
                //IRfcFunction companyBapi3 = repo.CreateFunction("Z_RFC_GET_CUST_PRICE_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.SetValue("ZVKORG", "K000");
                companyBapi.SetValue("ZVTWEG", "01");
                companyBapi.Invoke(prd);
                IRfcTable data3 = companyBapi.GetTable("O_KONP");
                DataTable dtRet3 = new DataTable();

                for (int liElement = 0; liElement < data3.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data3.GetElementMetadata(liElement);
                    dtRet3.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data3)
                {
                    DataRow dr = dtRet3.NewRow();

                    for (int liElement = 0; liElement < data3.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data3.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet3.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet3, "C:\\LogFiles\\connection4_K000_01.csv");

                return $"總筆數:{dtRet.Rows.Count} {dtRet2.Rows.Count} {dtRet3.Rows.Count}";

            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpGet]
        public string Connection5()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_CUST_CREDIT_HA");
                companyBapi.SetValue("ALL", "x");
                companyBapi.SetValue("ZKKBER", "K000");
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KNKK");
                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                        // Console.WriteLine("{0} is {1}", rfcEMD.Documentation, dr[rfcEMD.Name]);
                    }

                    dtRet.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet, "C:\\LogFiles\\connection5_K000.csv");

                return $"總筆數:{dtRet.Rows.Count}";

            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpGet]
        public string Connection6()
        {
            MyBackendConfig myConfig = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);
            RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");

            try
            {
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_MARD_HA");

                IRfcTable Itab = companyBapi.GetTable("ZSTOCK");    // 取得 SAP 中定義的表格結構

                //5.設定 IRfcTalbe 參數
                Itab.Append();	// 加入一筆record 到表格中
                Itab[0].SetValue("WERKS", "15R0");
                Itab[0].SetValue("LGORT", "K500");
                Itab[0].SetValue("MATNR", "MA22IC-HS");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[1].SetValue("WERKS", "15R0");
                Itab[1].SetValue("LGORT", "K500");
                Itab[1].SetValue("MATNR", "MA22IH-HS");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[2].SetValue("WERKS", "15R0");
                Itab[2].SetValue("LGORT", "K500");
                Itab[2].SetValue("MATNR", "MS22IE-HS");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[3].SetValue("WERKS", "15R0");
                Itab[3].SetValue("LGORT", "K500");
                Itab[3].SetValue("MATNR", "MA22IC-GA");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[4].SetValue("WERKS", "15R0");
                Itab[4].SetValue("LGORT", "K500");
                Itab[4].SetValue("MATNR", "MS22IC-GA");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[5].SetValue("WERKS", "15R0");
                Itab[5].SetValue("LGORT", "K500");
                Itab[5].SetValue("MATNR", "MA90IC-HP");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[6].SetValue("WERKS", "15R0");
                Itab[6].SetValue("LGORT", "K500");
                Itab[6].SetValue("MATNR", "MW22ICR-HS");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[7].SetValue("WERKS", "15R0");
                Itab[7].SetValue("LGORT", "K500");
                Itab[7].SetValue("MATNR", "MW20FR2");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[8].SetValue("WERKS", "15R0");
                Itab[8].SetValue("LGORT", "K500");
                Itab[8].SetValue("MATNR", "MA-GS22FC");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[9].SetValue("WERKS", "15R0");
                Itab[9].SetValue("LGORT", "K500");
                Itab[9].SetValue("MATNR", "MM2-K52BF");

                Itab.Append();	// 再加入一筆record 到表格中
                Itab[10].SetValue("WERKS", "15R0");
                Itab[10].SetValue("LGORT", "K500");
                Itab[10].SetValue("MATNR", "MS-K23B");

                //6.呼叫 SAP RFC 返回值
                companyBapi.SetValue("ZSTOCK", Itab);
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_MARD");
                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                        // Console.WriteLine("{0} is {1}", rfcEMD.Documentation, dr[rfcEMD.Name]);
                    }

                    dtRet.Rows.Add(dr);

                }

                //billy test 轉成 csv
                SaveToCSV(dtRet, "C:\\LogFiles\\connection6.csv");

                return $"總筆數:{dtRet.Rows.Count}";

            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        public void SaveToCSV(DataTable oTable, string FilePath)
        {
            string data = "";
            StreamWriter wr = new StreamWriter(FilePath, false, System.Text.Encoding.UTF8);
            foreach (DataColumn column in oTable.Columns)
            {
                data += column.ColumnName + ",";
            }
            data += "\n";
            wr.Write(data);
            data = "";

            foreach (DataRow row in oTable.Rows)
            {
                foreach (DataColumn column in oTable.Columns)
                {
                    data += row[column].ToString().Trim() + ",";
                }
                data += "\n";
                wr.Write(data);
                data = "";
            }
            data += "\n";

            wr.Dispose();
            wr.Close();
        }
    }

    #region KNA1(客戶)

    public class KNA1Controller : ApiController
    {
        [HttpPost]
        public Object Get([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_KNA1_APP_HA");
                companyBapi.SetValue("ALL", DATA["ALL"].ToString());
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KNA1");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                KNA1ResponseModel KNA1Response = new KNA1ResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    KNA1Response.data.Add(new KNA1Model
                    {
                        KUNNR = Row["KUNNR"].ToString(),
                        NAME1 = Row["NAME1"].ToString(),
                        VKORG = Row["VKORG"].ToString(),
                        VKBUR = Row["VKBUR"].ToString(),
                        VKGRP = Row["VKGRP"].ToString(),
                        KDGRP = Row["KDGRP"].ToString(),
                        BEZEI = Row["BEZEI"].ToString(),
                        TXNAM_SDB = Row["TXNAM_SDB"].ToString(),
                        ORT01 = Row["ORT01"].ToString(),
                        STRAS = Row["STRAS"].ToString()
                    });
                }
                KNA1Response.result = "1";
                KNA1Response.message = "Success";

                return KNA1Response;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }
    }

    #endregion KNA1(客戶)

    #region MARA(物料)

    public class MARAController : ApiController
    {
        [HttpPost]
        public Object Get([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_MARA_HA");
                companyBapi.SetValue("ALL", DATA["ALL"].ToString());
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_MARA");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                MARAResponseModel MARAResponse = new MARAResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    MARAResponse.data.Add(new MARAModel
                    {
                        MATNR = Row["MATNR"].ToString(),
                        MAKTX = Row["MAKTX"].ToString(),
                        MEINS = Row["MEINS"].ToString(),
                        MATKL = Row["MATKL"].ToString(),
                        PRODH = Row["PRODH"].ToString()
                    });
                }
                MARAResponse.result = "1";
                MARAResponse.message = "Success";

                return MARAResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }
    }

    #endregion MARA(物料)

    #region ORDER(訂單)

    public class ORDERController : ApiController
    {
        [HttpPost]
        public Object Add([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_CREATE_ORDER_HA");

                OrderRFCRequestModel OrderRFCRequest = JsonConvert.DeserializeObject<OrderRFCRequestModel>(JsonConvert.SerializeObject(DATA));

                companyBapi.SetValue("ZAUART", OrderRFCRequest.ZAUART);
                companyBapi.SetValue("ZVKORG", OrderRFCRequest.ZVKORG);
                companyBapi.SetValue("ZVTWEG", OrderRFCRequest.ZVTWEG);
                companyBapi.SetValue("ZSPART", OrderRFCRequest.ZSPART);
                companyBapi.SetValue("ZKUNNR", OrderRFCRequest.ZKUNNR);
                companyBapi.SetValue("ZKUNWE", OrderRFCRequest.ZKUNWE);
                companyBapi.SetValue("ZBSTKD", OrderRFCRequest.ZBSTKD);
                companyBapi.SetValue("ZCITY1", OrderRFCRequest.ZCITY1);
                companyBapi.SetValue("ZSTREET", OrderRFCRequest.ZSTREET);
                companyBapi.SetValue("ZTELF1", OrderRFCRequest.ZTELF1);
                companyBapi.SetValue("ZTEL_NUMBER", OrderRFCRequest.ZTEL_NUMBER);
                companyBapi.SetValue("ZANRED", OrderRFCRequest.ZANRED);
                companyBapi.SetValue("ZMARK", OrderRFCRequest.ZMARK);
                companyBapi.Invoke(prd);

                //建立 IRfcTable 結構
                IRfcTable Itab = companyBapi.GetTable("ZITEMREC");  // 取得 SAP 中定義的表格結構

                for (int i = 0; i < OrderRFCRequest.data.Count(); i++)
                {
                    Itab.Append();
                    Itab[i].SetValue("POSNR", OrderRFCRequest.data[i].POSNR);
                    Itab[i].SetValue("MATNR", OrderRFCRequest.data[i].MATNR);
                    Itab[i].SetValue("WERKS", OrderRFCRequest.data[i].WERKS);
                    Itab[i].SetValue("LGORT", OrderRFCRequest.data[i].LGORT);
                    Itab[i].SetValue("KWMENG", OrderRFCRequest.data[i].KWMENG);
                    Itab[i].SetValue("CO_NO", OrderRFCRequest.data[i].CO_NO);
                    Itab[i].SetValue("CO_SEQ", OrderRFCRequest.data[i].CO_SEQ);
                }

                companyBapi.SetValue("ZITEMREC", Itab);
                companyBapi.Invoke(prd);

                #region LOG-Request

                DataTable dtRetLOG = new DataTable();

                for (int liElement = 0; liElement < Itab.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = Itab.GetElementMetadata(liElement);
                    dtRetLOG.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in Itab)
                {
                    DataRow dr = dtRetLOG.NewRow();

                    for (int liElement = 0; liElement < Itab.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = Itab.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRetLOG.Rows.Add(dr);
                }

                SaveToCSV(dtRetLOG, "C:\\LogFiles\\ORDERController_ADD_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv", OrderRFCRequest);

                #endregion LOG-Request

                #region 成功資料

                //string Success = companyBapi.GetValue(15).ToString().Replace(" ", ";");
                //var SuccessList = Success.Split(';');

                //string CO_NO = "";
                //string VBELN = "";
                //string VBELN_L = "";
                //string MBLNR = "";
                //string VBELN_F = "";
                //foreach (var SuccessItem in SuccessList)
                //{
                //    if (SuccessItem.Contains("CO_NO="))
                //    {
                //        CO_NO = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                //    }
                //    else if (SuccessItem.Contains("VBELN="))
                //    {
                //        VBELN = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                //    }
                //    else if (SuccessItem.Contains("VBELN_L="))
                //    {
                //        VBELN_L = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                //    }
                //    else if (SuccessItem.Contains("MBLNR="))
                //    {
                //        MBLNR = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                //    }
                //    else if (SuccessItem.Contains("VBELN_F="))
                //    {
                //        VBELN_F = SuccessItem.Substring(SuccessItem.IndexOf("=")).Replace("=", "");
                //    }
                //}

                //string SALESCODE = VBELN;
                //string DELIVERYCODE = VBELN_L;
                //string POSTINGCODE = MBLNR;
                //string INVOICECODE = VBELN_F;

                IRfcTable dataS = companyBapi.GetTable("ZORDER");

                DataTable dtRetS = new DataTable();

                for (int liElement = 0; liElement < dataS.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = dataS.GetElementMetadata(liElement);
                    dtRetS.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in dataS)
                {
                    DataRow dr = dtRetS.NewRow();

                    for (int liElement = 0; liElement < dataS.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = dataS.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRetS.Rows.Add(dr);
                }

                #endregion 成功資料

                #region 失敗資料

                IRfcTable data = companyBapi.GetTable("ZRETURN");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                #endregion 失敗資料

                OrderResponseModel OrderResponse = new OrderResponseModel();

                string SALESCODE = "";
                string DELIVERYCODE = "";
                string POSTINGCODE = "";
                string INVOICECODE = "";

                foreach (DataRow Row in dtRetS.Rows) 
                {
                    SALESCODE = Row["VBELN"].ToString();
                    DELIVERYCODE = Row["VBELN_L"].ToString();
                    POSTINGCODE = Row["MBLNR"].ToString();
                    INVOICECODE = Row["VBELN_F"].ToString();
                }

                foreach (DataRow Row in dtRet.Rows)
                {
                    OrderResponse.data.Add(new OrderErrorItemModel
                    {
                        CO_NO = Row["CO_NO"].ToString(),
                        CO_SEQ = Row["CO_SEQ"].ToString(),
                        PRODUCTNO = Row["POSNR"].ToString(),
                        PRODUCTCODE = Row["MATNR"].ToString(),
                        PRODUCTQUANTITY = Row["KWMENG"].ToString(),
                        ERRORTYPE = Row["TYPE"].ToString(),
                        ERRORMESSAGE = Row["MESSAGE"].ToString(),
                        ERRORDATE = Row["DATUM"].ToString(),
                        ERRORTIME = Row["UZEIT"].ToString()
                    });
                }
                OrderResponse.SALESCODE = (dtRet.Rows.Count > 0 ? "" : SALESCODE);
                OrderResponse.DELIVERYCODE = (dtRet.Rows.Count > 0 ? "" : DELIVERYCODE);
                OrderResponse.POSTINGCODE = (dtRet.Rows.Count > 0 ? "" : POSTINGCODE);
                OrderResponse.INVOICECODE = (dtRet.Rows.Count > 0 ? "" : INVOICECODE);
                OrderResponse.result = "1";
                OrderResponse.message = "Success";

                return OrderResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        public void SaveToCSV(DataTable oTable, string FilePath, OrderRFCRequestModel OrderRFCRequest)
        {
            string data = "";
            StreamWriter wr = new StreamWriter(FilePath, false, System.Text.Encoding.UTF8);

            data += "ZAUART" + ",";
            data += "ZVKORG" + ",";
            data += "ZVTWEG" + ",";
            data += "ZSPART" + ",";
            data += "ZKUNNR" + ",";
            data += "ZKUNWE" + ",";
            data += "ZBSTKD" + ",";
            data += "ZCITY1" + ",";
            data += "ZSTREET" + ",";
            data += "ZTELF1" + ",";
            data += "ZTEL_NUMBER" + ",";
            data += "ZANRED" + ",";
            data += "ZMARK";
            data += "\n";
            wr.Write(data);
            data = "";

            data += OrderRFCRequest.ZAUART + ",";
            data += OrderRFCRequest.ZVKORG + ",";
            data += OrderRFCRequest.ZVTWEG + ",";
            data += OrderRFCRequest.ZSPART + ",";
            data += OrderRFCRequest.ZKUNNR + ",";
            data += OrderRFCRequest.ZKUNWE + ",";
            data += OrderRFCRequest.ZBSTKD + ",";
            data += OrderRFCRequest.ZCITY1 + ",";
            data += OrderRFCRequest.ZSTREET + ",";
            data += OrderRFCRequest.ZTELF1 + ",";
            data += OrderRFCRequest.ZTEL_NUMBER + ",";
            data += OrderRFCRequest.ZANRED + ",";
            data += OrderRFCRequest.ZMARK;
            data += "\n";
            wr.Write(data);
            data = "";

            foreach (DataColumn column in oTable.Columns)
            {
                data += column.ColumnName + ",";
            }
            data += "\n";
            wr.Write(data);
            data = "";

            foreach (DataRow row in oTable.Rows)
            {
                foreach (DataColumn column in oTable.Columns)
                {
                    data += row[column].ToString().Trim() + ",";
                }
                data += "\n";
                wr.Write(data);
                data = "";
            }
            data += "\n";

            wr.Dispose();
            wr.Close();
        }
    }

    #endregion ORDER(訂單)

    #region MARD(庫存) & Transfer(庫存轉移)

    public class MARDController : ApiController
    {
        [HttpPost]
        public Object Get([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_MARD_HA");
                IRfcTable Itab = companyBapi.GetTable("ZSTOCK");

                MARDRequestModel MARDRequest = JsonConvert.DeserializeObject<MARDRequestModel>(JsonConvert.SerializeObject(DATA));

                for (int i = 0; i < MARDRequest.data.Count(); i++) 
                {
                    Itab.Append();
                    Itab[i].SetValue("WERKS", MARDRequest.data[i].WERKS);
                    Itab[i].SetValue("LGORT", MARDRequest.data[i].LGORT);
                    Itab[i].SetValue("MATNR", MARDRequest.data[i].MATNR);
                }

                companyBapi.SetValue("ZSTOCK", Itab);
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_MARD");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                MARDResponseModel MARDResponse = new MARDResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    MARDResponse.data.Add(new MARDModel
                    {
                        WERKS = Row["WERKS"].ToString(),
                        LGORT = Row["LGORT"].ToString(),
                        MATNR = Row["MATNR"].ToString(),
                        MAKTX = Row["MAKTX"].ToString(),
                        LABST = Row["LABST"].ToString()
                    });
                }
                MARDResponse.result = "1";
                MARDResponse.message = "Success";

                return MARDResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }

        [HttpPost]
        public Object Transfer([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_STOCK_TRANSFER_HA");
                IRfcTable Itab = companyBapi.GetTable("ZTRANS");

                TransferRequestModel TransferRequest = JsonConvert.DeserializeObject<TransferRequestModel>(JsonConvert.SerializeObject(DATA));

                for (int i = 0; i < TransferRequest.data.Count(); i++)
                {
                    Itab.Append();
                    Itab[i].SetValue("BWART", TransferRequest.data[i].BWART);
                    Itab[i].SetValue("WERKS", TransferRequest.data[i].WERKS);
                    Itab[i].SetValue("LGORT", TransferRequest.data[i].LGORT);
                    Itab[i].SetValue("UMLGO", TransferRequest.data[i].UMLGO);
                    Itab[i].SetValue("MATNR", TransferRequest.data[i].MATNR);
                    Itab[i].SetValue("MENGE", TransferRequest.data[i].MENGE);
                    Itab[i].SetValue("ITEM_TXT", TransferRequest.data[i].ITEM_TXT);
                }

                companyBapi.SetValue("ZTRANS", Itab);
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("ZRETURN");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                TransferResponseModel TransferResponse = new TransferResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    TransferResponse.data.Add(new TransferResultModel
                    {
                        WERKS = Row["WERKS"].ToString(),
                        LGORT = Row["LGORT"].ToString(),
                        UMLGO = Row["UMLGO"].ToString(),
                        MATNR = Row["MATNR"].ToString(),
                        MENGE = Row["MENGE"].ToString(),
                        MBLNR = Row["MBLNR"].ToString(),
                        TYPE = Row["TYPE"].ToString(),
                        MESSAGE = Row["MESSAGE"].ToString(),
                        DATUM = Row["DATUM"].ToString(),
                        UZEIT = Row["UZEIT"].ToString()
                    });
                }
                TransferResponse.result = "1";
                TransferResponse.message = "Success";

                return TransferResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }
    }

    #endregion MARD(庫存) & Transfer(庫存轉移)

    #region CUSTPRICE(價格)

    public class CUSTPRICEController : ApiController
    {
        [HttpPost]
        public Object Get([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_CUST_PRICE_HA");
                companyBapi.SetValue("ALL", DATA["ALL"].ToString());
                companyBapi.SetValue("ZVKORG", DATA["ZVKORG"].ToString());
                companyBapi.SetValue("ZVTWEG", DATA["ZVTWEG"].ToString());
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KONP");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                CUSTPRICEResponseModel CUSTPRICEResponse = new CUSTPRICEResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    CUSTPRICEResponse.data.Add(new CUSTPRICEModel
                    {
                        VKORG = Row["VKORG"].ToString(),
                        VTWEG = Row["VTWEG"].ToString(),
                        KUNNR = Row["KUNNR"].ToString(),
                        MATNR = Row["MATNR"].ToString(),
                        KAETR = Row["KAETR"].ToString()
                    });
                }
                CUSTPRICEResponse.result = "1";
                CUSTPRICEResponse.message = "Success";

                return CUSTPRICEResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }
    }

    #endregion CUSTPRICE(價格)

    #region CUSTCREDIT(信限)

    public class CUSTCREDITController : ApiController
    {
        [HttpPost]
        public Object Get([FromBody] JObject DATA)
        {
            MyBackendConfig myConfig = new MyBackendConfig();
            RfcDestinationManager.RegisterDestinationConfiguration(myConfig);

            try
            {
                RfcDestination prd = RfcDestinationManager.GetDestination("PRD_000");
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("Z_RFC_GET_CUST_CREDIT_HA");
                companyBapi.SetValue("ALL", DATA["ALL"].ToString());
                companyBapi.SetValue("ZKKBER", DATA["ZKKBER"].ToString());
                companyBapi.Invoke(prd);
                IRfcTable data = companyBapi.GetTable("O_KNKK");

                DataTable dtRet = new DataTable();

                for (int liElement = 0; liElement < data.ElementCount; liElement++)
                {
                    RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);
                    dtRet.Columns.Add(rfcEMD.Name);
                }

                foreach (IRfcStructure row in data)
                {
                    DataRow dr = dtRet.NewRow();

                    for (int liElement = 0; liElement < data.ElementCount; liElement++)
                    {
                        RfcElementMetadata rfcEMD = data.GetElementMetadata(liElement);

                        dr[rfcEMD.Name] = row.GetString(rfcEMD.Name);
                    }

                    dtRet.Rows.Add(dr);
                }

                CUSTCREDITResponseModel CUSTCREDITResponse = new CUSTCREDITResponseModel();

                foreach (DataRow Row in dtRet.Rows)
                {
                    CUSTCREDITResponse.data.Add(new CUSTCREDITModel
                    {
                        KKBER = Row["KKBER"].ToString(),
                        KUNNR = Row["KUNNR"].ToString(),
                        KLIMK = Row["KLIMK"].ToString(),
                        OBLIG = Row["OBLIG"].ToString()
                    });
                }
                CUSTCREDITResponse.result = "1";
                CUSTCREDITResponse.message = "Success";

                return CUSTCREDITResponse;
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                return $"RfcCommunicationException {e.Message}";
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                return $"RfcLogonException {e.Message}";
            }
            catch (RfcAbapRuntimeException e)
            {
                // serious problem on ABAP system side...
                return $"RfcAbapRuntimeException {e.Message}";
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
                return $"RfcAbapBaseException {e.Message}";
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(myConfig);
            }
        }
    }

    #endregion CUSTCREDIT(信限)

    #endregion Controller

    #region Model

    public class ResultModel
    {
        public ResultModel()
        {
        }

        [Display(Name = "結果")]
        public string result { set; get; }

        [Display(Name = "訊息")]
        public string message { set; get; }
    }

    #region KNA1(客戶)

    public class KNA1ResponseModel : ResultModel
    {
        public KNA1ResponseModel()
        {
            this.data = new List<KNA1Model>();
        }

        [Display(Name = "資料")]
        public List<KNA1Model> data { get; set; }
    }

    public class KNA1Model
    {
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string VKORG { get; set; }
        public string VKBUR { get; set; }
        public string VKGRP { get; set; }
        public string KDGRP { get; set; }
        public string BEZEI { get; set; }
        public string TXNAM_SDB { get; set; }
        public string ORT01 { get; set; }
        public string STRAS { get; set; }
    }

    #endregion KNA1(客戶)

    #region MARA(物料)

    public class MARAResponseModel : ResultModel
    {
        public MARAResponseModel()
        {
            this.data = new List<MARAModel>();
        }

        [Display(Name = "資料")]
        public List<MARAModel> data { get; set; }
    }

    public class MARAModel
    {
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string MEINS { get; set; }
        public string MATKL { get; set; }
        public string PRODH { get; set; }
    }

    #endregion MARA(物料)

    #region ORDER(訂單)

    public class OrderRFCRequestModel
    {
        public OrderRFCRequestModel()
        {
            this.data = new List<OrderRFCItemModel>();
        }

        public string ZAUART { get; set; }
        public string ZVKORG { get; set; }
        public string ZVTWEG { get; set; }
        public string ZSPART { get; set; }
        public string ZKUNNR { get; set; }
        public string ZKUNWE { get; set; }
        public string ZBSTKD { get; set; }
        public string ZCITY1 { get; set; }
        public string ZSTREET { get; set; }
        public string ZTELF1 { get; set; }
        public string ZTEL_NUMBER { get; set; }
        public string ZANRED { get; set; }
        public string ZMARK { get; set; }

        [Display(Name = "資料")]
        public List<OrderRFCItemModel> data { get; set; }
    }

    public class OrderRFCItemModel
    {
        public string POSNR { get; set; }
        public string MATNR { get; set; }
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string KWMENG { get; set; }
        public string CO_NO { get; set; }
        public string CO_SEQ { get; set; }
    }

    public class OrderResponseModel : ResultModel
    {
        public OrderResponseModel()
        {
            this.data = new List<OrderErrorItemModel>();
        }

        public string SALESCODE { get; set; }
        public string DELIVERYCODE { get; set; }
        public string POSTINGCODE { get; set; }
        public string INVOICECODE { get; set; }

        [Display(Name = "失敗資料")]
        public List<OrderErrorItemModel> data { get; set; }
    }

    public class OrderErrorItemModel
    {
        public string CO_NO { get; set; }
        public string CO_SEQ { get; set; }
        public string PRODUCTNO { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string ERRORTYPE { get; set; }
        public string ERRORMESSAGE { get; set; }
        public string ERRORDATE { get; set; }
        public string ERRORTIME { get; set; }
    }

    #endregion ORDER(訂單)

    #region MARD(庫存)

    public class MARDRequestModel
    {
        public MARDRequestModel()
        {
            this.data = new List<MARDQueryModel>();
        }

        [Display(Name = "資料")]
        public List<MARDQueryModel> data { get; set; }
    }

    public class MARDQueryModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string MATNR { get; set; }
    }

    public class MARDResponseModel : ResultModel
    {
        public MARDResponseModel()
        {
            this.data = new List<MARDModel>();
        }

        [Display(Name = "資料")]
        public List<MARDModel> data { get; set; }
    }

    public class MARDModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string LABST { get; set; }
    }

    #endregion MARD(庫存)

    #region Transfer(庫存轉移)

    public class TransferRequestModel
    {
        public TransferRequestModel()
        {
            this.data = new List<TransferModel>();
        }

        [Display(Name = "資料")]
        public List<TransferModel> data { get; set; }
    }

    public class TransferModel
    {
        public string BWART { get; set; }
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string MATNR { get; set; }
        public string MENGE { get; set; }
        public string ITEM_TXT { get; set; }
    }

    public class TransferResponseModel : ResultModel
    {
        public TransferResponseModel()
        {
            this.data = new List<TransferResultModel>();
        }

        [Display(Name = "資料")]
        public List<TransferResultModel> data { get; set; }
    }

    public class TransferResultModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string MATNR { get; set; }
        public string MENGE { get; set; }
        public string MBLNR { get; set; }
        public string TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string DATUM { get; set; }
        public string UZEIT { get; set; }
    }

    #endregion Transfer(庫存轉移)

    #region CUSTPRICE(價格)

    public class CUSTPRICEResponseModel : ResultModel
    {
        public CUSTPRICEResponseModel()
        {
            this.data = new List<CUSTPRICEModel>();
        }

        [Display(Name = "資料")]
        public List<CUSTPRICEModel> data { get; set; }
    }

    public class CUSTPRICEModel
    {
        public string VKORG { get; set; }
        public string VTWEG { get; set; }
        public string KUNNR { get; set; }
        public string MATNR { get; set; }
        public string KAETR { get; set; }
    }

    #endregion CUSTPRICE(價格)

    #region CUSTCREDIT(信限)

    public class CUSTCREDITResponseModel : ResultModel
    {
        public CUSTCREDITResponseModel()
        {
            this.data = new List<CUSTCREDITModel>();
        }

        [Display(Name = "資料")]
        public List<CUSTCREDITModel> data { get; set; }
    }

    public class CUSTCREDITModel
    {
        public string KKBER { get; set; }
        public string KUNNR { get; set; }
        public string KLIMK { get; set; }
        public string OBLIG { get; set; }
    }

    #endregion CUSTCREDIT(信限)

    #endregion Model
}
