﻿using SAP.Middleware.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace TecoRFC
{
    public class MyBackendConfig : IDestinationConfiguration
    {
        public RfcConfigParameters GetParameters(String destinationName)
        {
            if ("PRD_000".Equals(destinationName))
            {
                RfcConfigParameters parms = new RfcConfigParameters();
                parms.Add(RfcConfigParameters.AppServerHost, ConfigurationManager.AppSettings["RFCAppServerHost"].ToString());
                parms.Add(RfcConfigParameters.SystemNumber, ConfigurationManager.AppSettings["RFCSystemNumber"].ToString());
                parms.Add(RfcConfigParameters.User, ConfigurationManager.AppSettings["RFCUser"].ToString());
                parms.Add(RfcConfigParameters.Password, ConfigurationManager.AppSettings["RFCPassword"].ToString());
                parms.Add(RfcConfigParameters.Client, ConfigurationManager.AppSettings["RFCClient"].ToString());
                parms.Add(RfcConfigParameters.Language, ConfigurationManager.AppSettings["RFCLanguage"].ToString());
                parms.Add(RfcConfigParameters.PoolSize, ConfigurationManager.AppSettings["RFCPoolSize"].ToString());
                parms.Add(RfcConfigParameters.PeakConnectionsLimit, ConfigurationManager.AppSettings["RFCPeakConnectionsLimit"].ToString());
                parms.Add(RfcConfigParameters.ConnectionIdleTimeout, ConfigurationManager.AppSettings["RFCConnectionIdleTimeout"].ToString());
                //parms.Add(RfcConfigParameters.MessageServerService, "3300");
                return parms;
            }
            else return null;
        }
        // The following two are not used in this example:
        public bool ChangeEventsSupported()
        {
            return false;
        }
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;
    }

}