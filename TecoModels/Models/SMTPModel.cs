﻿namespace TecoModels
{
    public class SMTPRequestModel
    {
        public string CustomerID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public class SMTPResponseModel : ResultModel
    {
    }
}