﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class InventoryRequestModel
    {
        public InventoryRequestModel()
        {
            this.data = new List<InventoryQueryModel>();
        }

        public string ID { get; set; }
        public string ADDRESS { get; set; }

        [Display(Name = "資料")]
        public List<InventoryQueryModel> data { get; set; }
    }

    public class InventoryQueryModel
    {
        public string TYPE { get; set; }
        public string PRODUCTCODE { get; set; }
        public string QUANTITY { get; set; }
    }

    public class InventoryResponseModel : ResultModel
    {
        public InventoryResponseModel()
        {
            this.data = new List<InventoryModel>();
        }

        [Display(Name = "資料")]
        public List<InventoryModel> data { get; set; }
    }

    public class InventoryModel
    {
        public string TYPE { get; set; }
        public string PRODUCTCODE { get; set; }
        public string INVENTORY { get; set; }
        public string TOTALINVENTORY { get; set; }
        public string SHIPPINGTIME { get; set; }
        public string MAXSHIPPINGTIME { get; set; }
    }

    public class ShippingModel
    {
        public string Inventory { get; set; }
        public string WareHouse { get; set; }
        public string ShippingTime { get; set; }
    }
}