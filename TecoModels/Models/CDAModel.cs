﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 後臺

    public class CDAResponseModel
    {
        public long ID { get; set; }
        public string GROUPID { get; set; }
        public string CUSTOMERNAME { get; set; }
        public string CUSTOMERID { get; set; }
        public string GROUPCODE { get; set; }
        public string GROUPTYPE { get; set; }
        public string GROUPNAME { get; set; }
        public string DEPARTMENTID { get; set; }
        public string MANAGEREMAIL { get; set; }
        public string MANAGERNAME { get; set; }
    }

    public class CDAUploadModel
    {
        public string GROUPID { get; set; }
        public string CUSTOMERNAME { get; set; }
        public string CUSTOMERID { get; set; }
        public string GROUPCODE { get; set; }
        public string GROUPTYPE { get; set; }
        public string GROUPNAME { get; set; }
        public string DEPARTMENTID { get; set; }
        public string MANAGEREMAIL { get; set; }
        public string MANAGERNAME { get; set; }
    }

    #endregion 後臺
}