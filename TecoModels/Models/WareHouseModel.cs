﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 前臺

    public class WareHouseRequestModel
    {
        public string ADDRESS { get; set; }
    }

    public class WareHouseResponseModel : ResultModel
    {
        public WareHouseResponseModel()
        {
            this.data = new WareHouseModel();
        }

        public WareHouseModel data { get; set; }
    }

    public class WareHouseModel
    {
        public string CODE { get; set; }
    }

    #endregion 前臺

    #region 後臺

    public class WHResponseModel
    {
        public long ID { get; set; }
        public string CODE { get; set; }
        public string WEIGHT { get; set; }
        public string AREA { get; set; }
        public string NOTE { get; set; }
    }

    public class WHUploadModel
    {
        public string CODE { get; set; }
        public string WEIGHT { get; set; }
        public string AREA { get; set; }
        public string NOTE { get; set; }
    }

    #endregion 後臺
}