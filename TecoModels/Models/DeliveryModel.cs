﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class DeliveryRequestModel
    {
        public DeliveryRequestModel()
        {
        }

        public string ID { get; set; }
        public string EXTERNORDERKEY { get; set; }
    }

    public class DeliveryRequestSendModel
    {
        public DeliveryRequestSendModel()
        {
            this.BODY = new DeliveryRequestBodyModel();
            this.HEADER = new DeliveryRequestHeaderModel();
        }

        public DeliveryRequestBodyModel BODY { get; set; }
        public DeliveryRequestHeaderModel HEADER { get; set; }
    }

    public class DeliveryRequestBodyModel
    {
        public DeliveryRequestBodyModel()
        {
            this.QRY = new DeliveryRequestQryModel();
        }

        public DeliveryRequestQryModel QRY { get; set; }
    }

    public class DeliveryRequestQryModel
    {
        public DeliveryRequestQryModel()
        {
            this.QRYLIST = new List<DeliveryRequestQryListModel>();
        }

        public List<DeliveryRequestQryListModel> QRYLIST { get; set; }
    }

    public class DeliveryRequestQryListModel
    {
        public DeliveryRequestQryListModel()
        {
        }

        public string CUSTNO { get; set; }
        public string EXTERNORDERKEY { get; set; }
    }

    public class DeliveryRequestHeaderModel
    {
        public DeliveryRequestHeaderModel()
        {
        }

        public string CREATEDATE { get; set; }
    }

    public class DeliveryResultModel
    {
        public DeliveryResultModel()
        {
            this.DOC = new DeliveryResultDocModel();
        }

        public DeliveryResultDocModel DOC { get; set; }
    }

    public class DeliveryResultDocModel
    {
        public DeliveryResultDocModel()
        {
            this.HEADER = new DeliveryResultHeaderModel();
            this.BODY = new DeliveryResultBodyModel();
        }

        public DeliveryResultHeaderModel HEADER { get; set; }
        public DeliveryResultBodyModel BODY { get; set; }
    }

    public class DeliveryResultHeaderModel
    {
        public DeliveryResultHeaderModel()
        {
        }

        public string CREATEDATE { get; set; }
    }

    public class DeliveryResultBodyModel
    {
        public DeliveryResultBodyModel()
        {
            this.QRYBAK = new DeliveryResultQrybakModel();
        }

        public DeliveryResultQrybakModel QRYBAK { get; set; }
        public string RETURNCODE { get; set; }
        public string RETURNDESC { get; set; }
    }

    public class DeliveryResultQrybakModel
    {
        public DeliveryResultQrybakModel()
        {
            this.QRYBAKLIST = new DeliveryResultQrybaklistModel();
        }

        public string EXTERNORDERKEY { get; set; }
        public DeliveryResultQrybaklistModel QRYBAKLIST { get; set; }
    }

    public class DeliveryResultQrybaklistModel
    {
        public DeliveryResultQrybaklistModel()
        {
        }

        public string WHNO { get; set; }
        public string CUSTNO { get; set; }
        public string SHTNO { get; set; }
        public string SSTS { get; set; }
        public string DSTS { get; set; }
        public string GIDT { get; set; }
        public string RDEDT { get; set; }
    }

    public class DeliveryResponseModel : ResultModel
    {
        public DeliveryResponseModel()
        {
            this.data = new List<DeliveryModel>();
        }

        [Display(Name = "資料")]
        public List<DeliveryModel> data { get; set; }
    }

    public class DeliveryModel
    {
        public string EXTERNORDERKEY { get; set; }
        public string WHNO { get; set; }
        public string CUSTNO { get; set; }
        public string SHTNO { get; set; }
        public string SSTS { get; set; }
        public string DSTS { get; set; }
        public string GIDT { get; set; }
        public string RDEDT { get; set; }
        public string RETURNCODE { get; set; }
        public string RETURNDESC { get; set; }
    }
}