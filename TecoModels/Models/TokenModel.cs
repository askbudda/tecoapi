﻿using System.Collections.Generic;

namespace TecoModels
{
    public class TokenModel
    {
        public string TOKEN { get; set; }
        public string REFRESH { get; set; }
        public int EXP { get; set; }
    }

    public class PayloadModel
    {
        public UserModel INFO { get; set; }
        public int EXP { get; set; }
    }

    public class UserModel
    {
        public string ID { get; set; }   
        public string NAME { get; set; }
        public string DEVICE { get; set; }
    }

    #region 前臺

    public class SignInRequestModel
    {
        public string ACCOUNT { get; set; }
        public string PASSWORD { get; set; }
        public string DEVICE { get; set; }
    }

    public class SignInResponseModel : ResultModel
    {
        public SignInResponseModel()
        {
            this.TokenData = new TokenModel();
            this.CustomerData = new List<CustomerModel>();
            this.CreditData = new List<CreditModel>();
            this.ProductRData = new List<ProductRModel>();
            this.GiveawayData = new List<GiveawayModel>();
        }

        public TokenModel TokenData { get; set; }
        public List<CustomerModel> CustomerData { get; set; }
        public List<CreditModel> CreditData { get; set; }
        public List<ProductRModel> ProductRData { get; set; }
        public List<GiveawayModel> GiveawayData { get; set; }
    }

    public class TokenRefreshRequestModel
    {
        public string ACCOUNT { get; set; }
        public string DEVICE { get; set; }
        public string REFRESH { get; set; }
    }

    public class TokenRefreshResponseModel : ResultModel
    {
        public TokenRefreshResponseModel()
        {
            this.data = new TokenModel();
        }

        public TokenModel data { get; set; }
    }

    #endregion 前臺

    #region 後臺

    public class SignInManagerRequestModel
    {
        public string ACCOUNT { get; set; }
        public string PASSWORD { get; set; }
    }

    public class SignInManagerResponseModel : ResultModel
    {
        public SignInManagerResponseModel()
        {
            this.TokenData = new TokenModel();
        }

        public TokenModel TokenData { get; set; }
    }

    public class TokenRefreshManagerRequestModel
    {
        public string ACCOUNT { get; set; }
        public string REFRESH { get; set; }
    }

    public class TokenRefreshManagerResponseModel : ResultModel
    {
        public TokenRefreshManagerResponseModel()
        {
            this.data = new TokenModel();
        }

        public TokenModel data { get; set; }
    }

    #endregion 後臺
}