﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class PasswordModifyModel
    {
        public string ACCOUNT { get; set; }
        public string PASSWORDOLD { get; set; }
        public string PASSWORDNEW { get; set; }
    }

    public class PasswordResetModel
    {
        public string ACCOUNT { get; set; }
    }
}