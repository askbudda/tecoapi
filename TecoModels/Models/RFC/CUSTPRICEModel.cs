﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class CUSTPRICEResponseModel : ResultModel
    {
        public CUSTPRICEResponseModel()
        {
            this.data = new List<CUSTPRICEModel>();
        }

        [Display(Name = "資料")]
        public List<CUSTPRICEModel> data { get; set; }
    }

    public class CUSTPRICEModel
    {
        public string VKORG { get; set; }
        public string VTWEG { get; set; }
        public string KUNNR { get; set; }
        public string MATNR { get; set; }
        public string KAETR { get; set; }
    }
}