﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class CUSTCREDITResponseModel : ResultModel
    {
        public CUSTCREDITResponseModel()
        {
            this.data = new List<CUSTCREDITModel>();
        }

        [Display(Name = "資料")]
        public List<CUSTCREDITModel> data { get; set; }
    }

    public class CUSTCREDITModel
    {
        public string KKBER { get; set; }
        public string KUNNR { get; set; }
        public string KLIMK { get; set; }
        public string OBLIG { get; set; }
    }
}