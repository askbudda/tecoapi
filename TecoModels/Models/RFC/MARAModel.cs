﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class MARAResponseModel : ResultModel
    {
        public MARAResponseModel()
        {
            this.data = new List<MARAModel>();
        }

        [Display(Name = "資料")]
        public List<MARAModel> data { get; set; }
    }

    public class MARAModel
    {
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string MEINS { get; set; }
        public string MATKL { get; set; }
        public string PRODH { get; set; }
    }
}