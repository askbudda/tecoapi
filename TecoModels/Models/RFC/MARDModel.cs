﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class MARDRequestModel
    {
        public MARDRequestModel()
        {
            this.data = new List<MARDQueryModel>();
        }

        [Display(Name = "資料")]
        public List<MARDQueryModel> data { get; set; }
    }

    public class MARDQueryModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string MATNR { get; set; }
    }

    public class MARDResponseModel : ResultModel
    {
        public MARDResponseModel()
        {
            this.data = new List<MARDModel>();
        }

        [Display(Name = "資料")]
        public List<MARDModel> data { get; set; }
    }

    public class MARDModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string LABST { get; set; }
    }
}