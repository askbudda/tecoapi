﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class KNA1ResponseModel : ResultModel
    {
        public KNA1ResponseModel()
        {
            this.data = new List<KNA1Model>();
        }

        [Display(Name = "資料")]
        public List<KNA1Model> data { get; set; }
    }

    public class KNA1Model
    {
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string VKORG { get; set; }
        public string VKBUR { get; set; }
        public string VKGRP { get; set; }
        public string KDGRP { get; set; }
        public string BEZEI { get; set; }
        public string TXNAM_SDB { get; set; }
        public string ORT01 { get; set; }
        public string STRAS { get; set; }
    }
}