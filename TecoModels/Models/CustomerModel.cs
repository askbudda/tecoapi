﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class CustomerRequestModel
    {
        public string ID { get; set; }
    }

    public class CustomerResponseModel : ResultModel
    {
        public CustomerResponseModel()
        {
            this.data = new List<CustomerModel>();
        }

        [Display(Name = "資料")]
        public List<CustomerModel> data { get; set; }
    }

    public class CustomerModel
    {
        public string ID { get; set; }
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public string GROUP { get; set; }
        public string NOTE { get; set; }
        public string USER { get; set; }
        public string ADDRESS { get; set; }
        public string DEPARTMENT { get; set; }
    }
}