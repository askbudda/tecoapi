﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TecoModels
{
    public class WorkDayResponseModel : ResultModel
    {
        public WorkDayResponseModel()
        {
            this.data = new List<WorkDayModel>();
        }

        [Display(Name = "資料")]
        public List<WorkDayModel> data { get; set; }
    }

    public class WorkDayModel
    {
        public int date { get; set; }
        public string weekDay { get; set; }
        public int type { get; set; }
        public string memo { get; set; }
    }
}