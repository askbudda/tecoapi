﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TecoModels
{
    public class CreditRequestModel
    {
        public string ID { get; set; }
    }

    public class CreditResponseModel : ResultModel
    {
        public CreditResponseModel()
        {
            this.data = new List<CreditModel>();
        }

        [Display(Name = "資料")]
        public List<CreditModel> data { get; set; }
    }

    public class CreditModel
    {
        public string TYPE { get; set; }
        public string TOTAL { get; set; }
        public string USED { get; set; }
        public string PERCENTBASE { get; set; }
        public string REMAINING { get; set; }
    }
}