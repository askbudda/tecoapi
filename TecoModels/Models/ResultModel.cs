﻿using System.ComponentModel.DataAnnotations;

namespace TecoModels
{
    public class ResultModel
    {
        public ResultModel()
        {
        }

        [Display(Name = "結果")]
        public string result { set; get; }

        [Display(Name = "訊息")]
        public string message { set; get; }
    }
}