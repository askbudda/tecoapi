﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class MessageResponseModel : ResultModel
    {
        public MessageResponseModel()
        {
            this.data = new List<MessageModel>();
        }

        [Display(Name = "資料")]
        public List<MessageModel> data { get; set; }
    }

    public class MessageModel
    {
        public string messageId { get; set; }
        public string message { get; set; }
        public string title { get; set; }
        public DateTime createTime { get; set; }
        public string tmCode { get; set; }
    }
}