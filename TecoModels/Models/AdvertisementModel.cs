﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 後臺

    public class AQueryResponseModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public string Enable { get; set; }
    }

    public class AAddRequestModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Enable { get; set; }
    }

    public class AModifyRequestModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Enable { get; set; }
    }

    public class ADeleteRequestModel
    {
        public long ID { get; set; }
    }

    #endregion 後臺
}