﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 後臺

    public class MQueryResponseModel
    {
        public long ID { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Enable { get; set; }
    }

    public class MAddRequestModel
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Enable { get; set; }
    }

    public class MModifyRequestModel
    {
        public long ID { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Enable { get; set; }
    }

    public class MDeleteRequestModel
    {
        public long ID { get; set; }
    }

    #endregion 後臺
}