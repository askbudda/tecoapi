﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 前臺

    public class ProductRRequestModel
    {
        public string ID { get; set; }
        public string Type { get; set; }
    }

    public class ProductRResponseModel : ResultModel
    {
        public ProductRResponseModel()
        {
            this.data = new List<ProductRModel>();
        }

        [Display(Name = "資料")]
        public List<ProductRModel> data { get; set; }
    }

    public class ProductRModel
    {
        public ProductRModel()
        {
            this.PRODUCTPRICE = new List<PRICEModel>();
            this.RELATEDPRODUCTPRICE = new List<PRICEModel>();
        }

        public string TYPENAME { get; set; }
        public string REFRIGERANT { get; set; }
        public string POWER { get; set; }
        public string SQUAREMETERS { get; set; }
        public string DEHUMIDIFIERCAPACITY { get; set; }
        public string ENERGYEFFICIENCYRATING { get; set; }
        public string SEASONALPERFORMANCE { get; set; }
        public string ELECTRICITYCONSUMPTIONPERYEAR { get; set; }
        public string ELECTRICCURRENT { get; set; }
        public string ELECTRICITYCONSUMPTION { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTSIZE { get; set; }
        public string PRODUCTWEIGHT { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTSIZE { get; set; }
        public string RELATEDPRODUCTWEIGHT { get; set; }
        public string LIQUIDPIPINGSIZE { get; set; }
        public string AIRPIPINGSIZE { get; set; }
        public string OUTDOORFEETSIZE { get; set; }
        public string CUSTOMERTYPE { get; set; }
        public string TYPE { get; set; }
        public string GIVEAWAY { get; set; }
        public string TECOURL { get; set; }
        public List<PRICEModel> PRODUCTPRICE { get; set; }
        public List<PRICEModel> RELATEDPRODUCTPRICE { get; set; }
        public System.DateTime CREATE_TIME { get; set; }
        public string CREATE_USER { get; set; }
    }

    public class PRICEModel
    {
        public string TYPE { get; set; }
        public string PRICE { get; set; }
    }

    #endregion 前臺

    #region 後臺

    public class PRResponseModel
    {
        public long ID { get; set; }
        public string TYPENAME { get; set; }
        public string REFRIGERANT { get; set; }
        public string POWER { get; set; }
        public string SQUAREMETERS { get; set; }
        public string DEHUMIDIFIERCAPACITY { get; set; }
        public string ENERGYEFFICIENCYRATING { get; set; }
        public string SEASONALPERFORMANCE { get; set; }
        public string ELECTRICITYCONSUMPTIONPERYEAR { get; set; }
        public string ELECTRICCURRENT { get; set; }
        public string ELECTRICITYCONSUMPTION { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTSIZE { get; set; }
        public string PRODUCTWEIGHT { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTSIZE { get; set; }
        public string RELATEDPRODUCTWEIGHT { get; set; }
        public string LIQUIDPIPINGSIZE { get; set; }
        public string AIRPIPINGSIZE { get; set; }
        public string OUTDOORFEETSIZE { get; set; }
        public string CUSTOMERTYPE { get; set; }
        public string TYPE { get; set; }
        public string GIVEAWAY { get; set; }
        public string TECOURL { get; set; }
    }

    public class PRUploadModel
    {
        public string TYPENAME { get; set; }
        public string REFRIGERANT { get; set; }
        public string POWER { get; set; }
        public string SQUAREMETERS { get; set; }
        public string DEHUMIDIFIERCAPACITY { get; set; }
        public string ENERGYEFFICIENCYRATING { get; set; }
        public string SEASONALPERFORMANCE { get; set; }
        public string ELECTRICITYCONSUMPTIONPERYEAR { get; set; }
        public string ELECTRICCURRENT { get; set; }
        public string ELECTRICITYCONSUMPTION { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTSIZE { get; set; }
        public string PRODUCTWEIGHT { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTSIZE { get; set; }
        public string RELATEDPRODUCTWEIGHT { get; set; }
        public string LIQUIDPIPINGSIZE { get; set; }
        public string AIRPIPINGSIZE { get; set; }
        public string OUTDOORFEETSIZE { get; set; }
        public string CUSTOMERTYPE { get; set; }
        public string TYPE { get; set; }
        public string GIVEAWAY { get; set; }
        public string TECOURL { get; set; }
    }

    #endregion 後臺
}