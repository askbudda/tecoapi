﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TecoModels.NotificationModel
{
    public class UserDeviceModel
    {
        public string deviceToken { get; set; }
        public string userId { get; set; }
    }
}