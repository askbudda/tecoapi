﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TecoModels.NotificationModel.RequestBody
{
    public class NotificationReqBody
    {
        public int? id { set; get; }
        public string title { set; get; }
        public string message { set; get; }
        public DateTime? scheduleTime { set; get; }
        public int type { set; get; }
    }
}