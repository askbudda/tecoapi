﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TecoModels.NotificationModel
{
    public class PushBatchModel
    {
        public PushDirectModel pushnotification { set; get; }
        public string path { set; get; }
    }
    public class PushDirectModel
    {
        public int id { set; get; }
        public string title { set; get; }
        public string message { set; get; }
        public List<string> targets { set; get; }
        public string redirectPage { set; get; }
    }
    public class PushScheduleModel
    {
        public PushBatchModel batchpushobject { set; get; }
        public DateTime? schedule { set; get; }
    }
}