﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using TecoModels;

namespace TecoService
{
    partial class BaseService : ServiceBase
    {
        private System.Timers.Timer MyTimer;

        public BaseService()
        {
            InitializeComponent();

            this.AutoLog = false;

            if (!System.Diagnostics.EventLog.SourceExists("BaseService"))
            {
                System.Diagnostics.EventLog.CreateEventSource("BaseService", "TecoBatchLog");
            }

            eventLog1.Source = "BaseService";
        }

        protected override void OnStart(string[] args)
        {
            //執行排程
            Schedule();
        }

        protected override void OnStop()
        {
            // TODO: 在此加入停止服務所需執行的終止程式碼。
            MyTimer.Stop();
            MyTimer = null;
        }

        private void Schedule()
        {
            //先執行一次更新(因為排程執行時間(週日00:00)才會執行)
            UpdateNow();

            // TODO: 在此加入啟動服務的程式碼。
            MyTimer = new System.Timers.Timer();
            MyTimer.Elapsed += new ElapsedEventHandler(Update);
            //每分鐘執行
            MyTimer.Interval = 60 * 1000; //(七天)7 * 24 * 60 * 60 * 1000
            MyTimer.Start();
        }

        private void Update(object sender, ElapsedEventArgs e)
        {
            //判斷排程執行時間(週日00:00)
            DateTime NowTime = DateTime.Now;
            string DayOfTheWeek = NowTime.DayOfWeek.ToString("d");

            if (DayOfTheWeek == "0" && NowTime.ToString("HH") == "00" &&
               (NowTime.ToString("mm") == "00" || NowTime.ToString("mm") == "01" ||
                NowTime.ToString("mm") == "02" || NowTime.ToString("mm") == "03"))
            {
                UpdateKNA1();
                Thread.Sleep(3000);

                UpdateCUSTCREDIT();
                Thread.Sleep(3000);

                UpdateMARA();
                Thread.Sleep(3000);

                UpdateCUSTPRICE();
            }
        }

        private void UpdateNow()
        {
            UpdateKNA1();
            Thread.Sleep(3000);

            UpdateCUSTCREDIT();
            Thread.Sleep(3000);

            UpdateMARA();
            Thread.Sleep(3000);

            UpdateCUSTPRICE();
        }

        #region [KNA1]客戶更新:RFC=>DB

        private void UpdateKNA1()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            eventLog1.WriteEntry("/KNA1/Update - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:客戶查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/KNA1/Get", "{\"ALL\":\"x\"}");
                KNA1ResponseModel Response = JsonConvert.DeserializeObject<KNA1ResponseModel>(HttpClientHelper.ConvertString(GetData));

                //判斷客戶清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    var KNA1List = TecoApp.KNA1.AsEnumerable().ToList();
                    var OldFirstID = (KNA1List.Count() == 0 ? 0 : Convert.ToInt32(KNA1List.FirstOrDefault().ID));
                    var OldLastID = (KNA1List.Count() == 0 ? 0 : Convert.ToInt32(KNA1List.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增客戶清單
                    var NameExist = "";
                    foreach (var KNA1 in Response.data)
                    {
                        KNA1 log = new KNA1();
                        log.ID = ID;
                        log.KUNNR = KNA1.KUNNR.Trim();
                        log.NAME1 = KNA1.NAME1.Trim();
                        log.VKORG = KNA1.VKORG.Trim();
                        log.VKBUR = KNA1.VKBUR.Trim();
                        log.VKGRP = KNA1.VKGRP.Trim();
                        log.KDGRP = KNA1.KDGRP.Trim();
                        log.BEZEI = KNA1.BEZEI.Trim();
                        log.TXNAM_SDB = KNA1.TXNAM_SDB.Trim();
                        log.ORT01 = KNA1.ORT01.Trim();
                        log.STRAS = KNA1.STRAS.Trim();
                        log.CREATE_TIME = System.DateTime.Now;
                        log.CREATE_USER = "System";
                        TecoApp.KNA1.Add(log);
                        ID++;

                        #region 設定登入清單

                        var AccountExist = false;
                        var KUNNRValue = ReplaceEng(KNA1.KUNNR);
                        var SignInList = TecoApp.SignIn.AsEnumerable().ToList();
                        foreach (var SignInItem in SignInList)
                        {
                            if (SignInItem.Account == KUNNRValue)
                            {
                                AccountExist = true;
                                break;
                            }
                        }

                        if (!AccountExist)
                        {
                            if (!NameExist.Contains(KUNNRValue))
                            {
                                SignIn signin = new SignIn();
                                signin.Account = KUNNRValue;
                                signin.Password = KUNNRValue;
                                signin.Name = KNA1.NAME1;
                                signin.Device = "";
                                signin.Refresh = "";
                                signin.Enable = "Y";
                                signin.CREATE_TIME = System.DateTime.Now;
                                signin.CREATE_USER = "System";
                                TecoApp.SignIn.Add(signin);
                                NameExist += KUNNRValue + ",";
                            }
                        }

                        #endregion 設定登入清單
                    }

                    TecoApp.KNA1.RemoveRange(KNA1List);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/KNA1/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/KNA1/Update - End");
            }
        }

        //清除英文字
        private static string ReplaceEng(string value)
        {
            return value.Replace("A", "").Replace("B", "").Replace("C", "")
                .Replace("D", "").Replace("E", "").Replace("F", "")
                .Replace("G", "").Replace("H", "").Replace("I", "")
                .Replace("J", "").Replace("K", "").Replace("L", "")
                .Replace("M", "").Replace("N", "").Replace("O", "")
                .Replace("P", "").Replace("Q", "").Replace("R", "")
                .Replace("S", "").Replace("T", "").Replace("U", "")
                .Replace("V", "").Replace("W", "").Replace("X", "")
                .Replace("Y", "").Replace("Z", "")
                .Replace("a", "").Replace("b", "").Replace("c", "")
                .Replace("d", "").Replace("e", "").Replace("f", "")
                .Replace("g", "").Replace("h", "").Replace("i", "")
                .Replace("j", "").Replace("k", "").Replace("l", "")
                .Replace("m", "").Replace("n", "").Replace("o", "")
                .Replace("p", "").Replace("q", "").Replace("r", "")
                .Replace("s", "").Replace("t", "").Replace("u", "")
                .Replace("v", "").Replace("w", "").Replace("x", "")
                .Replace("y", "").Replace("z", "");
        }

        #endregion [KNA1]客戶更新:RFC=>DB

        #region [CUSTCREDIT]信限更新:RFC=>DB

        private void UpdateCUSTCREDIT()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            eventLog1.WriteEntry("/CUSTCREDIT/Update - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:信限查詢(R)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"x\",\"ZKKBER\":\"R000\"}");
                CUSTCREDITResponseModel Response1 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:信限查詢(K)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"x\",\"ZKKBER\":\"K000\"}");
                CUSTCREDITResponseModel Response2 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData2));

                //判斷信限清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0))
                {
                    var CUSTCREDITList = TecoApp.CUSTCREDIT.AsEnumerable().ToList();
                    var OldFirstID = (CUSTCREDITList.Count() == 0 ? 0 : Convert.ToInt32(CUSTCREDITList.FirstOrDefault().ID));
                    var OldLastID = (CUSTCREDITList.Count() == 0 ? 0 : Convert.ToInt32(CUSTCREDITList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增信限清單
                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        foreach (var CUSTCREDIT in Response1.data)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = ID;
                            log.KKBER = CUSTCREDIT.KKBER.Trim();
                            log.KUNNR = CUSTCREDIT.KUNNR.Trim();
                            log.KLIMK = CUSTCREDIT.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDIT.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                            ID++;
                        }
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        foreach (var CUSTCREDIT in Response2.data)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = ID;
                            log.KKBER = CUSTCREDIT.KKBER.Trim();
                            log.KUNNR = CUSTCREDIT.KUNNR.Trim();
                            log.KLIMK = CUSTCREDIT.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDIT.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                            ID++;
                        }
                    }

                    TecoApp.CUSTCREDIT.RemoveRange(CUSTCREDITList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/CUSTCREDIT/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/MARD/Update - End");
            }
        }

        #endregion [CUSTCREDIT]信限更新:RFC=>DB

        #region [MARA]商品更新:RFC=>DB

        private void UpdateMARA()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            eventLog1.WriteEntry("/MARA/Update - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:商品查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARA/Get", "{\"ALL\":\"x\"}");
                MARAResponseModel Response = JsonConvert.DeserializeObject<MARAResponseModel>(HttpClientHelper.ConvertString(GetData));

                //判斷商品清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    var MARAList = TecoApp.MARA.AsEnumerable().ToList();
                    var OldFirstID = (MARAList.Count() == 0 ? 0 : Convert.ToInt32(MARAList.FirstOrDefault().ID));
                    var OldLastID = (MARAList.Count() == 0 ? 0 : Convert.ToInt32(MARAList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增商品清單
                    foreach (var MARA in Response.data)
                    {
                        MARA log = new MARA();
                        log.ID = ID;
                        log.MATNR = MARA.MATNR.Trim();
                        log.MAKTX = MARA.MAKTX.Trim();
                        log.MEINS = MARA.MEINS.Trim();
                        log.MATKL = MARA.MATKL.Trim();
                        log.PRODH = MARA.PRODH.Trim();
                        log.CREATE_TIME = System.DateTime.Now;
                        log.CREATE_USER = "System";
                        TecoApp.MARA.Add(log);
                        ID++;
                    }

                    TecoApp.MARA.RemoveRange(MARAList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/MARA/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/MARA/Update - End");
            }
        }

        #endregion [MARA]商品更新:RFC=>DB

        #region [CUSTPRICE]價格更新:RFC=>DB

        private void UpdateCUSTPRICE()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            eventLog1.WriteEntry("/CUSTPRICE/Update - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:價格查詢(R.01)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"01\"}");
                CUSTPRICEResponseModel Response1 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(R.18)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"18\"}");
                CUSTPRICEResponseModel Response2 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData2));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(K.28)
                var GetData3 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"K000\",\"ZVTWEG\":\"28\"}");
                CUSTPRICEResponseModel Response3 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData3));

                //判斷價格清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0) ||
                    (Response3.result == "1" && Response3.data.Count() > 0))
                {
                    var CUSTPRICEList = TecoApp.CUSTPRICE.AsEnumerable().ToList();
                    var OldFirstID = (CUSTPRICEList.Count() == 0 ? 0 : Convert.ToInt32(CUSTPRICEList.FirstOrDefault().ID));
                    var OldLastID = (CUSTPRICEList.Count() == 0 ? 0 : Convert.ToInt32(CUSTPRICEList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增價格清單
                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        foreach (var CUSTPRICE in Response1.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        foreach (var CUSTPRICE in Response2.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }
                    if (Response3.result == "1" && Response3.data.Count() > 0)
                    {
                        foreach (var CUSTPRICE in Response3.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }

                    TecoApp.CUSTPRICE.RemoveRange(CUSTPRICEList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/CUSTPRICE/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/CUSTPRICE/Update - End");
            }
        }

        #endregion [CUSTPRICE]價格更新:RFC=>DB
    }
}
