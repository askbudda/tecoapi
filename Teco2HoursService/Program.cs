﻿using System.ServiceProcess;

namespace Teco2HoursService
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Base2HoursService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
