﻿using NLog;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Teco2HoursService
{
    public static class HttpClientHelper
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private static string m_error = string.Empty;
        public static string Error {
            get { return m_error; }
        }

        /// <summary>
        /// 送出Http請求
        /// </summary>
        /// <param name="httpMethod">POST/GET</param>
        /// <param name="url"></param>
        /// <param name="postData">httpMethod=POST才有效</param>
        /// <returns>http responseText</returns>
        public static string GET_Http(string url)
        {
            string result = "";
            try
            {
                WebRequest req = HttpWebRequest.Create(url);
                req.Method = "GET";
                //req.Timeout = 3000;

                try
                {
                    using (var response = (HttpWebResponse)req.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr = new StreamReader(stream);
                        result = sr.ReadToEnd();
                        result = ConvertString(result);
                    }
                }
                catch (WebException wex1)
                {
                    _logger.Warn("status = {0}{1},{2}", (wex1.Response == null ? "" : ((HttpWebResponse)wex1.Response).StatusCode.ToString()), wex1.Status, wex1.Message);
                    m_error = string.Format("{0},{1}", ((HttpWebResponse)wex1.Response).StatusCode, ((HttpWebResponse)wex1.Response).StatusDescription);
                    //throw new Exception(m_wrapper_except3);
                    throw wex1;
                }
                catch (Exception ex1)
                {
                    _logger.Warn(ex1.Message);
                    //throw new Exception(m_wrapper_except4);
                    throw ex1;
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 送出Http請求 for JSON
        /// </summary>
        /// <param name="httpMethod">POST/GET</param>
        /// <param name="url"></param>
        /// <param name="postData">httpMethod=POST才有效</param>
        /// <returns>http responseText</returns>
        public static string POST_JSON(string url, string postData)
        {
            string result = "";
            try
            {
                WebRequest req = HttpWebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/json";
                //req.Timeout = 3000;
                if (postData != "")
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    try
                    {
                        using (var dataStream = req.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                        }
                    }
                    catch (WebException wex0)
                    {
                        _logger.Warn("status = {0}{1},{2}", (wex0.Response == null ? "" : ((HttpWebResponse)wex0.Response).StatusCode.ToString()), wex0.Status, wex0.Message);
                        //throw new Exception(m_wrapper_except3);
                        throw wex0;
                    }
                    catch (Exception ex0)
                    {
                        _logger.Warn(ex0.Message);
                        //throw new Exception(m_wrapper_except4);
                        throw ex0;
                    }
                }

                try
                {
                    using (var response = (HttpWebResponse)req.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr = new StreamReader(stream);
                        result = sr.ReadToEnd();
                        result = ConvertString(result);
                    }
                }
                catch (WebException wex1)
                {
                    _logger.Warn("status = {0}{1},{2}", (wex1.Response == null ? "" : ((HttpWebResponse)wex1.Response).StatusCode.ToString()), wex1.Status, wex1.Message);
                    m_error = string.Format("{0},{1}", ((HttpWebResponse)wex1.Response).StatusCode, ((HttpWebResponse)wex1.Response).StatusDescription);
                    //throw new Exception(m_wrapper_except3);
                    throw wex1;
                }
                catch (Exception ex1)
                {
                    _logger.Warn(ex1.Message);
                    //throw new Exception(m_wrapper_except4);
                    throw ex1;
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 送出Http請求 for XML
        /// </summary>
        /// <param name="httpMethod">POST/GET</param>
        /// <param name="url"></param>
        /// <param name="postData">httpMethod=POST才有效</param>
        /// <returns>http responseText</returns>
        public static string POST_XML(string url, string postData)
        {
            string result = "";
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                WebRequest req = HttpWebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = byteArray.Length;
                //req.Timeout = 3000;
                if (postData.ToString() != "")
                {
                    try
                    {
                        using (var dataStream = req.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                        }
                    }
                    catch (WebException wex0)
                    {
                        _logger.Warn("status = {0}{1},{2}", (wex0.Response == null ? "" : ((HttpWebResponse)wex0.Response).StatusCode.ToString()), wex0.Status, wex0.Message);
                        //throw new Exception(m_wrapper_except3);
                        throw wex0;
                    }
                    catch (Exception ex0)
                    {
                        _logger.Warn(ex0.Message);
                        //throw new Exception(m_wrapper_except4);
                        throw ex0;
                    }
                }

                try
                {
                    using (var response = (HttpWebResponse)req.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr = new StreamReader(stream);
                        result = sr.ReadToEnd();
                        result = ConvertString(result);
                    }
                }
                catch (WebException wex1)
                {
                    _logger.Warn("status = {0}{1},{2}", (wex1.Response == null ? "" : ((HttpWebResponse)wex1.Response).StatusCode.ToString()), wex1.Status, wex1.Message);
                    m_error = string.Format("{0},{1}", ((HttpWebResponse)wex1.Response).StatusCode, ((HttpWebResponse)wex1.Response).StatusDescription);
                    //throw new Exception(m_wrapper_except3);
                    throw wex1;
                }
                catch (Exception ex1)
                {
                    _logger.Warn(ex1.Message);
                    //throw new Exception(m_wrapper_except4);
                    throw ex1;
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static string ConvertString(string inStr)
        {
            string outStr = (string)new System.ComponentModel.StringConverter().ConvertFromString(inStr);
            return outStr;
        }
    }
}
