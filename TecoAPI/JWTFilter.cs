﻿using System.Linq;
using System.Web.Http.Filters;
using TecoModels;

namespace TecoAPI
{
    public class JWTFilter : ActionFilterAttribute
    {
        private TokenManager _tokenManager;
        public JWTFilter()
        {
            _tokenManager = new TokenManager();
        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var request = actionContext.Request;

            if (!WithoutVerifyToken(request.RequestUri.ToString()))
            {
                if (request.Headers.Authorization == null || request.Headers.Authorization.Scheme != "Bearer")
                {
                    throw new System.Exception("Token Losted");
                }
                else
                {
                    var result = Verify(request.Headers.Authorization.Parameter);

                    if (result == null)
                    {
                        throw new System.Exception("Token Expired");
                    }
                }
            }

            base.OnActionExecuting(actionContext);
        }

        public UserModel Verify(string Token)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            var Key = TecoApp.Config.AsEnumerable().Where(p => p.Code == "Key" && p.Enable == "Y").ToList().FirstOrDefault().Value;
            return _tokenManager.Verify(Token, Key);
        }

        //設定Token除外名單
        public bool WithoutVerifyToken(string requestUri)
        {
            #region Hangfire Dashboard

            if (requestUri.EndsWith("/hangfire"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/servers"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/recurring"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/retries"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/enqueued"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/scheduled"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/processing"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/succeeded"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/failed"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/deleted"))
            {
                return true;
            }

            if (requestUri.EndsWith("/hangfire/jobs/awaiting"))
            {
                return true;
            }

            #endregion Hangfire Dashboard

            #region Token

            if (requestUri.EndsWith("/Token/SignIn")) 
            {
                return true;
            }

            if (requestUri.EndsWith("/Token/Refresh"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Token/SignInManager"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Token/RefreshManager"))
            {
                return true;
            }

            #endregion Token

            #region SMTP

            if (requestUri.EndsWith("/SMTP/SendMail"))
            {
                return true;
            }

            #endregion SMTP

            #region Order排程

            if (requestUri.EndsWith("/Order/addNotification"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Order/UploadPushFile"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Order/SendMail"))
            {
                return true;
            }

            #endregion Order排程

            #region Schedule

            if (requestUri.EndsWith("/Schedule/Add"))
            {
                return true;
            }

            #endregion Schedule

            #region Batch

            if (requestUri.EndsWith("/Batch/Base"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/KNA1"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/MARA"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/CUSTCREDIT"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/CUSTPRICE"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/MARD"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Batch/Order"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Order/Batch"))
            {
                return true;
            }

            #endregion Batch

            #region RFC

            if (requestUri.EndsWith("/KNA1/Update")) 
            {
                return true;
            }

            if (requestUri.EndsWith("/MARA/Update"))
            {
                return true;
            }

            if (requestUri.EndsWith("/CUSTCREDIT/Update"))
            {
                return true;
            }

            if (requestUri.EndsWith("/CUSTPRICE/Update"))
            {
                return true;
            }

            if (requestUri.EndsWith("/MARD/Update"))
            {
                return true;
            }

            #endregion RFC

            #region Management

            #region 客戶部門歸屬資料

            if (requestUri.EndsWith("/CDA/Import"))
            {
                return true;
            }

            if (requestUri.EndsWith("/CDA/Query"))
            {
                return true;
            }

            #endregion 客戶部門歸屬資料

            #region 倉庫資料

            if (requestUri.EndsWith("/WareHouse/Import"))
            {
                return true;
            }

            if (requestUri.EndsWith("/WareHouse/Query"))
            {
                return true;
            }

            #endregion 倉庫資料

            #region 商品(R)資料

            if (requestUri.EndsWith("/ProductR/Import"))
            {
                return true;
            }

            if (requestUri.EndsWith("/ProductR/Query"))
            {
                return true;
            }

            #endregion 商品(R)資料

            #region 贈品資料

            if (requestUri.EndsWith("/Giveaway/Query"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Giveaway/Add"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Giveaway/Modify"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Giveaway/Delete"))
            {
                return true;
            }

            #endregion 贈品資料

            #region 管理員資料

            if (requestUri.EndsWith("/Manager/Query"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Manager/Add"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Manager/Modify"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Manager/Delete"))
            {
                return true;
            }

            #endregion 管理員資料

            #region Config資料

            if (requestUri.EndsWith("/Config/Query"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Config/Add"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Config/Modify"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Config/Delete"))
            {
                return true;
            }

            #endregion Config資料

            #region 廣告資料

            if (requestUri.EndsWith("/Advertisement/Query"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Advertisement/Add"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Advertisement/Modify"))
            {
                return true;
            }

            if (requestUri.EndsWith("/Advertisement/Delete"))
            {
                return true;
            }

            #endregion 廣告資料

            #region 訂單/庫存資料

            if (requestUri.EndsWith("/Order/Get"))
            {
                return true;
            }

            if (requestUri.EndsWith("/MARD/Query"))
            {
                return true;
            }

            #endregion 廣告資料

            #endregion Management

            return false;
        }
    }
}