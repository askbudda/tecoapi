﻿using RestSharp;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using TecoModels.NotificationModel;

namespace TecoAPI.Services
{
    public class PushNotificationService
    {
        public static async Task<HttpStatusCode> Push(PushDirectModel pushDirectModel)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["PushApiUrl"]);
            var request = new RestRequest("", Method.POST);
            request.AddJsonBody(pushDirectModel);
            request.AddHeader("Content-type", "application/json");
            var response = await client.ExecuteTaskAsync(request);
            return response.StatusCode;
        }
        public static async Task<HttpStatusCode> PushBatch(PushBatchModel pushBatchModel)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["PushApiUrl"]);
            var request = new RestRequest(ConfigurationManager.AppSettings["PushBatchApiUrl"], Method.POST);
            request.AddJsonBody(pushBatchModel);
            request.AddHeader("Content-type", "application/json");
            var response = await client.ExecuteTaskAsync(request);
            return response.StatusCode;
        }
        public static async Task<HttpStatusCode> PushSchedule(PushScheduleModel pushScheduleModel)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["PushApiUrl"]);
            var request = new RestRequest(ConfigurationManager.AppSettings["PushScheduleApiUrl"], Method.POST);
            request.AddJsonBody(pushScheduleModel);
            request.AddHeader("Content-type", "application/json");
            var response = await client.ExecuteTaskAsync(request);
            return response.StatusCode;
        }
    }
}