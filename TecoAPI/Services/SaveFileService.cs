﻿using CsvHelper;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using TecoModels;
using TecoModels.NotificationModel;

namespace TecoAPI.Services
{
    public class SaveFileService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static UploadPushFileResponse UploadPushFile(MultipartFormDataStreamProvider provider)
        {
            TecoAppEntities db = null;
            UploadPushFileResponse uploadPushFileResponse = new UploadPushFileResponse();
            var path = "";
            List<string> userIds = new List<string>();
            try
            {
                foreach (MultipartFileData file in provider.FileData)
                {
                    string fileName = file.Headers.ContentDisposition.FileName;
                    string newFileName = Path.GetFileName(file.LocalFileName);
                    string fullFilePath;
                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }
                    fullFilePath = ConfigurationManager.AppSettings["SavePushTempFilePath"] + newFileName;

                    using (var sr = new StreamReader(fullFilePath))
                    {
                        while (!sr.EndOfStream)
                        {
                            userIds.Add(sr.ReadLine().Split(',')[0]);
                        }
                    }

                    db = new TecoAppEntities();
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                    {
                        var device = (from item in db.DEVICEINFO
                                      where userIds.Contains(item.UserId)
                                      select item).OrderByDescending(x => x.LastTime).FirstOrDefault();

                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(device.DeviceToken);

                        path = ConfigurationManager.AppSettings["SavePushFilePath"] + newFileName;
                        File.WriteAllText(path, builder.ToString());
                        scope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            uploadPushFileResponse.userIds = userIds;
            uploadPushFileResponse.path = path;
            return uploadPushFileResponse;
        }

        public static UploadPushFileResponse UploadWorkDayFile(MultipartFormDataStreamProvider provider)
        {
            TecoAppEntities db = null;
            UploadPushFileResponse uploadPushFileResponse = new UploadPushFileResponse();
            var path = "";
            List<string> userIds = new List<string>();
            try
            {
                foreach (MultipartFileData file in provider.FileData)
                {
                    string fileName = file.Headers.ContentDisposition.FileName;
                    string newFileName = Path.GetFileName(file.LocalFileName);
                    string fullFilePath;
                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }
                    fullFilePath = ConfigurationManager.AppSettings["SaveWorkdayTempFilePath"] + newFileName;

                    /*using (var sr = new StreamReader(fullFilePath))
                    {
                        while (!sr.EndOfStream)
                        {
                            userIds.Add(sr.ReadLine().Split(',')[0]);
                        }
                    }*/

                    /*db = new TecoAppEntities();
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                    {
                        var device = (from item in db.DEVICEINFO
                                      where userIds.Contains(item.UserId)
                                      select item).OrderByDescending(x => x.LastTime).FirstOrDefault();

                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(device.DeviceToken);

                        path = ConfigurationManager.AppSettings["SavePushFilePath"] + newFileName;
                        File.WriteAllText(path, builder.ToString());
                        scope.Complete();
                    }*/

                    //將csv內容寫入DB
                    TecoAppEntities TecoApp = new TecoAppEntities();

                    using (var reader = new StreamReader(fullFilePath))
                    using (var csv = new CsvReader(reader))
                    {
                        csv.Configuration.HasHeaderRecord = false; //不抓欄位名稱，中文會有問題
                        csv.Read(); //跳過第一行欄位名稱
                        var records = csv.GetRecords<WorkDayModel>().ToList();

                        //檢查是否已有第一天資料
                        var tempData = records.ElementAt(0).date;
                        int tempYear = tempData / 10000;
                        int tempEnd = (tempYear + 1) * 10000;

                        //清除當年度資料
                        var remove = (from date in TecoApp.WORKDAY
                                      where date.date >= tempData && date.date < ((tempYear + 1) * 10000)
                                      select date).ToList();

                        if (remove != null)
                        {
                            foreach (var data in remove)
                            {
                                TecoApp.WORKDAY.Remove(data);
                            }
                            TecoApp.SaveChanges();
                        }

                        //寫入資料
                        foreach (var day in records)
                        {
                            var dateInfo = new WORKDAY()
                            {
                                date = day.date,
                                weekDay = day.weekDay,
                                type = day.type,
                                memo = day.memo,
                            };
                            TecoApp.WORKDAY.Add(dateInfo);
                        }
                        TecoApp.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            uploadPushFileResponse.userIds = userIds;
            uploadPushFileResponse.path = path;
            return uploadPushFileResponse;
        }

        public async static Task<string> SaveOrderMessage(List<string> userIdList)
        {
            TecoAppEntities db = null;
            var path = "";
            try
            {
                db = new TecoAppEntities();
                #region 取deviceToken 並存入csv
                StringBuilder userClause = new StringBuilder();
                userIdList = userIdList.Distinct<string>().ToList();
                for (var i = 0; i < userIdList.Count; i++)
                {
                    var userId = userIdList[i];

                    userClause.Append("\'" + userId + "\'");

                    if (i != userIdList.Count - 1)
                    {
                        userClause.Append(" , ");
                    }
                }

                string sql = "select UserId, DeviceToken from deviceinfo where UserId in( " + userClause + " ) ORDER BY LastTime DESC";
                var userDeviceModel = await db.Database.SqlQuery<UserDeviceModel>(sql).ToListAsync();

                List<string> deviceTokens = new List<string>();
                List<string> userIds = new List<string>();

                foreach (var item in userDeviceModel)
                {
                    if (!userIds.Contains(item.userId))
                    {
                        deviceTokens.Add(item.deviceToken);
                        userIds.Add(item.userId);
                    }
                }

                StringBuilder builder = new StringBuilder();
                foreach (var deviceToken in deviceTokens)
                {
                    builder.AppendLine(deviceToken);
                }

                path = ConfigurationManager.AppSettings["SavePushFilePath"] + DateTime.Now.ToString("yyyyMMddHHmmssff") + "_" + Guid.NewGuid().ToString().Split('-')[0] + ".csv";
                File.WriteAllText(path, builder.ToString());

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return path;
        }
        public static string FileNameFilter(string fileName)
        {
            fileName = fileName.Replace("\"", string.Empty);
            var specialWords = ConfigurationManager.AppSettings["SpecialWords"].Split(',');
            foreach (var specialWord in specialWords)
            {
                fileName = fileName.Replace(specialWord, string.Empty);
            }
            return fileName;
        }
    }
    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public string path { get; set; }
        public CustomMultipartFormDataStreamProvider(string path) : base(path)
        {
            this.path = path;
        }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            string fileName = SaveFileService.FileNameFilter(headers.ContentDisposition.Name);
            string pathFile = this.path + fileName;
            if (File.Exists(pathFile))
            {
                int index = fileName.LastIndexOf(".");
                fileName = fileName.Insert(index, DateTime.Now.ToString("yyyyMMddHHmmssff") + "_" + Guid.NewGuid().ToString().Split('-')[0]);
            }
            return fileName;
        }
    }
}