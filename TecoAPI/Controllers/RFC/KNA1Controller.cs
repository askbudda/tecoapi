﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class KNA1Controller : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 客戶更新(全部):RFC=>DB

        [HttpGet]
        public string Update()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:客戶查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/KNA1/Get", "{\"ALL\":\"x\"}");
                KNA1ResponseModel Response = JsonConvert.DeserializeObject<KNA1ResponseModel>(HttpClientHelper.ConvertString(GetData));

                //判斷客戶清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    result = "Get " + Response.data.Count() + "data";

                    var KNA1List = TecoApp.KNA1.AsEnumerable().ToList();
                    var OldFirstID = (KNA1List.Count() == 0 ? 0: Convert.ToInt32(KNA1List.FirstOrDefault().ID));
                    var OldLastID = (KNA1List.Count() == 0 ? 0: Convert.ToInt32(KNA1List.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增客戶清單
                    var NameExist = "";
                    foreach (var KNA1 in Response.data)
                    {
                        KNA1 log = new KNA1(); 
                        log.ID = ID;
                        log.KUNNR = KNA1.KUNNR.Trim();
                        log.NAME1 = KNA1.NAME1.Trim();
                        log.VKORG = KNA1.VKORG.Trim();
                        log.VKBUR = KNA1.VKBUR.Trim();
                        log.VKGRP = KNA1.VKGRP.Trim();
                        log.KDGRP = KNA1.KDGRP.Trim();
                        log.BEZEI = KNA1.BEZEI.Trim();
                        log.TXNAM_SDB = KNA1.TXNAM_SDB.Trim();
                        log.ORT01 = KNA1.ORT01.Trim();
                        log.STRAS = KNA1.STRAS.Trim();
                        log.CREATE_TIME = System.DateTime.Now;
                        log.CREATE_USER = "System";
                        TecoApp.KNA1.Add(log);
                        ID++;

                        #region 設定登入清單

                        var AccountExist = false;
                        var KUNNRValue = ReplaceEng(KNA1.KUNNR);
                        var SignInList = TecoApp.SignIn.AsEnumerable().ToList();
                        foreach (var SignInItem in SignInList) 
                        {
                            if (SignInItem.Account == KUNNRValue) 
                            {
                                AccountExist = true;
                                break;
                            }
                        }

                        if (!AccountExist) 
                        {
                            if (!NameExist.Contains(KUNNRValue)) 
                            {
                                SignIn signin = new SignIn();
                                signin.Account = KUNNRValue;
                                signin.Password = KUNNRValue;
                                signin.Name = KNA1.NAME1;
                                signin.Device = "";
                                signin.Refresh = "";
                                signin.Enable = "Y";
                                signin.CREATE_TIME = System.DateTime.Now;
                                signin.CREATE_USER = "System";
                                TecoApp.SignIn.Add(signin);
                                NameExist += KUNNRValue + ",";
                            }
                        }

                        #endregion 設定登入清單
                    }

                    TecoApp.KNA1.RemoveRange(KNA1List);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/KNA1/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 客戶更新(全部):RFC=>DB

        #region 客戶更新(部分):RFC=>DB

        [HttpGet]
        public string UpdatePortion()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:客戶查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/KNA1/Get", "{\"ALL\":\"\"}");
                KNA1ResponseModel Response = JsonConvert.DeserializeObject<KNA1ResponseModel>(HttpClientHelper.ConvertString(GetData));

                //DB新增客戶清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    result = "Get " + Response.data.Count() + "data";

                    var KNA1List = TecoApp.KNA1.AsEnumerable().ToList();

                    var NewIDCount = 1;

                    var NameExist = "";
                    foreach (var KNA1Update in Response.data)
                    {
                        var IsNew = true;

                        foreach (var KNA1 in KNA1List)
                        {
                            if (KNA1.VKORG == KNA1Update.VKORG.Trim() && KNA1.KUNNR == KNA1Update.KUNNR.Trim())
                            {
                                KNA1.NAME1 = KNA1Update.NAME1.Trim();
                                KNA1.VKBUR = KNA1Update.VKBUR.Trim();
                                KNA1.VKGRP = KNA1Update.VKGRP.Trim();
                                KNA1.KDGRP = KNA1Update.KDGRP.Trim();
                                KNA1.BEZEI = KNA1Update.BEZEI.Trim();
                                KNA1.TXNAM_SDB = KNA1Update.TXNAM_SDB.Trim();
                                KNA1.ORT01 = KNA1Update.ORT01.Trim();
                                KNA1.STRAS = KNA1Update.STRAS.Trim();
                                KNA1.UPDATE_TIME = System.DateTime.Now;
                                KNA1.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            KNA1 log = new KNA1();
                            log.ID = KNA1List.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.KUNNR = KNA1Update.KUNNR.Trim();
                            log.NAME1 = KNA1Update.NAME1.Trim();
                            log.VKORG = KNA1Update.VKORG.Trim();
                            log.VKBUR = KNA1Update.VKBUR.Trim();
                            log.VKGRP = KNA1Update.VKGRP.Trim();
                            log.KDGRP = KNA1Update.KDGRP.Trim();
                            log.BEZEI = KNA1Update.BEZEI.Trim();
                            log.TXNAM_SDB = KNA1Update.TXNAM_SDB.Trim();
                            log.ORT01 = KNA1Update.ORT01.Trim();
                            log.STRAS = KNA1Update.STRAS.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.KNA1.Add(log);
                        }

                        #region 設定登入清單

                        var AccountExist = false;
                        var KUNNRValue = ReplaceEng(KNA1Update.KUNNR);
                        var SignInList = TecoApp.SignIn.AsEnumerable().ToList();
                        foreach (var SignInItem in SignInList)
                        {
                            if (SignInItem.Account == KUNNRValue)
                            {
                                AccountExist = true;
                                break;
                            }
                        }

                        if (!AccountExist)
                        {
                            if (!NameExist.Contains(KUNNRValue))
                            {
                                SignIn signin = new SignIn();
                                signin.Account = KUNNRValue;
                                signin.Password = KUNNRValue;
                                signin.Name = KNA1Update.NAME1;
                                signin.Device = "";
                                signin.Refresh = "";
                                signin.Enable = "Y";
                                signin.CREATE_TIME = System.DateTime.Now;
                                signin.CREATE_USER = "System";
                                TecoApp.SignIn.Add(signin);
                                NameExist += KUNNRValue + ",";
                            }
                        }

                        #endregion 設定登入清單
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/KNA1/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 客戶更新(部分):RFC=>DB

        //清除英文字
        public static string ReplaceEng(string value) 
        {
            return value.Replace("A", "").Replace("B", "").Replace("C", "")
                .Replace("D", "").Replace("E", "").Replace("F", "")
                .Replace("G", "").Replace("H", "").Replace("I", "")
                .Replace("J", "").Replace("K", "").Replace("L", "")
                .Replace("M", "").Replace("N", "").Replace("O", "")
                .Replace("P", "").Replace("Q", "").Replace("R", "")
                .Replace("S", "").Replace("T", "").Replace("U", "")
                .Replace("V", "").Replace("W", "").Replace("X", "")
                .Replace("Y", "").Replace("Z", "")
                .Replace("a", "").Replace("b", "").Replace("c", "")
                .Replace("d", "").Replace("e", "").Replace("f", "")
                .Replace("g", "").Replace("h", "").Replace("i", "")
                .Replace("j", "").Replace("k", "").Replace("l", "")
                .Replace("m", "").Replace("n", "").Replace("o", "")
                .Replace("p", "").Replace("q", "").Replace("r", "")
                .Replace("s", "").Replace("t", "").Replace("u", "")
                .Replace("v", "").Replace("w", "").Replace("x", "")
                .Replace("y", "").Replace("z", "");
        }
    }
}
