﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class CUSTCREDITController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 信限更新(全部):RFC=>DB

        [HttpGet]
        public string Update()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:信限查詢(R)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"x\",\"ZKKBER\":\"R000\"}");
                CUSTCREDITResponseModel Response1 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData1));
                
                System.Threading.Thread.Sleep(1000);

                //RFC:信限查詢(K)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"x\",\"ZKKBER\":\"K000\"}");
                CUSTCREDITResponseModel Response2 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData2));

                //判斷信限清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0))
                {
                    var CUSTCREDITList = TecoApp.CUSTCREDIT.AsEnumerable().ToList();
                    var OldFirstID = (CUSTCREDITList.Count() == 0 ? 0 : Convert.ToInt32(CUSTCREDITList.FirstOrDefault().ID));
                    var OldLastID = (CUSTCREDITList.Count() == 0 ? 0 : Convert.ToInt32(CUSTCREDITList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增信限清單
                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        result = "Get " + Response1.data.Count() + "data-R; ";

                        foreach (var CUSTCREDIT in Response1.data)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = ID;
                            log.KKBER = CUSTCREDIT.KKBER.Trim();
                            log.KUNNR = CUSTCREDIT.KUNNR.Trim();
                            log.KLIMK = CUSTCREDIT.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDIT.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                            ID++;
                        }
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        result += "Get " + Response2.data.Count() + "data-K; ";

                        foreach (var CUSTCREDIT in Response2.data)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = ID;
                            log.KKBER = CUSTCREDIT.KKBER.Trim();
                            log.KUNNR = CUSTCREDIT.KUNNR.Trim();
                            log.KLIMK = CUSTCREDIT.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDIT.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                            ID++;
                        }
                    }

                    TecoApp.CUSTCREDIT.RemoveRange(CUSTCREDITList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CUSTCREDIT/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 信限更新(全部):RFC=>DB

        #region 信限更新(部分):RFC=>DB

        [HttpGet]
        public string UpdatePortion()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:信限查詢(R)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"\",\"ZKKBER\":\"R000\"}");
                CUSTCREDITResponseModel Response1 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:信限查詢(K)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"\",\"ZKKBER\":\"K000\"}");
                CUSTCREDITResponseModel Response2 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData2));

                //判斷信限清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0))
                {
                    var CUSTCREDITList = TecoApp.CUSTCREDIT.AsEnumerable().ToList();
                    List<CUSTCREDITModel> UpdateList = new List<CUSTCREDITModel>();

                    var NewIDCount = 1;

                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        result = "Get " + Response1.data.Count() + "data-R; ";
                        UpdateList.AddRange(Response1.data);
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        result += "Get " + Response2.data.Count() + "data-K; ";
                        UpdateList.AddRange(Response2.data);
                    }

                    //DB新增信限清單
                    foreach (var CUSTCREDITUpdate in UpdateList)
                    {
                        var IsNew = true;

                        foreach (var CUSTCREDIT in CUSTCREDITList)
                        {
                            if (CUSTCREDIT.KKBER == CUSTCREDITUpdate.KKBER.Trim() && CUSTCREDIT.KUNNR == CUSTCREDITUpdate.KUNNR.Trim())
                            {
                                CUSTCREDIT.KLIMK = CUSTCREDITUpdate.KLIMK.Trim().Replace(".00", "");
                                CUSTCREDIT.OBLIG = CUSTCREDITUpdate.OBLIG.Trim().Replace(".00", "");
                                CUSTCREDIT.UPDATE_TIME = System.DateTime.Now;
                                CUSTCREDIT.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = CUSTCREDITList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.KKBER = CUSTCREDITUpdate.KKBER.Trim();
                            log.KUNNR = CUSTCREDITUpdate.KUNNR.Trim();
                            log.KLIMK = CUSTCREDITUpdate.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDITUpdate.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CUSTCREDIT/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 信限更新(部分):RFC=>DB
    }
}
