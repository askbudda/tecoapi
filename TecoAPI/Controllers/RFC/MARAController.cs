﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class MARAController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 商品更新(全部):RFC=>DB

        [HttpGet]
        public string Update()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:商品查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARA/Get", "{\"ALL\":\"x\"}");
                MARAResponseModel Response = JsonConvert.DeserializeObject<MARAResponseModel>(HttpClientHelper.ConvertString(GetData));

                //判斷商品清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    result = "Get " + Response.data.Count() + "data";

                    var MARAList = TecoApp.MARA.AsEnumerable().ToList();
                    var OldFirstID = (MARAList.Count() == 0 ? 0 : Convert.ToInt32(MARAList.FirstOrDefault().ID));
                    var OldLastID = (MARAList.Count() == 0 ? 0 : Convert.ToInt32(MARAList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }
                    
                    //DB新增商品清單
                    foreach (var MARA in Response.data)
                    {
                        MARA log = new MARA();
                        log.ID = ID;
                        log.MATNR = MARA.MATNR.Trim();
                        log.MAKTX = MARA.MAKTX.Trim();
                        log.MEINS = MARA.MEINS.Trim();
                        log.MATKL = MARA.MATKL.Trim();
                        log.PRODH = MARA.PRODH.Trim();
                        log.CREATE_TIME = System.DateTime.Now;
                        log.CREATE_USER = "System";
                        TecoApp.MARA.Add(log);
                        ID++;
                    }

                    TecoApp.MARA.RemoveRange(MARAList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/MARA/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 商品更新(全部):RFC=>DB

        #region 商品更新(部分):RFC=>DB

        [HttpGet]
        public string UpdatePortion()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:商品查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARA/Get", "{\"ALL\":\"\"}");
                MARAResponseModel Response = JsonConvert.DeserializeObject<MARAResponseModel>(HttpClientHelper.ConvertString(GetData));

                //DB新增商品清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    result = "Get " + Response.data.Count() + "data";

                    var MARAList = TecoApp.MARA.AsEnumerable().ToList();

                    var NewIDCount = 1;

                    foreach (var MARAUpdate in Response.data)
                    {
                        var IsNew = true;

                        foreach (var MARA in MARAList)
                        {
                            if (MARA.MATNR == MARAUpdate.MATNR.Trim())
                            {
                                MARA.MAKTX = MARAUpdate.MAKTX.Trim();
                                MARA.MEINS = MARAUpdate.MEINS.Trim();
                                MARA.MATKL = MARAUpdate.MATKL.Trim();
                                MARA.PRODH = MARAUpdate.PRODH.Trim();
                                MARA.UPDATE_TIME = System.DateTime.Now;
                                MARA.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            MARA log = new MARA();
                            log.ID = MARAList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.MATNR = MARAUpdate.MATNR.Trim();
                            log.MAKTX = MARAUpdate.MAKTX.Trim();
                            log.MEINS = MARAUpdate.MEINS.Trim();
                            log.MATKL = MARAUpdate.MATKL.Trim();
                            log.PRODH = MARAUpdate.PRODH.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.MARA.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/MARA/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 商品更新(部分):RFC=>DB
    }
}
