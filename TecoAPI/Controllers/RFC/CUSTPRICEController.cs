﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class CUSTPRICEController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 價格更新(全部):RFC=>DB

        [HttpGet]
        public string Update()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:價格查詢(R.01)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"01\"}");
                CUSTPRICEResponseModel Response1 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(R.18)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"18\"}");
                CUSTPRICEResponseModel Response2 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData2));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(K.28)
                var GetData3 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"x\",\"ZVKORG\":\"K000\",\"ZVTWEG\":\"28\"}");
                CUSTPRICEResponseModel Response3 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData3));

                //判斷價格清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0) ||
                    (Response3.result == "1" && Response3.data.Count() > 0))
                {
                    var CUSTPRICEList = TecoApp.CUSTPRICE.AsEnumerable().ToList();
                    var OldFirstID = (CUSTPRICEList.Count() == 0 ? 0 : Convert.ToInt32(CUSTPRICEList.FirstOrDefault().ID));
                    var OldLastID = (CUSTPRICEList.Count() == 0 ? 0 : Convert.ToInt32(CUSTPRICEList.LastOrDefault().ID));

                    //設定新增的ID初始值
                    int ID = 1;
                    if (OldFirstID < 10000)
                    {
                        ID = OldLastID + 10000;
                    }

                    //DB新增價格清單
                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        result = "Get " + Response1.data.Count() + "data-R.01; ";

                        foreach (var CUSTPRICE in Response1.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        result += "Get " + Response2.data.Count() + "data-R.18; ";

                        foreach (var CUSTPRICE in Response2.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }
                    if (Response3.result == "1" && Response3.data.Count() > 0)
                    {
                        result += "Get " + Response3.data.Count() + "data-K.28; ";

                        foreach (var CUSTPRICE in Response3.data)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = ID;
                            log.VKORG = CUSTPRICE.VKORG.Trim();
                            log.VTWEG = CUSTPRICE.VTWEG.Trim();
                            log.KUNNR = CUSTPRICE.KUNNR.Trim();
                            log.MATNR = CUSTPRICE.MATNR.Trim();
                            log.KAETR = CUSTPRICE.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                            ID++;
                        }
                    }

                    TecoApp.CUSTPRICE.RemoveRange(CUSTPRICEList);
                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CUSTPRICE/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 價格更新(全部):RFC=>DB

        #region 價格更新(部分):RFC=>DB

        [HttpGet]
        public string UpdatePortion()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:價格查詢(R.01)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"01\"}");
                CUSTPRICEResponseModel Response1 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(R.18)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"18\"}");
                CUSTPRICEResponseModel Response2 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData2));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(K.28)
                var GetData3 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"K000\",\"ZVTWEG\":\"28\"}");
                CUSTPRICEResponseModel Response3 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData3));

                //判斷價格清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0) ||
                    (Response3.result == "1" && Response3.data.Count() > 0))
                {
                    var CUSTPRICEList = TecoApp.CUSTPRICE.AsEnumerable().ToList();
                    List<CUSTPRICEModel> UpdateList = new List<CUSTPRICEModel>();

                    var NewIDCount = 1;

                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        result = "Get " + Response1.data.Count() + "data-R.01; ";
                        UpdateList.AddRange(Response1.data);
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        result += "Get " + Response2.data.Count() + "data-R.18; ";
                        UpdateList.AddRange(Response2.data);
                    }
                    if (Response3.result == "1" && Response3.data.Count() > 0)
                    {
                        result += "Get " + Response3.data.Count() + "data-K.28; ";
                        UpdateList.AddRange(Response3.data);
                    }

                    //DB更新價格清單
                    foreach (var CUSTPRICEUpdate in UpdateList)
                    {
                        var IsNew = true;

                        foreach (var CUSTPRICE in CUSTPRICEList) 
                        { 
                            if (CUSTPRICE.VKORG == CUSTPRICEUpdate.VKORG.Trim() && CUSTPRICE.VTWEG == CUSTPRICEUpdate.VTWEG.Trim() &&
                                CUSTPRICE.KUNNR == CUSTPRICEUpdate.KUNNR.Trim() && CUSTPRICE.MATNR == CUSTPRICEUpdate.MATNR.Trim())
                            {
                                CUSTPRICE.KAETR = CUSTPRICEUpdate.KAETR.Trim().Replace(".00", "");
                                CUSTPRICE.UPDATE_TIME = System.DateTime.Now;
                                CUSTPRICE.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = CUSTPRICEList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.VKORG = CUSTPRICEUpdate.VKORG.Trim();
                            log.VTWEG = CUSTPRICEUpdate.VTWEG.Trim();
                            log.KUNNR = CUSTPRICEUpdate.KUNNR.Trim();
                            log.MATNR = CUSTPRICEUpdate.MATNR.Trim();
                            log.KAETR = CUSTPRICEUpdate.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CUSTPRICE/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 價格更新(部分):RFC=>DB
    }
}
