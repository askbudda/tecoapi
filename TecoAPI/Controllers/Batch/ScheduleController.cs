﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Http;
using TecoModels;
using Hangfire;
using NLog;
using System;

namespace TecoAPI.Controllers
{
    public class ScheduleController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //新增排程
        [HttpPost]
        public ResultModel Add([FromBody] JObject DATA)
        {
            ResultModel Response = new ResultModel();
            ScheduleRequestModel ScheduleRequest = JsonConvert.DeserializeObject<ScheduleRequestModel>(JsonConvert.SerializeObject(DATA));

            try
            {
                #region 排程

                if (ScheduleRequest.Function == "Base")
                {
                    //Sample:"0 0,2,4,6,8,10,12,14,16,18,20,22 * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.Base(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.Base(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "KNA1")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.KNA1(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.KNA1(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "MARA")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.MARA(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.MARA(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "CUSTCREDIT")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.CUSTCREDIT(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.CUSTCREDIT(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "CUSTPRICE")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.CUSTPRICE(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.CUSTPRICE(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "MARD")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.MARD(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.MARD(), TimeSpan.FromHours(2));
                }
                else if (ScheduleRequest.Function == "Order")
                {
                    //Sample:"0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
                    RecurringJob.AddOrUpdate(() => BatchController.Order(), ScheduleRequest.Schedule);
                    //BackgroundJob.Schedule(() => BatchController.Order(), TimeSpan.FromMinutes(1));
                }

                #region Sample

                // fire and got:站台啟動後只會執行一次
                //BackgroundJob.Enqueue(() => Console.WriteLine("Fire and forgot"));

                // delay: 設定時間間隔，每隔時間間隔執行一次
                //BackgroundJob.Schedule(() => Console.WriteLine("Delayed"), TimeSpan.FromDays(1));

                // recurring: 設定cron敘述，重複執行多次
                //RecurringJob.AddOrUpdate(() => Console.WriteLine("Daily Job"), Cron.Daily);

                // continue: 在某個job執行完後接續執行
                //var id = BackgroundJob.Enqueue(() => Console.WriteLine("Hello, "));
                //BackgroundJob.ContinueWith(id, () => Console.WriteLine("world!"));

                #endregion Sample

                #endregion 排程

                Response.result = "1";
                Response.message = "Success";
            }
            catch (Exception ex)
            {
                logger.Error("/Schedule/Add - " + ex.Message);
                Response.result = "0";
                Response.message = "Exception:/Schedule/Add:" + ex.Message;
            }

            return Response;
        }
    }
}
