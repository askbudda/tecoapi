﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class SMTPController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 取得信限資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> SendMail([FromBody] JObject DATA)
        {
            SMTPResponseModel SMTPResponse = new SMTPResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                SMTPRequestModel SMTPRequest = JsonConvert.DeserializeObject<SMTPRequestModel>(JsonConvert.SerializeObject(DATA));

                #region 設定郵件內容

                System.Net.Mail.MailMessage MailMsg = new System.Net.Mail.MailMessage();

                //寄件者
                var SMTPAccount = ConfigurationManager.AppSettings["SMTPAccount"].ToString();
                var SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                MailMsg.From = new System.Net.Mail.MailAddress(SMTPAccount, SMTPAccount.Substring(0, SMTPAccount.IndexOf('@')), System.Text.Encoding.UTF8);

                //收件者(測試用)
                MailMsg.To.Add(new System.Net.Mail.MailAddress("nox@webcomm.com.tw", "Nox", System.Text.Encoding.UTF8));
                MailMsg.To.Add(new System.Net.Mail.MailAddress("billy.liu@webcomm.com.tw", "Billy", System.Text.Encoding.UTF8));
                var SMTPTestMail = ConfigurationManager.AppSettings["SMTPTestMail"].ToString();
                MailMsg.To.Add(new System.Net.Mail.MailAddress(SMTPTestMail, SMTPTestMail.Substring(0, SMTPTestMail.IndexOf('@')), System.Text.Encoding.UTF8));

                //收件者
                var CDA = await TecoApp.CustomerDepartmentAttribution.Where(p => p.CUSTOMERID == SMTPRequest.CustomerID).AsNoTracking().ToListAsync();
                var ManagerMail = CDA.FirstOrDefault().MANAGEREMAIL.Replace("/", "@").ToLower() + ".com.tw";
                var ManagerName = CDA.FirstOrDefault().MANAGERNAME;
                MailMsg.To.Add(new System.Net.Mail.MailAddress(ManagerMail, ManagerName, System.Text.Encoding.UTF8));

                //主旨
                string MailSubject = SMTPRequest.Subject;
                MailMsg.Subject = MailSubject;
                MailMsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //附件
                //MailMsg.Attachments.Add(new Attachment(new MemoryStream(file.BinContent), file.Name));

                #region 信件內容

                MailMsg.Body = SMTPRequest.Body;

                #endregion 信件內容

                MailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                MailMsg.IsBodyHtml = true;
                MailMsg.Priority = MailPriority.Low;

                #endregion 設定郵件內容

                #region 設定郵件物件與寄送

                logger.Info("/SMTP/SendMail Sendding Subject : {0}", MailSubject);

                using (System.Net.Mail.SmtpClient MailObj = new System.Net.Mail.SmtpClient()
                {
                    Credentials = new System.Net.NetworkCredential(SMTPAccount, SMTPPassword),
                    Host = "msa.hinet.net",
                    //Port = int.Parse(Port),
                    //EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                })
                {
                    MailObj.Send(MailMsg);
                    MailObj.Dispose();
                    MailMsg.Dispose();
                }

                logger.Info("/SMTP/SendMail Successful Subject : {0}", MailSubject);

                #endregion 設定郵件物件與寄送

                SMTPResponse.result = "1";
                SMTPResponse.message = "Success";
            }
            catch (Exception ex)
            {
                logger.Error("/SMTP/SendMail - " + ex.Message);
                SMTPResponse.result = "0";
                SMTPResponse.message = "Exception:/SMTP/SendMail:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return SMTPResponse;
        }

        #endregion 取得信限資料
    }
}
