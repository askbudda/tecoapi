﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class CustomerController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 取得客戶資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Get([FromBody] JObject DATA)
        {
            CustomerResponseModel CustomerResponse = new CustomerResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                CustomerRequestModel CustomerRequest = JsonConvert.DeserializeObject<CustomerRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == CustomerRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var CustomerDepartmentAttributionList = await TecoApp.CustomerDepartmentAttribution.Where(p => p.CUSTOMERID == CustomerRequest.ID).AsNoTracking().ToListAsync();
                    var DEPARTMENT = "";

                    //部門
                    if (CustomerDepartmentAttributionList.Count() > 0) 
                    {
                        DEPARTMENT = CustomerDepartmentAttributionList.FirstOrDefault().DEPARTMENTID;
                    }

                    //客戶資料
                    foreach (var KNA1Item in KNA1List)
                    {
                        var CheckExist = false;
                        foreach (var DataItem in CustomerResponse.data)
                        {
                            if (DataItem.TYPE == KNA1Item.VKORG)
                            {
                                DataItem.NAME = KNA1Item.NAME1;
                                DataItem.GROUP = KNA1Item.KDGRP;
                                DataItem.NOTE = KNA1Item.BEZEI;
                                DataItem.USER = KNA1Item.TXNAM_SDB;
                                DataItem.ADDRESS = KNA1Item.ORT01 + KNA1Item.STRAS;
                                DataItem.DEPARTMENT = DEPARTMENT;

                                CheckExist = true;
                                break;
                            }
                        }

                        if (!CheckExist)
                        {
                            CustomerResponse.data.Add(new CustomerModel()
                            {
                                ID = KNA1Item.KUNNR,
                                NAME = KNA1Item.NAME1,
                                TYPE = KNA1Item.VKORG,
                                GROUP = KNA1Item.KDGRP,
                                NOTE = KNA1Item.BEZEI,
                                USER = KNA1Item.TXNAM_SDB,
                                ADDRESS = KNA1Item.ORT01 + KNA1Item.STRAS,
                                DEPARTMENT = DEPARTMENT
                            });
                        }
                    }

                    CustomerResponse.result = "1";
                    CustomerResponse.message = "Success";
                }
                else
                {
                    CustomerResponse.result = "0";
                    CustomerResponse.message = "Error:/Customer/Get:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Customer/Get - " + ex.Message);
                CustomerResponse.result = "0";
                CustomerResponse.message = "Exception:/Customer/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return CustomerResponse;
        }

        #endregion 取得客戶資料
    }
}
