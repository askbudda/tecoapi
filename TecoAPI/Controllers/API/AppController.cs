﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using NLog;

namespace TecoAPI.Controllers.API
{
    public class AppController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        [CorsHandle]
        public bool CheckAppVersion(string deviceType, string version)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            var isValid = false;

            try
            {
                var type = (deviceType.ToLower() == "android") ? "App_Android" : "App_iOS";

                var isExist = (from item in TecoApp.APPVERSION
                                where item.Type == type && item.Number == version && item.IsActive == 1
                                select new
                                {
                                    item.Id
                                }).AsEnumerable().FirstOrDefault();

                if (isExist != null)
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("/App/CheckAppVersion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return isValid;
        }

        [HttpPost]
        [CorsHandle]
        public bool SetDeviceInfo([FromBody] JObject DATA)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            var isValid = false;

            try
            {
                var deviceId = DATA["deviceId"].ToString();

                var deviceInfo = (from item in TecoApp.DEVICEINFO
                                    where item.Id == deviceId
                                    select item).AsEnumerable().FirstOrDefault();

                if (deviceInfo == null)
                {
                    deviceInfo = new DEVICEINFO()
                    {
                        Id = DATA["deviceId"].ToString(),
                        UserId = DATA["userId"].ToString(),
                        DeviceType = DATA["deviceType"].ToString(),
                        DeviceModel = DATA["deviceModel"].ToString(),
                        DeviceResolution = DATA["deviceResolution"].ToString(),
                        DeviceVersion = DATA["deviceVersion"].ToString(),
                        DeviceManufacturer = DATA["deviceManufacturer"].ToString(),
                        DeviceToken = DATA["deviceToken"].ToString(),
                        AppVersion = DATA["appVersion"].ToString(),
                        LastTime = DateTime.Now
                    };

                    TecoApp.DEVICEINFO.Add(deviceInfo);
                }
                else
                {
                    deviceInfo.UserId = DATA["userId"].ToString();
                    deviceInfo.DeviceType = DATA["deviceType"].ToString();
                    deviceInfo.DeviceModel = DATA["deviceModel"].ToString();
                    deviceInfo.DeviceResolution = DATA["deviceResolution"].ToString();
                    deviceInfo.DeviceVersion = DATA["deviceVersion"].ToString();
                    deviceInfo.DeviceManufacturer = DATA["deviceManufacturer"].ToString();
                    deviceInfo.DeviceToken = DATA["deviceToken"].ToString();
                    deviceInfo.AppVersion = DATA["appVersion"].ToString();
                    deviceInfo.LastTime = DateTime.Now;

                    TecoApp.Entry(deviceInfo).State = EntityState.Modified;
                }

                TecoApp.SaveChanges();
                isValid = true;
            }
            catch (Exception ex)
            {
                logger.Error("/App/SetDeviceInfo - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return isValid;
        }
    }
}
