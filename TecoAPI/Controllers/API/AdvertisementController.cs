﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class AdvertisementController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 前臺

        #region 取得廣告資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<AQueryResponseModel>> Get()
        {
            var AResponse = new List<AQueryResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "AppImagePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value;
                var AList = await TecoApp.Advertisement.Where(p => p.Enable == "Y").AsNoTracking().ToListAsync();

                foreach (var A in AList)
                {
                    AResponse.Add(new AQueryResponseModel()
                    {
                        ID = A.ID,
                        Name = A.Name,
                        Type = A.Type,
                        Link = A.Link,
                        Image = RootFilePath + A.Image,
                        Enable = A.Enable
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Advertisement/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return AResponse;
        }

        #endregion 取得廣告資料

        #endregion 前臺

        #region 後臺

        #region 查詢廣告資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<AQueryResponseModel>> Query()
        {
            var AResponse = new List<AQueryResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var AList = await TecoApp.Advertisement.AsNoTracking().ToListAsync();

                foreach (var A in AList)
                {
                    AResponse.Add(new AQueryResponseModel()
                    {
                        ID = A.ID,
                        Name = A.Name,
                        Type = A.Type,
                        Link = A.Link,
                        Image = A.Image,
                        Enable = (A.Enable == "Y" ? "1" : "0")
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Advertisement/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return AResponse;
        }

        #endregion 查詢廣告資料

        #region 新增廣告資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<AQueryResponseModel>> Add()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootImagePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value;
                RootFilePath += "A\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var AAddRequest = new AAddRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        AAddRequest = JsonConvert.DeserializeObject<AAddRequestModel>(Value);
                    }
                }

                string FilePath = "";
                foreach (MultipartFileData File in StreamProvider.FileData)
                {
                    string FileName = File.Headers.ContentDisposition.FileName;
                    string FileNameNew = Path.GetFileName(File.LocalFileName);
                    if (FileName.StartsWith("\"") && FileName.EndsWith("\""))
                    {
                        FileName = FileName.Trim('"');
                    }
                    if (FileName.Contains(@"/") || FileName.Contains(@"\"))
                    {
                        FileName = Path.GetFileName(FileName);
                    }
                    FilePath = "Image/A/" + FileNameNew;
                }

                Advertisement log = new Advertisement()
                {
                    Name = AAddRequest.Name,
                    Type = AAddRequest.Type,
                    Link = AAddRequest.Link,
                    Image = FilePath,
                    Enable = (AAddRequest.Enable == "1" ? "Y" : "N"),
                    CREATE_TIME = System.DateTime.Now,
                    CREATE_USER = "System"
                };
                TecoApp.Advertisement.Add(log);

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Advertisement/Add - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 新增廣告資料

        #region 修改廣告資料

        [HttpPut]
        [CorsHandle]
        public async Task<List<AQueryResponseModel>> Modify()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootImagePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value;
                RootFilePath += "A\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var AModifyRequest = new AModifyRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        AModifyRequest = JsonConvert.DeserializeObject<AModifyRequestModel>(Value);
                    }
                }

                string FilePath = "";
                foreach (MultipartFileData File in StreamProvider.FileData)
                {
                    string FileName = File.Headers.ContentDisposition.FileName;
                    string FileNameNew = Path.GetFileName(File.LocalFileName);
                    if (FileName.StartsWith("\"") && FileName.EndsWith("\""))
                    {
                        FileName = FileName.Trim('"');
                    }
                    if (FileName.Contains(@"/") || FileName.Contains(@"\"))
                    {
                        FileName = Path.GetFileName(FileName);
                    }
                    FilePath = "Image/A/" + FileNameNew;
                }

                var Advertisement = await TecoApp.Advertisement.Where(x => x.ID == AModifyRequest.ID).FirstOrDefaultAsync();
                Advertisement.Name = AModifyRequest.Name.Trim();
                Advertisement.Type = AModifyRequest.Type.Trim();
                Advertisement.Link = AModifyRequest.Link.Trim();
                Advertisement.Enable = (AModifyRequest.Enable.Trim() == "1" ? "Y" : "N");
                Advertisement.UPDATE_TIME = System.DateTime.Now;
                Advertisement.UPDATE_USER = "System";
                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    Advertisement.Image = FilePath;
                }
                TecoApp.Entry(Advertisement).State = EntityState.Modified;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Advertisement/Modify - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 修改廣告資料

        #region 刪除廣告資料

        [HttpDelete]
        [CorsHandle]
        public async Task<List<AQueryResponseModel>> Delete(int id)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootImagePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value;

                var Advertisement = await TecoApp.Advertisement.Where(x => x.ID == id).FirstOrDefaultAsync();
                RootFilePath += "A\\" + Advertisement.Image.Replace("Image/A/", "");
                TecoApp.Advertisement.Remove(Advertisement);
                TecoApp.Entry(Advertisement).State = EntityState.Deleted;

                await TecoApp.SaveChangesAsync();

                System.IO.File.Delete(RootFilePath);
            }
            catch (Exception ex)
            {
                logger.Error("/Advertisement/Delete - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 刪除廣告資料

        #endregion 後臺
    }
}
