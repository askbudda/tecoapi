﻿using CsvHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class ProductRController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 前臺

        #region 取得商品資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Get([FromBody] JObject DATA)
        {
            ProductRResponseModel ProductRResponse = new ProductRResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                ProductRRequestModel ProductRRequest = JsonConvert.DeserializeObject<ProductRRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == ProductRRequest.ID).AsNoTracking().ToListAsync();

                //判斷客戶
                if (KNA1List.Count() > 0)
                {
                    var CType1 = false;
                    var CType2 = false;
                    var CTypeR = false;
                    foreach (var item in KNA1List)
                    {
                        if (item.KDGRP == "01")
                        {
                            CType1 = true;
                        }
                        if (item.KDGRP == "02")
                        {
                            CType2 = true;
                        }
                        if (item.VKORG.Contains("R"))
                        {
                            CTypeR = true;
                        }
                    }

                    //判斷客戶的商品權限
                    if (CType1 == false && CType2 == false)
                    {
                        ProductRResponse.result = "0";
                        ProductRResponse.message = "Error:/ProductR/Get:ID無對應商品:Type!=1&2";
                    }
                    else if (CTypeR == false)
                    {
                        ProductRResponse.result = "0";
                        ProductRResponse.message = "Error:/ProductR/Get:ID無對應商品:Group!=R";
                    }
                    else
                    {
                        var ProductRList = await TecoApp.ProductR.AsNoTracking().ToListAsync();

                        if (CType1 && CType2)
                        {
                            if (ProductRRequest.Type != "ALL")
                            {
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("1") && p.CUSTOMERTYPE.Contains("2") && p.TYPE == ProductRRequest.Type).AsNoTracking().ToListAsync();
                            }
                            else 
                            { 
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("1") && p.CUSTOMERTYPE.Contains("2")).AsNoTracking().ToListAsync();
                            }
                        }
                        else if (CType1)
                        {
                            if (ProductRRequest.Type != "ALL")
                            {
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("1") && p.TYPE == ProductRRequest.Type).AsNoTracking().ToListAsync();
                            }
                            else
                            {
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("1")).AsNoTracking().ToListAsync();
                            }
                        }
                        else if (CType2)
                        {
                            if (ProductRRequest.Type != "ALL")
                            {
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("2") && p.TYPE == ProductRRequest.Type).AsNoTracking().ToListAsync();
                            }
                            else
                            {
                                ProductRList = await TecoApp.ProductR.Where(p => p.CUSTOMERTYPE.Contains("2")).AsNoTracking().ToListAsync();
                            }
                        }

                        //判斷商品資料
                        if (ProductRList.Count() > 0)
                        {
                            var CUSTPRICEList = await TecoApp.CUSTPRICE.Where(p => p.KUNNR == "" || p.KUNNR == ProductRRequest.ID).AsNoTracking().ToListAsync();

                            foreach (var ProductRItem in ProductRList)
                            {
                                List<PRICEModel> PRODUCTPRICE = new List<PRICEModel>();
                                List<PRICEModel> RELATEDPRODUCTPRICE = new List<PRICEModel>();

                                #region 設定商品價格與關聯商品價格

                                var PRICER01v1 = "";
                                var PRICER01v2 = "";
                                var PRICER18v1 = "";
                                var PRICER18v2 = "";
                                var RELATEDPRICER01v1 = "";
                                var RELATEDPRICER01v2 = "";
                                var RELATEDPRICER18v1 = "";
                                var RELATEDPRICER18v2 = "";
                                foreach (var PRICEItem in CUSTPRICEList)
                                {
                                    if (PRICEItem.MATNR == ProductRItem.PRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "01" && PRICEItem.KUNNR == ProductRRequest.ID)
                                    {
                                        PRICER01v1 = PRICEItem.KAETR;
                                    }
                                    else if (PRICEItem.MATNR == ProductRItem.PRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "01" && PRICEItem.KUNNR == "")
                                    {
                                        PRICER01v2 = PRICEItem.KAETR;
                                    }
                                    else if (PRICEItem.MATNR == ProductRItem.PRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "18" && PRICEItem.KUNNR == ProductRRequest.ID)
                                    {
                                        PRICER18v1 = PRICEItem.KAETR;
                                    }
                                    else if (PRICEItem.MATNR == ProductRItem.PRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "18" && PRICEItem.KUNNR == "")
                                    {
                                        PRICER18v2 = PRICEItem.KAETR;
                                    }

                                    if (ProductRItem.RELATEDPRODUCTCODE.Length > 5)
                                    {
                                        if (PRICEItem.MATNR == ProductRItem.RELATEDPRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "01" && PRICEItem.KUNNR == ProductRRequest.ID)
                                        {
                                            RELATEDPRICER01v1 = PRICEItem.KAETR;
                                        }
                                        else if (PRICEItem.MATNR == ProductRItem.RELATEDPRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "01" && PRICEItem.KUNNR == "")
                                        {
                                            RELATEDPRICER01v2 = PRICEItem.KAETR;
                                        }
                                        else if (PRICEItem.MATNR == ProductRItem.RELATEDPRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "18" && PRICEItem.KUNNR == ProductRRequest.ID)
                                        {
                                            RELATEDPRICER18v1 = PRICEItem.KAETR;
                                        }
                                        else if (PRICEItem.MATNR == ProductRItem.RELATEDPRODUCTCODE && PRICEItem.VKORG == "R000" && PRICEItem.VTWEG == "18" && PRICEItem.KUNNR == "")
                                        {
                                            RELATEDPRICER18v2 = PRICEItem.KAETR;
                                        }
                                    }
                                }

                                #endregion 設定商品價格與關聯商品價格

                                #region 設定商品資料

                                PRODUCTPRICE.Add(new PRICEModel()
                                {
                                    TYPE = "01",
                                    PRICE = (PRICER01v1 != "" ? PRICER01v1 : PRICER01v2)
                                });
                                PRODUCTPRICE.Add(new PRICEModel()
                                {
                                    TYPE = "18",
                                    PRICE = (PRICER18v1 != "" ? PRICER18v1 : PRICER18v2)
                                });
                                if (ProductRItem.RELATEDPRODUCTCODE.Length > 5)
                                {
                                    RELATEDPRODUCTPRICE.Add(new PRICEModel()
                                    {
                                        TYPE = "01",
                                        PRICE = (RELATEDPRICER01v1 != "" ? RELATEDPRICER01v1 : RELATEDPRICER01v2)
                                    });
                                    RELATEDPRODUCTPRICE.Add(new PRICEModel()
                                    {
                                        TYPE = "18",
                                        PRICE = (RELATEDPRICER18v1 != "" ? RELATEDPRICER18v1 : RELATEDPRICER18v2)
                                    });
                                }

                                ProductRResponse.data.Add(new ProductRModel()
                                {
                                    TYPENAME = ProductRItem.TYPENAME,
                                    REFRIGERANT = ProductRItem.REFRIGERANT,
                                    POWER = ProductRItem.POWER,
                                    SQUAREMETERS = ProductRItem.SQUAREMETERS,
                                    DEHUMIDIFIERCAPACITY = ProductRItem.DEHUMIDIFIERCAPACITY,
                                    ENERGYEFFICIENCYRATING = ProductRItem.ENERGYEFFICIENCYRATING,
                                    SEASONALPERFORMANCE = ProductRItem.SEASONALPERFORMANCE,
                                    ELECTRICITYCONSUMPTIONPERYEAR = ProductRItem.ELECTRICITYCONSUMPTIONPERYEAR,
                                    ELECTRICCURRENT = ProductRItem.ELECTRICCURRENT,
                                    ELECTRICITYCONSUMPTION = ProductRItem.ELECTRICITYCONSUMPTION,
                                    PRODUCTCODE = ProductRItem.PRODUCTCODE,
                                    PRODUCTSIZE = ProductRItem.PRODUCTSIZE,
                                    PRODUCTWEIGHT = ProductRItem.PRODUCTWEIGHT,
                                    RELATEDPRODUCTCODE = ProductRItem.RELATEDPRODUCTCODE,
                                    RELATEDPRODUCTSIZE = ProductRItem.RELATEDPRODUCTSIZE,
                                    RELATEDPRODUCTWEIGHT = ProductRItem.RELATEDPRODUCTWEIGHT,
                                    LIQUIDPIPINGSIZE = ProductRItem.LIQUIDPIPINGSIZE,
                                    AIRPIPINGSIZE = ProductRItem.AIRPIPINGSIZE,
                                    OUTDOORFEETSIZE = ProductRItem.OUTDOORFEETSIZE,
                                    CUSTOMERTYPE = ProductRItem.CUSTOMERTYPE,
                                    TYPE = ProductRItem.TYPE,
                                    GIVEAWAY = ProductRItem.GIVEAWAY,
                                    TECOURL = ProductRItem.TECOURL,
                                    PRODUCTPRICE = PRODUCTPRICE,
                                    RELATEDPRODUCTPRICE = RELATEDPRODUCTPRICE,
                                    CREATE_TIME = ProductRItem.CREATE_TIME,
                                    CREATE_USER = ProductRItem.CREATE_USER
                                });

                                #endregion 設定商品資料
                            }

                            ProductRResponse.result = "1";
                            ProductRResponse.message = "Success";
                        }
                        else
                        {
                            ProductRResponse.result = "0";
                            ProductRResponse.message = "Error:/ProductR/Get:無商品資料";
                        }
                    }
                }
                else
                {
                    ProductRResponse.result = "0";
                    ProductRResponse.message = "Error:/ProductR/Get:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/ProductR/Get - " + ex.Message);
                ProductRResponse.result = "0";
                ProductRResponse.message = "Exception:/ProductR/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return ProductRResponse;
        }

        #endregion 取得商品資料

        #endregion 前臺

        #region 後臺

        #region 匯入商品(R)資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<PRResponseModel>> Import()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "PR\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                foreach (MultipartFileData File in StreamProvider.FileData)
                {
                    string FileName = File.Headers.ContentDisposition.FileName;
                    string FileNameNew = Path.GetFileName(File.LocalFileName);
                    string FilePath;
                    if (FileName.StartsWith("\"") && FileName.EndsWith("\""))
                    {
                        FileName = FileName.Trim('"');
                    }
                    if (FileName.Contains(@"/") || FileName.Contains(@"\"))
                    {
                        FileName = Path.GetFileName(FileName);
                    }
                    FilePath = RootFilePath + FileNameNew;

                    using (var StreamReader = new StreamReader(FilePath))
                    using (var CsvReader = new CsvReader(StreamReader))
                    {
                        CsvReader.Configuration.HasHeaderRecord = false; //不抓欄位名稱，中文會有問題
                        CsvReader.Read(); //跳過第一行欄位名稱
                        var PRUpload = CsvReader.GetRecords<PRUploadModel>().ToList();

                        var PRList = TecoApp.ProductR.ToList();
                        //var OldFirstID = (PRList.Count() == 0 ? 0 : Convert.ToInt32(PRList.FirstOrDefault().ID));
                        //var OldLastID = (PRList.Count() == 0 ? 0 : Convert.ToInt32(PRList.LastOrDefault().ID));

                        TecoApp.ProductR.RemoveRange(PRList);
                        TecoApp.SaveChanges();

                        logger.Debug("/ProductR/Import - RemoveRangeSuccess");

                        //設定新增的ID初始值
                        int ID = 1;
                        //if (OldFirstID < 10000)
                        //{
                        //    ID = OldLastID + 10000;
                        //}

                        //DB新增客戶部門歸屬清單
                        foreach (var PR in PRUpload)
                        {
                            ProductR log = new ProductR();
                            log.ID = ID;
                            log.TYPENAME = PR.TYPENAME.Trim();
                            log.REFRIGERANT = PR.REFRIGERANT.Trim();
                            log.POWER = PR.POWER.Trim();
                            log.SQUAREMETERS = PR.SQUAREMETERS.Trim();
                            log.DEHUMIDIFIERCAPACITY = PR.DEHUMIDIFIERCAPACITY.Trim();
                            log.ENERGYEFFICIENCYRATING = PR.ENERGYEFFICIENCYRATING.Trim();
                            log.SEASONALPERFORMANCE = PR.SEASONALPERFORMANCE.Trim();
                            log.ELECTRICITYCONSUMPTIONPERYEAR = PR.ELECTRICITYCONSUMPTIONPERYEAR.Trim();
                            log.ELECTRICCURRENT = PR.ELECTRICCURRENT.Trim();
                            log.ELECTRICITYCONSUMPTION = PR.ELECTRICITYCONSUMPTION.Trim();
                            log.PRODUCTCODE = PR.PRODUCTCODE.Trim();
                            log.PRODUCTSIZE = PR.PRODUCTSIZE.Trim();
                            log.PRODUCTWEIGHT = PR.PRODUCTWEIGHT.Trim();
                            log.RELATEDPRODUCTCODE = PR.RELATEDPRODUCTCODE.Trim();
                            log.RELATEDPRODUCTSIZE = PR.RELATEDPRODUCTSIZE.Trim();
                            log.RELATEDPRODUCTWEIGHT = PR.RELATEDPRODUCTWEIGHT.Trim();
                            log.LIQUIDPIPINGSIZE = PR.LIQUIDPIPINGSIZE.Trim();
                            log.AIRPIPINGSIZE = PR.AIRPIPINGSIZE.Trim();
                            log.OUTDOORFEETSIZE = PR.OUTDOORFEETSIZE.Trim();
                            log.CUSTOMERTYPE = PR.CUSTOMERTYPE.Trim();
                            log.TYPE = PR.TYPE.Trim();
                            log.GIVEAWAY = PR.GIVEAWAY.Trim();
                            log.TECOURL = PR.TECOURL.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.ProductR.Add(log);
                            ID++;
                        }

                        TecoApp.SaveChanges();

                        logger.Debug("/ProductR/Import - AddRangeSuccess");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/ProductR/Import - " + ex.Message + " - " + ex);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 匯入商品(R)資料

        #region 查詢商品(R)資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<PRResponseModel>> Query()
        {
            var PRResponse = new List<PRResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var PRList = await TecoApp.ProductR.AsNoTracking().ToListAsync();

                foreach (var PR in PRList)
                {
                    PRResponse.Add(new PRResponseModel()
                    {
                        ID = PR.ID,
                        TYPENAME = PR.TYPENAME,
                        REFRIGERANT = PR.REFRIGERANT,
                        POWER = PR.POWER,
                        SQUAREMETERS = PR.SQUAREMETERS,
                        DEHUMIDIFIERCAPACITY = PR.DEHUMIDIFIERCAPACITY,
                        ENERGYEFFICIENCYRATING = PR.ENERGYEFFICIENCYRATING,
                        SEASONALPERFORMANCE = PR.SEASONALPERFORMANCE,
                        ELECTRICITYCONSUMPTIONPERYEAR = PR.ELECTRICITYCONSUMPTIONPERYEAR,
                        ELECTRICCURRENT = PR.ELECTRICCURRENT,
                        ELECTRICITYCONSUMPTION = PR.ELECTRICITYCONSUMPTION,
                        PRODUCTCODE = PR.PRODUCTCODE,
                        PRODUCTSIZE = PR.PRODUCTSIZE,
                        PRODUCTWEIGHT = PR.PRODUCTWEIGHT,
                        RELATEDPRODUCTCODE = PR.RELATEDPRODUCTCODE,
                        RELATEDPRODUCTSIZE = PR.RELATEDPRODUCTSIZE,
                        RELATEDPRODUCTWEIGHT = PR.RELATEDPRODUCTWEIGHT,
                        LIQUIDPIPINGSIZE = PR.LIQUIDPIPINGSIZE,
                        AIRPIPINGSIZE = PR.AIRPIPINGSIZE,
                        OUTDOORFEETSIZE = PR.OUTDOORFEETSIZE,
                        CUSTOMERTYPE = PR.CUSTOMERTYPE,
                        TYPE = PR.TYPE,
                        GIVEAWAY = PR.GIVEAWAY,
                        TECOURL = PR.TECOURL
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/ProductR/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return PRResponse;
        }

        #endregion 查詢商品(R)資料

        #endregion 後臺
    }
}
