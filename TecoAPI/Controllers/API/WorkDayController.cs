﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TecoModels;
using System.Data.Entity;
using System.IO;
using CsvHelper;
using TecoAPI.Services;
using System.Configuration;
using System.Threading.Tasks;
using NLog;
using Newtonsoft.Json.Linq;

namespace TecoAPI.Controllers
{
    public class WorkDayController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        [CorsHandle]
        public Object Get([FromBody] JObject DATA)
        {
            WorkDayResponseModel WorkDayResponse = new WorkDayResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var now = DateTime.Now;
                var todayNum = 0;
                var transferDay = 0;
                var resDataCount = 14;

                bool isSuccess =  Int32.TryParse(DATA["transferDay"].ToString(), out transferDay);
                if (!isSuccess || transferDay < 0 || transferDay > 6)
                {
                    WorkDayResponse.result = "0";
                    WorkDayResponse.message = "error 移運日數錯誤";
                    return WorkDayResponse;
                }

                //判斷週六是否有送貨需要撈前一天
                if (now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday)
                {
                    //平日下午三點前可以選當天
                    if (now.Hour < 15)
                    {
                        Int32.TryParse(now.AddDays(-2).ToString("yyyyMMdd"), out todayNum);
                    } else
                    {
                        Int32.TryParse(now.AddDays(-1).ToString("yyyyMMdd"), out todayNum);
                    }
                }
                else if (now.DayOfWeek == DayOfWeek.Saturday)
                {
                    //週六中午十二點前可以選當天
                    if (now.Hour < 12)
                    {
                        Int32.TryParse(now.AddDays(-2).ToString("yyyyMMdd"), out todayNum);
                    }
                    else
                    {
                        Int32.TryParse(now.AddDays(-1).ToString("yyyyMMdd"), out todayNum);
                    }
                }
                else
                {
                    Int32.TryParse(now.AddDays(-1).ToString("yyyyMMdd"), out todayNum);
                }


                if (todayNum != 0)
                {
                    List<WORKDAY> WorkDayList = TecoApp.WORKDAY.Where(p => p.date > todayNum)
                                                                .OrderBy(x => x.date)
                                                                .Take(30)
                                                                .AsEnumerable().ToList();

                    var isFriHoliday = false;
                    for (var i = 0; i < WorkDayList.Count; i++)
                    {
                        //若五六日連假，則週六不上班
                        if (WorkDayList[i].weekDay == "五")
                        {
                            if (WorkDayList[i].type == 2)
                            {
                                isFriHoliday = true;
                            }
                            else
                            {
                                isFriHoliday = false;
                            }
                        }

                        //前一天為判斷第一天就是週六是否上班用，不回傳
                        if (i == 0) continue;

                        //扣除相當於移倉日期的前n個工作日，之後才回傳14天
                        if (WorkDayList[i].type == 0)
                        {
                            //工作日
                            if (transferDay > 0)
                            {
                                transferDay--;
                                continue;
                            }
                            else if (transferDay == 0)
                            {
                                //顯示的第一天要是工作日
                                WorkDayResponse.data.Add(new WorkDayModel()
                                {
                                    date = WorkDayList[i].date,
                                    weekDay = WorkDayList[i].weekDay,
                                    type = WorkDayList[i].type,
                                    memo = WorkDayList[i].memo,
                                });
                                transferDay--;
                                resDataCount--;
                            }
                            else if (resDataCount > 0)
                            {
                                WorkDayResponse.data.Add(new WorkDayModel()
                                {
                                    date = WorkDayList[i].date,
                                    weekDay = WorkDayList[i].weekDay,
                                    type = WorkDayList[i].type,
                                    memo = WorkDayList[i].memo,
                                });
                                resDataCount--;
                            }
                        }
                        else if (WorkDayList[i].weekDay == "六" && WorkDayList[i].type == 2 && !isFriHoliday)
                        {
                            //周六半天班
                            if (transferDay > 0)
                            {
                                transferDay--;
                                continue;
                            }
                            else if (transferDay == 0)
                            {
                                //顯示的第一天要是工作日
                                WorkDayResponse.data.Add(new WorkDayModel()
                                {
                                    date = WorkDayList[i].date,
                                    weekDay = WorkDayList[i].weekDay,
                                    type = 0,
                                    memo = WorkDayList[i].memo,
                                });
                                transferDay--;
                                resDataCount--;
                            }
                            else if (resDataCount > 0)
                            {
                                WorkDayResponse.data.Add(new WorkDayModel()
                                {
                                    date = WorkDayList[i].date,
                                    weekDay = WorkDayList[i].weekDay,
                                    type = 0,
                                    memo = WorkDayList[i].memo,
                                });
                                resDataCount--;
                            }
                        }
                        else
                        {
                            //假日
                            if (transferDay >= 0)
                            {
                                //顯示的第一天要是工作日
                                continue;
                            }
                            else if (resDataCount > 0)
                            {
                                WorkDayResponse.data.Add(new WorkDayModel()
                                {
                                    date = WorkDayList[i].date,
                                    weekDay = WorkDayList[i].weekDay,
                                    type = WorkDayList[i].type,
                                    memo = WorkDayList[i].memo,
                                });
                                resDataCount--;
                            }
                        }
                    }

                    WorkDayResponse.result = "1";
                    WorkDayResponse.message = "Success";
                }
                else
                {
                    WorkDayResponse.result = "0";
                    WorkDayResponse.message = "error 日期不符 " + now.ToString("YYYYMMDD");
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WorkDay/Get - " + ex.Message);
                WorkDayResponse.result = "0";
                WorkDayResponse.message = "Exception:/WorkDay/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return WorkDayResponse;
        }

        [HttpPost]
        [CorsHandle]
        public async Task<WorkDayResponseModel> uploadCSV()
        {
            TecoAppEntities db = null;
            //NotificationResBody notificationResBody = new NotificationResBody();
            WorkDayResponseModel WorkDayResponse = new WorkDayResponseModel();

            try
            {
                var now = DateTime.Now;
                var path = "";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                string root = ConfigurationManager.AppSettings["SaveWorkdayTempFilePath"];
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                CustomMultipartFormDataStreamProvider provider = new CustomMultipartFormDataStreamProvider(root);

                try
                {
                    db = new TecoAppEntities();
                    //NotificationReqBody notificationReqBody = new NotificationReqBody();
                    await Request.Content.ReadAsMultipartAsync(provider);
                    //foreach (var key in provider.FormData.AllKeys)
                    //{
                    //    foreach (var val in provider.FormData.GetValues(key))
                    //    {
                    //        notificationReqBody = JsonConvert.DeserializeObject<NotificationReqBody>(val);
                    //    }
                    //}

                    //if (notificationReqBody.type == TypeAllImmediate || notificationReqBody.type == TypeAllSchedule)
                    //{
                    //    StringBuilder builder = new StringBuilder();
                    //    builder.AppendLine(ConfigurationManager.AppSettings["PushAllTarget"]);
                    //    path = ConfigurationManager.AppSettings["SavePushFilePath"] + ConfigurationManager.AppSettings["PushAllFile"];
                    //    File.WriteAllText(path, builder.ToString());
                    //    logger.Info("TypeAllImmediate || TypeAllSchedule");
                    //}
                    //else
                    //{
                        var result = SaveFileService.UploadWorkDayFile(provider);
                        path = result.path;

                        //foreach (var userId in result.userIds)
                        //{
                        //    var orderNotification = new OrderNotification()
                        //    {
                        //        Title = notificationReqBody.title,
                        //        Message = notificationReqBody.message,
                        //        Type = 1,
                        //        CreateTime = now,
                        //        Tmcode = userId
                        //    };
                        //    db.OrderNotification.Add(orderNotification);
                        //}
                    //}

                    /*var Notification = new Notification()
                    {
                        Title = notificationReqBody.title,
                        Message = notificationReqBody.message,
                        CreateTime = now,
                        ScheduleTime = notificationReqBody.scheduleTime,
                        Type = notificationReqBody.type,
                        State = PrepareState,
                        FilePath = path
                    };
                    db.Notification.Add(Notification);
                    await db.SaveChangesAsync();

                    PushDirectModel pushDirectModel = new PushDirectModel();
                    pushDirectModel.id = Notification.Id;
                    pushDirectModel.title = notificationReqBody.title;
                    pushDirectModel.message = notificationReqBody.message;

                    HttpStatusCode pushStatusCode = new HttpStatusCode();

                    if (notificationReqBody.type == TypeAllSchedule || notificationReqBody.type == TypeListSchedule)
                    {
                        PushBatchModel pushBatchModel = new PushBatchModel();
                        pushBatchModel.pushnotification = pushDirectModel;

                        PushScheduleModel pushScheduleModel = new PushScheduleModel();
                        pushScheduleModel.batchpushobject = pushBatchModel;
                        pushScheduleModel.schedule = notificationReqBody.scheduleTime;
                        pushBatchModel.path = Notification.FilePath;

                        pushStatusCode = await PushNotificationService.PushSchedule(pushScheduleModel);
                    }
                    else
                    {
                        if (notificationReqBody.type == TypeAllImmediate)
                        {
                            List<string> targets = new List<string>();
                            targets.Add(ConfigurationManager.AppSettings["PushAllTarget"]);
                            pushDirectModel.targets = targets;
                            pushStatusCode = await PushNotificationService.Push(pushDirectModel);
                        }
                        else
                        {
                            PushBatchModel pushBatchModel = new PushBatchModel();
                            pushBatchModel.pushnotification = pushDirectModel;
                            pushBatchModel.path = Notification.FilePath;
                            pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);
                        }
                    }
                    if (pushStatusCode != HttpStatusCode.OK)
                    {
                        Notification.State = FailState;
                        db.Entry(Notification).State = EntityState.Modified;
                        notificationResBody.Error = "推播失敗";
                        logger.Error("Java Push Error: " + pushStatusCode);
                        await db.SaveChangesAsync();
                    }*/
                }
                catch (Exception ex)
                {
                    //logger.Error(ex.ToString());
                    //throw ex;
                    //notificationResBody.Error = ex.ToString();
                    WorkDayResponse.message = "系統忙碌中";
                }
                finally
                {
                    if (db != null) db.Dispose();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WorkDay/uploadCSV - " + ex.Message);
                WorkDayResponse.result = "0";
                WorkDayResponse.message = "Exception:/WorkDay/uploadCSV:" + ex.Message;
            }

            return WorkDayResponse;
        }

        [HttpGet]
        [CorsHandle]
        public Object ImportCSV()
        {
            WorkDayResponseModel WorkDayResponse = new WorkDayResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                try
                {
                    using (var reader = new StreamReader(@"C:\TecoWorkday\WorkdayTempFile\2021_test.csv"))
                    using (var csv = new CsvReader(reader))
                    {
                        csv.Configuration.HasHeaderRecord = false; //不抓欄位名稱，中文會有問題
                        csv.Read(); //跳過第一行欄位名稱
                        var records = csv.GetRecords<WorkDayModel>().ToList();

                        //檢查是否已有第一天資料
                        var tempData = records.ElementAt(0).date;
                        int tempYear = tempData / 10000;
                        int tempEnd = (tempYear + 1) * 10000;

                        //清除當年度資料
                        var remove = (from date in TecoApp.WORKDAY
                                      where date.date >= tempData && date.date < ((tempYear + 1) * 10000)
                                      select date).ToList();

                        if (remove != null)
                        {
                            foreach (var data in remove)
                            {
                                TecoApp.WORKDAY.Remove(data);
                            }
                            TecoApp.SaveChanges();
                        }

                        //寫入資料
                        foreach (var day in records)
                        {
                            var dateInfo = new WORKDAY()
                            {
                                date = day.date,
                                weekDay = day.weekDay,
                                type = day.type,
                                memo = day.memo,
                            };
                            TecoApp.WORKDAY.Add(dateInfo);
                        }
                        TecoApp.SaveChanges();

                        WorkDayResponse.result = "1";
                        WorkDayResponse.message = "Success";

                        //有第一天資料就不寫入
                        /*var firstDay = (from item in TecoApp.WORKDAY
                                               where item.date == tempData
                                               select item).ToList().FirstOrDefault();

                        if (firstDay == null)
                        {
                            foreach (var day in records)
                            {
                                var dateInfo = new WORKDAY()
                                {
                                    date = day.date,
                                    weekDay = day.weekDay,
                                    type = day.type,
                                    memo = day.memo,
                                };
                                TecoApp.WORKDAY.Add(dateInfo);
                            }
                            TecoApp.SaveChanges();

                            WorkDayResponse.result = "1";
                            WorkDayResponse.message = "Success123";
                        }
                        else
                        {

                            WorkDayResponse.result = "1";
                            WorkDayResponse.message = "already exist";
                        }*/
                    }
                }
                catch (Exception ex)
                {
                    WorkDayResponse.message = ex.ToString();
                }
                finally
                {
                    if (TecoApp != null) TecoApp.Dispose();
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WorkDay/ImportCSV - " + ex.Message);
                WorkDayResponse.result = "0";
                WorkDayResponse.message = "Exception:/WorkDay/ImportCSV:" + ex.Message;
            }

            return WorkDayResponse;
        }
    }
}
