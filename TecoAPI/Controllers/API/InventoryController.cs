﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class InventoryController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 取得庫存資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Get([FromBody] JObject DATA)
        {
            //設定是否即時查詢庫存
            bool SetMARDQueryOnTime = true;
            InventoryResponseModel InventoryResponse = new InventoryResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                InventoryRequestModel InventoryRequest = JsonConvert.DeserializeObject<InventoryRequestModel>(JsonConvert.SerializeObject(DATA));
                InventoryRequest.ADDRESS = InventoryRequest.ADDRESS.Replace("台", "臺");

                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;
                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == InventoryRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var WareHouseItemList = await TecoApp.WareHouseItem.AsNoTracking().ToListAsync();
                    var WareHouseWEIGHT = "";
                    var WareHouseID = "";

                    #region 設定倉庫資料

                    foreach (var WareHouseItem in WareHouseItemList)
                    {
                        if (InventoryRequest.ADDRESS.Contains(WareHouseItem.AREA))
                        {
                            WareHouseWEIGHT = WareHouseItem.WEIGHT;
                            WareHouseID = WareHouseItem.CODE;

                            if (WareHouseWEIGHT == "1")
                            {
                                break;
                            }
                        }
                    }

                    WareHouseItemList = await TecoApp.WareHouseItem.Where(p => p.CODE == WareHouseID).AsNoTracking().ToListAsync();

                    foreach (var WareHouseItem in WareHouseItemList)
                    {
                        if (InventoryRequest.ADDRESS.Contains(WareHouseItem.AREA))
                        {
                            WareHouseWEIGHT = WareHouseItem.WEIGHT;
                            WareHouseID = WareHouseItem.CODE;

                            if (WareHouseWEIGHT == "2")
                            {
                                break;
                            }
                        }
                    }

                    foreach (var WareHouseItem in WareHouseItemList)
                    {
                        if (InventoryRequest.ADDRESS.Contains(WareHouseItem.AREA))
                        {
                            WareHouseWEIGHT = WareHouseItem.WEIGHT;
                            WareHouseID = WareHouseItem.CODE;

                            if (WareHouseWEIGHT == "3")
                            {
                                break;
                            }
                        }
                    }

                    #endregion 設定倉庫資料

                    //判斷倉庫資料
                    if (WareHouseID == "")
                    {
                        InventoryResponse.result = "0";
                        InventoryResponse.message = "Error:/Inventory/Get:地址無對應倉庫";
                    }
                    else
                    {
                        //是否即時查詢庫存
                        if (SetMARDQueryOnTime)
                        {
                            #region 即時查詢庫存

                            //取得倉庫關聯設定
                            var WareHouseRelatedList = await TecoApp.WareHouseRelated.Where(p => p.ToWhere == WareHouseID).OrderBy(p => p.ShippingRate).ThenBy(p => p.DelayShippingTime).AsNoTracking().ToListAsync();

                            MARDRequestModel MARDRequestAll = new MARDRequestModel();
                            MARDRequestModel MARDRequestItem = new MARDRequestModel();
                            MARDResponseModel MARDResponse = new MARDResponseModel();

                            //取得所有倉庫編號
                            var WareHouseList = TecoApp.WareHouse.AsEnumerable().ToList();

                            #region 設定庫存查詢資料

                            foreach (var InventoryItem in InventoryRequest.data)
                            {
                                var PRODUCTTYPE = "";

                                if (InventoryItem.TYPE == "R000")
                                {
                                    PRODUCTTYPE = "15R0";
                                }
                                else if (InventoryItem.TYPE == "K000")
                                {
                                    PRODUCTTYPE = "12K0";
                                }

                                if (PRODUCTTYPE != "")
                                {
                                    MARDRequestAll.data.Add(new MARDQueryModel()
                                    {
                                        WERKS = PRODUCTTYPE,
                                        LGORT = WareHouseID,
                                        MATNR = InventoryItem.PRODUCTCODE
                                    });
                                }
                            }

                            #endregion 設定庫存查詢資料

                            #region 庫存查詢(全部倉庫)

                            bool FirstTime = true;
                            foreach (var ReqItem in MARDRequestAll.data)
                            {
                                if (!FirstTime)
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    MARDRequestItem.data.Clear();
                                }
                                else
                                {
                                    FirstTime = false;
                                }

                                foreach (var WareHouse in WareHouseList)
                                {
                                    MARDRequestItem.data.Add(new MARDQueryModel()
                                    {
                                        WERKS = ReqItem.WERKS,
                                        LGORT = WareHouse.CODE,
                                        MATNR = ReqItem.MATNR
                                    });
                                }

                                //RFC:庫存查詢
                                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARD/Get", JsonConvert.SerializeObject(MARDRequestItem));
                                MARDResponseModel ResponseItem = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetData));

                                if (ResponseItem.result == "1")
                                {
                                    double TotalCount = 0;
                                    string Detail = "";

                                    foreach (var WareHouseRelated in WareHouseRelatedList)
                                    {
                                        foreach (var RespItem in ResponseItem.data)
                                        {
                                            if (WareHouseRelated.FromWhere == RespItem.LGORT)
                                            {
                                                Detail += ";" + RespItem.LABST + "," + RespItem.LGORT + "," + WareHouseRelated.DelayShippingTime;                                                
                                                TotalCount += Convert.ToDouble(RespItem.LABST);
                                                break;
                                            }
                                        }
                                    }

                                    MARDResponse.data.Add(new MARDModel
                                    {
                                        WERKS = ReqItem.WERKS,
                                        LGORT = "All",
                                        MATNR = ReqItem.MATNR,
                                        MAKTX = Detail,
                                        LABST = TotalCount.ToString()
                                    });
                                }
                                else
                                {
                                    MARDResponse.data.Add(new MARDModel
                                    {
                                        WERKS = ReqItem.WERKS,
                                        LGORT = "All",
                                        MATNR = ReqItem.MATNR,
                                        MAKTX = "PostError",
                                        LABST = "X"
                                    });
                                }
                            }

                            #endregion 庫存查詢(全部倉庫)

                            #region 設定庫存資料

                            foreach (var InventoryItem in InventoryRequest.data)
                            {
                                var PRODUCTTYPE = "";

                                if (InventoryItem.TYPE == "R000")
                                {
                                    PRODUCTTYPE = "15R0";
                                }
                                else if (InventoryItem.TYPE == "K000")
                                {
                                    PRODUCTTYPE = "12K0";
                                }

                                if (PRODUCTTYPE != "")
                                {
                                    double InventoryItemQUANTITY = 0;
                                    bool QUANTITYTryParse = double.TryParse(InventoryItem.QUANTITY, out InventoryItemQUANTITY);

                                    if (QUANTITYTryParse && InventoryItemQUANTITY > 0)
                                    {
                                        bool CheckItem = false;

                                        foreach (var MARDItemResp in MARDResponse.data)
                                        {
                                            if (InventoryItem.PRODUCTCODE == MARDItemResp.MATNR)
                                            {
                                                CheckItem = true;

                                                //RFC異常
                                                if (MARDItemResp.MAKTX == "PostError")
                                                {
                                                    InventoryResponse.data.Add(new InventoryModel()
                                                    {
                                                        TYPE = InventoryItem.TYPE,
                                                        PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                        INVENTORY = "PostError",
                                                        TOTALINVENTORY = "",
                                                        SHIPPINGTIME = "X",
                                                        MAXSHIPPINGTIME = "X"
                                                    });
                                                }
                                                else
                                                {
                                                    //各倉庫庫存資料
                                                    List<ShippingModel> AllShipping = new List<ShippingModel>();
                                                    var ShippingDetailList = MARDItemResp.MAKTX.Split(';').ToList();
                                                    foreach (var ShippingDetail in ShippingDetailList)
                                                    {
                                                        if (!string.IsNullOrWhiteSpace(ShippingDetail))
                                                        {
                                                            AllShipping.Add(new ShippingModel() { 
                                                                Inventory = ShippingDetail.Split(',')[0],
                                                                WareHouse = ShippingDetail.Split(',')[1],
                                                                ShippingTime = ShippingDetail.Split(',')[2]
                                                            });
                                                        }
                                                    }

                                                    //本地倉庫存
                                                    string BaseShipping = "0";
                                                    foreach (var Shipping in AllShipping)
                                                    {
                                                        if (Shipping.WareHouse == WareHouseID)
                                                        {
                                                            BaseShipping = Shipping.Inventory.Replace(".000", "");
                                                            BaseShipping = (string.IsNullOrWhiteSpace(BaseShipping) ? "0" : BaseShipping);
                                                            break;
                                                        }
                                                    }

                                                    //總量判斷
                                                    if (InventoryItemQUANTITY <= Convert.ToDouble(MARDItemResp.LABST))
                                                    {
                                                        //移倉判斷
                                                        if (InventoryItemQUANTITY <= Convert.ToDouble(BaseShipping))
                                                        {
                                                            InventoryResponse.data.Add(new InventoryModel()
                                                            {
                                                                TYPE = InventoryItem.TYPE,
                                                                PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                                INVENTORY = BaseShipping,
                                                                TOTALINVENTORY = MARDItemResp.LABST,
                                                                SHIPPINGTIME = "0",
                                                                MAXSHIPPINGTIME = WareHouseRelatedList.LastOrDefault().DelayShippingTime
                                                            });
                                                        }
                                                        else
                                                        {
                                                            //運送日
                                                            var ShippingTime = "";
                                                            //需求數量
                                                            double DemandCount = InventoryItemQUANTITY;
                                                            foreach (var Shipping in AllShipping)
                                                            {
                                                                ShippingTime = Shipping.ShippingTime;
                                                                var InventoryTemp = Convert.ToDouble(Shipping.Inventory);

                                                                if (InventoryTemp >= DemandCount)
                                                                {
                                                                    break;
                                                                }
                                                                else
                                                                {
                                                                    DemandCount = DemandCount - InventoryTemp;
                                                                }
                                                            }

                                                            InventoryResponse.data.Add(new InventoryModel()
                                                            {
                                                                TYPE = InventoryItem.TYPE,
                                                                PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                                INVENTORY = BaseShipping,
                                                                TOTALINVENTORY = MARDItemResp.LABST,
                                                                SHIPPINGTIME = ShippingTime,
                                                                MAXSHIPPINGTIME = WareHouseRelatedList.LastOrDefault().DelayShippingTime
                                                            });
                                                        }
                                                    }
                                                    else
                                                    {
                                                        InventoryResponse.data.Add(new InventoryModel()
                                                        {
                                                            TYPE = InventoryItem.TYPE,
                                                            PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                            INVENTORY = BaseShipping,
                                                            TOTALINVENTORY = MARDItemResp.LABST,
                                                            SHIPPINGTIME = "X",
                                                            MAXSHIPPINGTIME = WareHouseRelatedList.LastOrDefault().DelayShippingTime
                                                        });
                                                    }
                                                }

                                                break;
                                            }
                                        }

                                        if (CheckItem == false)
                                        {
                                            InventoryResponse.data.Add(new InventoryModel()
                                            {
                                                TYPE = InventoryItem.TYPE,
                                                PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                INVENTORY = "NoneProduct",
                                                TOTALINVENTORY = "",
                                                SHIPPINGTIME = "X",
                                                MAXSHIPPINGTIME = "X"
                                            });
                                        }
                                    }
                                    else
                                    {
                                        InventoryResponse.data.Add(new InventoryModel()
                                        {
                                            TYPE = InventoryItem.TYPE,
                                            PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                            INVENTORY = "ErrorQuantity",
                                            TOTALINVENTORY = "",
                                            SHIPPINGTIME = "X",
                                            MAXSHIPPINGTIME = "X"
                                        });
                                    }
                                }
                                else
                                {
                                    InventoryResponse.data.Add(new InventoryModel()
                                    {
                                        TYPE = InventoryItem.TYPE,
                                        PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                        INVENTORY = "ErrorType",
                                        TOTALINVENTORY = "",
                                        SHIPPINGTIME = "X",
                                        MAXSHIPPINGTIME = "X"
                                    });
                                }
                            }

                            #endregion 設定庫存資料

                            #endregion 即時查詢庫存
                        }
                        else
                        {
                            //取得倉庫關聯設定
                            var WareHouseRelatedList = await TecoApp.WareHouseRelated.Where(p => p.ToWhere == WareHouseID).OrderByDescending(p => p.DelayShippingTime).AsNoTracking().ToListAsync();
                            var WareHouseShippingTime = WareHouseRelatedList.FirstOrDefault().DelayShippingTime;

                            #region 庫存DB(2小時更新)

                            var MARDList = await TecoApp.MARD.Where(p => p.LGORT == WareHouseID).AsNoTracking().ToListAsync();

                            foreach (var InventoryItem in InventoryRequest.data)
                            {
                                #region 設定庫存資料

                                var CheckItem = false;
                                var PRODUCTTYPE = "";
                                if (InventoryItem.TYPE == "R000")
                                {
                                    PRODUCTTYPE = "15R0";
                                }
                                else if (InventoryItem.TYPE == "K000")
                                {
                                    PRODUCTTYPE = "12K0";
                                }

                                if (PRODUCTTYPE != "")
                                {
                                    foreach (var MARDItem in MARDList)
                                    {
                                        if (PRODUCTTYPE == MARDItem.WERKS && InventoryItem.PRODUCTCODE == MARDItem.MATNR)
                                        {
                                            CheckItem = true;
                                            InventoryResponse.data.Add(new InventoryModel()
                                            {
                                                TYPE = InventoryItem.TYPE,
                                                PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                                INVENTORY = MARDItem.LABST,
                                                TOTALINVENTORY = "",
                                                SHIPPINGTIME = "",
                                                MAXSHIPPINGTIME = WareHouseShippingTime
                                            });
                                            break;
                                        }
                                    }

                                    if (CheckItem == false) 
                                    {
                                        InventoryResponse.data.Add(new InventoryModel()
                                        {
                                            TYPE = InventoryItem.TYPE,
                                            PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                            INVENTORY = "NoneProduct",
                                            TOTALINVENTORY = "",
                                            SHIPPINGTIME = "X",
                                            MAXSHIPPINGTIME = "X"
                                        });
                                    }
                                }
                                else 
                                {
                                    InventoryResponse.data.Add(new InventoryModel()
                                    {
                                        TYPE = InventoryItem.TYPE,
                                        PRODUCTCODE = InventoryItem.PRODUCTCODE,
                                        INVENTORY = "ErrorType",
                                        TOTALINVENTORY = "",
                                        SHIPPINGTIME = "X",
                                        MAXSHIPPINGTIME = "X"
                                    });
                                }

                                #endregion 設定庫存資料
                            }

                            #endregion 庫存DB(2小時更新)
                        }

                        InventoryResponse.result = "1";
                        InventoryResponse.message = "Success";
                    }
                }
                else
                {
                    InventoryResponse.result = "0";
                    InventoryResponse.message = "Error:/Inventory/Get:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Inventory/Get - " + ex.Message);
                InventoryResponse.result = "0";
                InventoryResponse.message = "Exception:/Inventory/Get:" + ex;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return InventoryResponse;
        }

        #endregion 取得庫存資料
    }
}
