﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class TokenController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private TokenManager _tokenManager;
        public TokenController()
        {
            _tokenManager = new TokenManager();
        }

        #region 前臺

        #region 登入

        [HttpPost]
        [CorsHandle]
        public async Task<SignInResponseModel> SignIn([FromBody] JObject DATA)
        {
            SignInResponseModel SignInResponse = new SignInResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                SignInRequestModel SignInRequest = JsonConvert.DeserializeObject<SignInRequestModel>(JsonConvert.SerializeObject(DATA));

                var ID = "D" + SignInRequest.ACCOUNT;

                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;

                var SignInList = await TecoApp.SignIn.Where(p => p.Account == SignInRequest.ACCOUNT && p.Password == SignInRequest.PASSWORD).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (SignInList.Count() == 0) 
                {
                    SignInResponse.result = "0";
                    SignInResponse.message = "Error:/Token/SignIn:帳號/密碼錯誤";
                }
                else
                {
                    Config = await TecoApp.Config.Where(p => p.Code == "Key" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Key = Config.FirstOrDefault().Value;
                    Config = await TecoApp.Config.Where(p => p.Code == "Exp" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Exp = Config.FirstOrDefault().Value;
                    var Data = await TecoApp.SignIn.Where(p => p.Account == SignInRequest.ACCOUNT && p.Password == SignInRequest.PASSWORD && p.Device == SignInRequest.DEVICE).AsNoTracking().ToListAsync();

                    UserModel User = new UserModel();

                    if (Data.Count() == 0)
                    {
                        User.ID = SignInList.FirstOrDefault().Account;
                        User.NAME = SignInList.FirstOrDefault().Name;
                        User.DEVICE = SignInRequest.DEVICE;
                    }
                    else 
                    { 
                        User.ID = Data.FirstOrDefault().Account;
                        User.NAME = Data.FirstOrDefault().Name;
                        User.DEVICE = SignInRequest.DEVICE;
                    }

                    var Token = _tokenManager.Get(User, Key, Convert.ToInt32(Exp));

                    if (Data.Count() == 0)
                    {
                        foreach (var SignInItem in SignInList)
                        {
                            SignInItem.Device = SignInRequest.DEVICE;
                            SignInItem.Refresh = Token.REFRESH;
                            SignInItem.UPDATE_TIME = System.DateTime.Now;
                            SignInItem.UPDATE_USER = "System";
                            TecoApp.Entry(SignInItem).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        foreach (var DataItem in Data) 
                        {
                            DataItem.Refresh = Token.REFRESH;
                            DataItem.UPDATE_TIME = System.DateTime.Now;
                            DataItem.UPDATE_USER = "System";
                            TecoApp.Entry(DataItem).State = EntityState.Modified;
                        }
                    }

                    await TecoApp.SaveChangesAsync();

                    #region TokenData

                    SignInResponse.TokenData = new TokenModel() { 
                        TOKEN = Token.TOKEN,
                        REFRESH  = Token.REFRESH,
                        EXP = Token.EXP
                    };

                    #endregion TokenData

                    #region CustomerData

                    CustomerRequestModel CustomerRequest = new CustomerRequestModel() {
                        ID = ID
                    };
                    var GetCustomerData = HttpClientHelper.POST_JSON(ServiceURL + "/API/Customer/Get", JsonConvert.SerializeObject(CustomerRequest));
                    CustomerResponseModel CustomerResponse = JsonConvert.DeserializeObject<CustomerResponseModel>(HttpClientHelper.ConvertString(GetCustomerData));

                    if (CustomerResponse.result == "1") 
                    {
                        SignInResponse.CustomerData = CustomerResponse.data;
                    }

                    #endregion CustomerData

                    #region CreditData

                    CreditRequestModel CreditRequest = new CreditRequestModel()
                    {
                        ID = ID
                    };
                    var GetCreditData = HttpClientHelper.POST_JSON(ServiceURL + "/API/Credit/Get", JsonConvert.SerializeObject(CreditRequest));
                    CreditResponseModel CreditResponse = JsonConvert.DeserializeObject<CreditResponseModel>(HttpClientHelper.ConvertString(GetCreditData));

                    if (CreditResponse.result == "1")
                    {
                        SignInResponse.CreditData = CreditResponse.data;
                    }

                    #endregion CreditData

                    #region ProductRData

                    ProductRRequestModel ProductRRequest = new ProductRRequestModel()
                    {
                        ID = ID,
                        Type = "ALL"
                    };
                    var GetProductRData = HttpClientHelper.POST_JSON(ServiceURL + "/API/ProductR/Get", JsonConvert.SerializeObject(ProductRRequest));
                    ProductRResponseModel ProductRResponse = JsonConvert.DeserializeObject<ProductRResponseModel>(HttpClientHelper.ConvertString(GetProductRData));

                    if (ProductRResponse.result == "1")
                    {
                        SignInResponse.ProductRData = ProductRResponse.data;
                    }

                    #endregion ProductRData

                    #region GiveawayData

                    GiveawayRequestModel GiveawayRequest = new GiveawayRequestModel()
                    {
                        ID = ID
                    };
                    var GetGiveawayData = HttpClientHelper.POST_JSON(ServiceURL + "/API/Giveaway/Get", JsonConvert.SerializeObject(GiveawayRequest));
                    GiveawayResponseModel GiveawayResponse = JsonConvert.DeserializeObject<GiveawayResponseModel>(HttpClientHelper.ConvertString(GetGiveawayData));

                    if (GiveawayResponse.result == "1")
                    {
                        SignInResponse.GiveawayData = GiveawayResponse.data;
                    }

                    #endregion GiveawayData

                    SignInResponse.result = "1";
                    SignInResponse.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Token/SignIn - " + ex.Message);
                SignInResponse.result = "0";
                SignInResponse.message = "Exception:/Token/SignIn:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return SignInResponse;
        }

        #endregion 登入

        #region 更新Token

        [HttpPost]
        [CorsHandle]
        public async Task<TokenRefreshResponseModel> Refresh([FromBody] JObject DATA)
        {
            TokenRefreshResponseModel TokenRefreshResponse = new TokenRefreshResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                TokenRefreshRequestModel TokenRefreshRequest = JsonConvert.DeserializeObject<TokenRefreshRequestModel>(JsonConvert.SerializeObject(DATA));

                var SignInList = await TecoApp.SignIn.Where(p => p.Account == TokenRefreshRequest.ACCOUNT && p.Device == TokenRefreshRequest.DEVICE && p.Refresh == TokenRefreshRequest.REFRESH).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (SignInList.Count() == 0)
                {
                    TokenRefreshResponse.result = "0";
                    TokenRefreshResponse.message = "Error:/Token/Refresh:帳號/設備/重置碼錯誤";
                }
                else
                {
                    var Config = await TecoApp.Config.Where(p => p.Code == "Key" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Key = Config.FirstOrDefault().Value;
                    Config = await TecoApp.Config.Where(p => p.Code == "Exp" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Exp = Config.FirstOrDefault().Value;

                    UserModel User = new UserModel();

                    User.ID = SignInList.FirstOrDefault().Account;
                    User.NAME = SignInList.FirstOrDefault().Name;
                    User.DEVICE = SignInList.FirstOrDefault().Device;

                    var Token = _tokenManager.Get(User, Key, Convert.ToInt32(Exp));

                    foreach (var SignInItem in SignInList)
                    {
                        SignInItem.Refresh = Token.REFRESH;
                        SignInItem.UPDATE_TIME = System.DateTime.Now;
                        SignInItem.UPDATE_USER = "System";
                        TecoApp.Entry(SignInItem).State = EntityState.Modified;
                    }

                    await TecoApp.SaveChangesAsync();

                    TokenRefreshResponse.data = new TokenModel()
                    {
                        TOKEN = Token.TOKEN,
                        REFRESH = Token.REFRESH,
                        EXP = Token.EXP
                    };
                    TokenRefreshResponse.result = "1";
                    TokenRefreshResponse.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Token/Refresh - " + ex.Message);
                TokenRefreshResponse.result = "0";
                TokenRefreshResponse.message = "Exception:/Token/Refresh:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return TokenRefreshResponse;
        }

        #endregion 更新Token

        #endregion 前臺

        #region 後臺

        #region 登入

        [HttpPost]
        [CorsHandle]
        public async Task<SignInManagerResponseModel> SignInManager()
        {
            SignInManagerResponseModel SignInManagerResponse = new SignInManagerResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "SI\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var SignInManagerRequest = new SignInManagerRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        SignInManagerRequest = JsonConvert.DeserializeObject<SignInManagerRequestModel>(Value);
                    }
                }

                var Manager = await TecoApp.Manager.Where(x => x.Account == SignInManagerRequest.ACCOUNT && x.Password == SignInManagerRequest.PASSWORD).FirstOrDefaultAsync();

                //判斷驗證是否成功
                if (string.IsNullOrWhiteSpace(Manager.Name))
                {
                    SignInManagerResponse.result = "0";
                    SignInManagerResponse.message = "Error:/Token/SignInManager:帳號/密碼錯誤";
                }
                else
                {
                    Config = await TecoApp.Config.Where(p => p.Code == "Key" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Key = Config.FirstOrDefault().Value;
                    Config = await TecoApp.Config.Where(p => p.Code == "Exp" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Exp = Config.FirstOrDefault().Value;

                    UserModel User = new UserModel();

                    User.ID = Manager.Account;
                    User.NAME = Manager.Name;
                    User.DEVICE = "Manage";

                    var Token = _tokenManager.Get(User, Key, Convert.ToInt32(Exp));

                    Manager.Refresh = Token.REFRESH;
                    Manager.UPDATE_TIME = System.DateTime.Now;
                    Manager.UPDATE_USER = "System";
                    TecoApp.Entry(Manager).State = EntityState.Modified;

                    await TecoApp.SaveChangesAsync();

                    #region TokenData

                    SignInManagerResponse.TokenData = new TokenModel()
                    {
                        TOKEN = Token.TOKEN,
                        REFRESH = Token.REFRESH,
                        EXP = Token.EXP
                    };

                    #endregion TokenData

                    SignInManagerResponse.result = "1";
                    SignInManagerResponse.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Token/SignInManager - " + ex.Message);
                SignInManagerResponse.result = "0";
                SignInManagerResponse.message = "Exception:/Token/SignInManager:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return SignInManagerResponse;
        }

        #endregion 登入

        #region 更新Token

        [HttpPost]
        [CorsHandle]
        public async Task<TokenRefreshManagerResponseModel> RefreshManager()
        {
            TokenRefreshManagerResponseModel TokenRefreshManagerResponse = new TokenRefreshManagerResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "SI\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var TokenRefreshManagerRequest = new TokenRefreshManagerRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        TokenRefreshManagerRequest = JsonConvert.DeserializeObject<TokenRefreshManagerRequestModel>(Value);
                    }
                }

                var Manager = await TecoApp.Manager.Where(x => x.Account == TokenRefreshManagerRequest.ACCOUNT && x.Refresh == TokenRefreshManagerRequest.REFRESH).FirstOrDefaultAsync();

                //判斷驗證是否成功
                if (string.IsNullOrWhiteSpace(Manager.Name))
                {
                    TokenRefreshManagerResponse.result = "0";
                    TokenRefreshManagerResponse.message = "Error:/Token/RefreshManager:帳號/重置碼錯誤";
                }
                else
                {
                    Config = await TecoApp.Config.Where(p => p.Code == "Key" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Key = Config.FirstOrDefault().Value;
                    Config = await TecoApp.Config.Where(p => p.Code == "Exp" && p.Enable == "Y").AsNoTracking().ToListAsync();
                    var Exp = Config.FirstOrDefault().Value;

                    UserModel User = new UserModel();

                    User.ID = Manager.Account;
                    User.NAME = Manager.Name;
                    User.DEVICE = "Manage";

                    var Token = _tokenManager.Get(User, Key, Convert.ToInt32(Exp));

                    Manager.Refresh = Token.REFRESH;
                    Manager.UPDATE_TIME = System.DateTime.Now;
                    Manager.UPDATE_USER = "System";
                    TecoApp.Entry(Manager).State = EntityState.Modified;

                    await TecoApp.SaveChangesAsync();

                    TokenRefreshManagerResponse.data = new TokenModel()
                    {
                        TOKEN = Token.TOKEN,
                        REFRESH = Token.REFRESH,
                        EXP = Token.EXP
                    };
                    TokenRefreshManagerResponse.result = "1";
                    TokenRefreshManagerResponse.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Token/RefreshManager - " + ex.Message);
                TokenRefreshManagerResponse.result = "0";
                TokenRefreshManagerResponse.message = "Exception:/Token/RefreshManager:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return TokenRefreshManagerResponse;
        }

        #endregion 更新Token

        #endregion 後臺
    }
}
