﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TecoAPI.Services;
using TecoModels.NotificationModel;
using TecoModels.NotificationModel.RequestBody;
using TecoModels.NotificationModel.ResponseBody;

namespace TecoAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("Notification")]
    public class NotificationApiController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        const int PrepareState = 1;
        const int PushingState = 2;
        const int DoneState = 3;
        const int CancelState = 4;
        const int FailState = 5;

        const int TypeAllImmediate = 0;
        const int TypeAllSchedule = 1;
        const int TypeListImmediate = 2;
        const int TypeListSchedule = 3;
        const int TypeOrderMessage = 4;

        [HttpGet]
        [Route("")]
        public async Task<List<NotificationResBody>> getNotifications()
        {
            var resBody = new List<NotificationResBody>();
            var notifications = new List<Notification>();

            TecoAppEntities db = null;
            try
            {
                db = new TecoAppEntities();

                notifications = await db.Notification.AsNoTracking().ToListAsync();

                foreach (var Notification in notifications)
                {
                    if (Notification.Type != TypeOrderMessage)
                    {
                        var notificationResBody = new NotificationResBody()
                        {
                            Id = Notification.Id,
                            Title = Notification.Title,
                            Message = Notification.Message,
                            CreateTime = Notification.CreateTime,
                            ScheduleTime = Notification.ScheduleTime,
                            PushTime = Notification.PushTime,
                            Type = Notification.Type,
                            State = Notification.State
                        };

                        resBody.Add(notificationResBody);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return resBody;
        }

        [HttpPost]
        [Route("")]
        public async Task<NotificationResBody> addNotification()
        {
            TecoAppEntities db = null;
            NotificationResBody notificationResBody = new NotificationResBody();
            var now = DateTime.Now;
            var path = "";

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string root = ConfigurationManager.AppSettings["SavePushTempFilePath"];
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            CustomMultipartFormDataStreamProvider provider = new CustomMultipartFormDataStreamProvider(root);

            try
            {
                db = new TecoAppEntities();
                NotificationReqBody notificationReqBody = new NotificationReqBody();
                await Request.Content.ReadAsMultipartAsync(provider);
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        notificationReqBody = JsonConvert.DeserializeObject<NotificationReqBody>(val);
                    }
                }

                if (notificationReqBody.type == TypeAllImmediate || notificationReqBody.type == TypeAllSchedule)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(ConfigurationManager.AppSettings["PushAllTarget"]);
                    path = ConfigurationManager.AppSettings["SavePushFilePath"] + ConfigurationManager.AppSettings["PushAllFile"];
                    File.WriteAllText(path, builder.ToString());
                    logger.Info("TypeAllImmediate || TypeAllSchedule");
                }
                else
                {
                    var result = SaveFileService.UploadPushFile(provider);
                    path = result.path;

                    foreach(var userId in result.userIds)
                    {
                        var orderNotification = new OrderNotification()
                        {
                            Title = notificationReqBody.title,
                            Message = notificationReqBody.message,
                            Type = 1,
                            CreateTime = now,
                            Tmcode = userId
                        };
                        db.OrderNotification.Add(orderNotification);
                    }
                }

                var Notification = new Notification()
                {
                    Title = notificationReqBody.title,
                    Message = notificationReqBody.message,
                    CreateTime = now,
                    ScheduleTime = notificationReqBody.scheduleTime,
                    Type = notificationReqBody.type,
                    State = PrepareState,
                    FilePath = path
                };
                db.Notification.Add(Notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = Notification.Id;
                pushDirectModel.title = notificationReqBody.title;
                pushDirectModel.message = notificationReqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                if (notificationReqBody.type == TypeAllSchedule || notificationReqBody.type == TypeListSchedule)
                {
                    PushBatchModel pushBatchModel = new PushBatchModel();
                    pushBatchModel.pushnotification = pushDirectModel;

                    PushScheduleModel pushScheduleModel = new PushScheduleModel();
                    pushScheduleModel.batchpushobject = pushBatchModel;
                    pushScheduleModel.schedule = notificationReqBody.scheduleTime;
                    pushBatchModel.path = Notification.FilePath;

                    pushStatusCode = await PushNotificationService.PushSchedule(pushScheduleModel);
                }
                else
                {
                    if (notificationReqBody.type == TypeAllImmediate)
                    {
                        List<string> targets = new List<string>();
                        targets.Add(ConfigurationManager.AppSettings["PushAllTarget"]);
                        pushDirectModel.targets = targets;
                        pushStatusCode = await PushNotificationService.Push(pushDirectModel);
                    }
                    else
                    {
                        PushBatchModel pushBatchModel = new PushBatchModel();
                        pushBatchModel.pushnotification = pushDirectModel;
                        pushBatchModel.path = Notification.FilePath;
                        pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);
                    }
                }
                if (pushStatusCode != HttpStatusCode.OK)
                {
                    Notification.State = FailState;
                    db.Entry(Notification).State = EntityState.Modified;
                    notificationResBody.Error = "推播失敗";
                    logger.Error("Java Push Error: " + pushStatusCode);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                //throw ex;
                //notificationResBody.Error = ex.ToString();
                notificationResBody.Error = "系統忙碌中";
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return notificationResBody;
        }

        [HttpPut]
        [Route("")]
        public async Task updateNotification(NotificationReqBody reqBody)
        {
            TecoAppEntities db = null;
            try
            {
                db = new TecoAppEntities();
                var Notification = await db.Notification.Where(x => x.Id == reqBody.id).FirstOrDefaultAsync();
                Notification.Message = reqBody.message;
                db.Entry(Notification).State = EntityState.Modified;

                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task deleteNotification(int id)
        {
            TecoAppEntities db = null;
            try
            {
                db = new TecoAppEntities();
                var Notification = await db.Notification.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (Notification.State == PrepareState)
                {
                    Notification.State = CancelState;
                    db.Entry(Notification).State = EntityState.Modified;
                }
                else
                {
                    db.Notification.Remove(Notification);
                    db.Entry(Notification).State = EntityState.Deleted;
                }
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
        }

        [HttpPost]
        [Route("orderMessage")]
        public async Task orderNotification(OrderMessageReqBody reqBody)
        {
            TecoAppEntities db = null;
            try
            {
                db = new TecoAppEntities();
                var path = await SaveFileService.SaveOrderMessage(reqBody.userIds);

                var Notification = new Notification()
                {
                    Title = reqBody.title,
                    Message = reqBody.message,
                    CreateTime = DateTime.Now,
                    Type = TypeOrderMessage,
                    State = PrepareState,
                    FilePath = path
                };
                db.Notification.Add(Notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = Notification.Id;
                pushDirectModel.title = reqBody.title;
                pushDirectModel.message = reqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                PushBatchModel pushBatchModel = new PushBatchModel();
                pushBatchModel.pushnotification = pushDirectModel;
                pushBatchModel.path = Notification.FilePath;
                pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);

                if (pushStatusCode != HttpStatusCode.OK)
                {
                    Notification.State = FailState;
                    db.Entry(Notification).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

        }

        [HttpPost]
        [Route("bookingMessage")]
        public async Task<Boolean> bookingMessage(BookingMessageReqBody reqBody)
        {
            TecoAppEntities db = null;
            PushResBody pushResBody = new PushResBody();
            try
            {
                db = new TecoAppEntities();
                var path = await SaveFileService.SaveOrderMessage(reqBody.userIds);

                var Notification = new Notification()
                {
                    Title = reqBody.title,
                    Message = reqBody.message,
                    CreateTime = DateTime.Now,
                    Type = TypeOrderMessage,
                    State = PrepareState,
                    FilePath = path
                };
                db.Notification.Add(Notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = Notification.Id;
                pushDirectModel.title = reqBody.title;
                pushDirectModel.message = reqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                PushBatchModel pushBatchModel = new PushBatchModel();
                pushBatchModel.pushnotification = pushDirectModel;
                pushBatchModel.path = Notification.FilePath;
                pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);

                if (pushStatusCode != HttpStatusCode.OK)
                {
                    pushResBody.isSuccess = false;
                    Notification.State = FailState;
                    db.Entry(Notification).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    foreach (var userId in reqBody.userIds)
                    {
                        var message = new OrderNotification()
                        {
                            Message = reqBody.message,
                            Title = reqBody.title,
                            CreateTime = DateTime.Now,
                            Tmcode = userId
                        };
                        db.OrderNotification.Add(message);
                    }
                    await db.SaveChangesAsync();

                    pushResBody.isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            if (pushResBody.isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
