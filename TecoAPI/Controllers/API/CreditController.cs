﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class CreditController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 取得信限資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Get([FromBody] JObject DATA)
        {
            CreditResponseModel CreditResponse = new CreditResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                CreditRequestModel CreditRequest = JsonConvert.DeserializeObject<CreditRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == CreditRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var CREDITList = await TecoApp.CUSTCREDIT.Where(p => p.KUNNR == CreditRequest.ID).AsNoTracking().ToListAsync();

                    //判斷信限資料
                    if (CREDITList.Count() > 0)
                    {
                        var ConfigCredit = await TecoApp.Config.Where(p => p.Code == "Credit" && p.Enable == "Y").AsNoTracking().ToListAsync();
                        var Credit = ConfigCredit.FirstOrDefault().Value;

                        foreach (var KNA1Item in KNA1List)
                        {
                            foreach (var CREDITItem in CREDITList)
                            {
                                var Total = Convert.ToDouble(CREDITItem.KLIMK);
                                var Used = Convert.ToDouble(CREDITItem.OBLIG);

                                var CheckExist = false;
                                foreach (var DataItem in CreditResponse.data)
                                {
                                    if (DataItem.TYPE == CREDITItem.KKBER)
                                    {
                                        DataItem.TOTAL = CREDITItem.KLIMK;
                                        DataItem.USED = CREDITItem.OBLIG;
                                        DataItem.PERCENTBASE = Credit;
                                        DataItem.REMAINING = ((Total - Used) / Total).ToString();

                                        CheckExist = true;
                                        break;
                                    }
                                }

                                if (!CheckExist)
                                {
                                    CreditResponse.data.Add(new CreditModel() { 
                                        TYPE = CREDITItem.KKBER,
                                        TOTAL = CREDITItem.KLIMK,
                                        USED = CREDITItem.OBLIG,
                                        PERCENTBASE = Credit,
                                        REMAINING = ((Total - Used) / Total).ToString()
                                    });
                                }
                            }
                        }

                        CreditResponse.result = "1";
                        CreditResponse.message = "Success";
                    }
                    else 
                    {
                        CreditResponse.result = "0";
                        CreditResponse.message = "Error:/Credit/Get:無信用資料";
                    }
                }
                else
                {
                    CreditResponse.result = "0";
                    CreditResponse.message = "Error:/Credit/Get:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Credit/Get - " + ex.Message);
                CreditResponse.result = "0";
                CreditResponse.message = "Exception:/Credit/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return CreditResponse;
        }

        #endregion 取得信限資料
    }
}
