﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class GiveawayController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 前臺

        #region 取得贈品資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Get([FromBody] JObject DATA)
        {
            GiveawayResponseModel GiveawayResponse = new GiveawayResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                GiveawayRequestModel GiveawayRequest = JsonConvert.DeserializeObject<GiveawayRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == GiveawayRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var CType1 = false;
                    var CType2 = false;
                    var CTypeR = false;
                    var CTypeK = false;
                    foreach (var item in KNA1List)
                    {
                        if (item.KDGRP == "01")
                        {
                            CType1 = true;
                        }
                        if (item.KDGRP == "02")
                        {
                            CType2 = true;
                        }
                        if (item.VKORG.Contains("R"))
                        {
                            CTypeR = true;
                        }
                        if (item.VKORG.Contains("K"))
                        {
                            CTypeK = true;
                        }
                    }

                    //判斷客戶的商品權限
                    if (CType1 == false && CType2 == false)
                    {
                        GiveawayResponse.result = "0";
                        GiveawayResponse.message = "Error:/Giveaway/Get:ID無對應商品:Type!=1&2";
                    }
                    else if (CTypeR == false && CTypeK == false)
                    {
                        GiveawayResponse.result = "0";
                        GiveawayResponse.message = "Error:/Giveaway/Get:ID無對應商品:Group!=R&K";
                    }
                    else
                    {
                        var GiveawayList = await TecoApp.Giveaway.Where(p => p.START_TIME <= DateTime.Now && p.END_TIME >= DateTime.Now).AsNoTracking().ToListAsync();

                        //判斷贈品資料
                        if (GiveawayList.Count() > 0)
                        {
                            foreach (var GiveawayItem in GiveawayList)
                            {
                                if ((GiveawayItem.TYPE.Contains("R") && CTypeR) ||
                                    (GiveawayItem.TYPE.Contains("K") && CTypeK))
                                {
                                    GiveawayResponse.data.Add(new GiveawayModel()
                                    {
                                        PRODUCTCODE = GiveawayItem.PRODUCTCODE,
                                        PRODUCTNAME = GiveawayItem.PRODUCTNAME,
                                        PRICE = GiveawayItem.PRICE,
                                    });
                                }
                            }

                            GiveawayResponse.result = "1";
                            GiveawayResponse.message = "Success";
                        }
                        else
                        {
                            GiveawayResponse.result = "0";
                            GiveawayResponse.message = "Error:/Giveaway/Get:無贈品資料";
                        }
                    }
                }
                else
                {
                    GiveawayResponse.result = "0";
                    GiveawayResponse.message = "Error:/Giveaway/Get:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Get - " + ex.Message);
                GiveawayResponse.result = "0";
                GiveawayResponse.message = "Exception:/Giveaway/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return GiveawayResponse;
        }

        #endregion 取得贈品資料

        #endregion 前臺

        #region 後臺

        #region 查詢贈品資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<GQueryResponseModel>> Query()
        {
            var GResponse = new List<GQueryResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var GList = await TecoApp.Giveaway.AsNoTracking().ToListAsync();

                foreach (var G in GList)
                {
                    GResponse.Add(new GQueryResponseModel()
                    {
                        ID = G.ID,
                        PRODUCTCODE = G.PRODUCTCODE,
                        PRODUCTNAME = G.PRODUCTNAME,
                        PRICE = G.PRICE,
                        TYPE = G.TYPE,
                        START_TIME = G.START_TIME,
                        END_TIME = G.END_TIME
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return GResponse;
        }

        #endregion 查詢贈品資料

        #region 新增贈品資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<GQueryResponseModel>> Add()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "G\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var GAddRequest = new GAddRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        GAddRequest = JsonConvert.DeserializeObject<GAddRequestModel>(Value);
                    }
                }

                Giveaway log = new Giveaway()
                {
                    ActivityID = 1,
                    PRODUCTCODE = GAddRequest.PRODUCTCODE.Trim(),
                    PRODUCTNAME = GAddRequest.PRODUCTNAME.Trim(),
                    PRICE = GAddRequest.PRICE.Trim(),
                    TYPE = GAddRequest.TYPE.Trim(),
                    START_TIME = GAddRequest.START_TIME,
                    END_TIME = GAddRequest.END_TIME,
                    CREATE_TIME = System.DateTime.Now,
                    CREATE_USER = "System"
                };
                TecoApp.Giveaway.Add(log);

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Add - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 新增贈品資料

        #region 修改贈品資料

        [HttpPut]
        [CorsHandle]
        public async Task<List<GQueryResponseModel>> Modify()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "G\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var GModifyRequest = new GModifyRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        GModifyRequest = JsonConvert.DeserializeObject<GModifyRequestModel>(Value);
                    }
                }

                var Giveaway = await TecoApp.Giveaway.Where(x => x.ID == GModifyRequest.ID).FirstOrDefaultAsync();
                Giveaway.PRODUCTCODE = GModifyRequest.PRODUCTCODE.Trim();
                Giveaway.PRODUCTNAME = GModifyRequest.PRODUCTNAME.Trim();
                Giveaway.PRICE = GModifyRequest.PRICE.Trim();
                Giveaway.TYPE = GModifyRequest.TYPE.Trim();
                Giveaway.START_TIME = GModifyRequest.START_TIME;
                Giveaway.END_TIME = GModifyRequest.END_TIME;
                Giveaway.UPDATE_TIME = System.DateTime.Now;
                Giveaway.UPDATE_USER = "System";
                TecoApp.Entry(Giveaway).State = EntityState.Modified;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Modify - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 修改贈品資料

        #region 刪除贈品資料

        [HttpDelete]
        [CorsHandle]
        public async Task<List<GQueryResponseModel>> Delete(int id)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Giveaway = await TecoApp.Giveaway.Where(x => x.ID == id).FirstOrDefaultAsync();
                TecoApp.Giveaway.Remove(Giveaway);
                TecoApp.Entry(Giveaway).State = EntityState.Deleted;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Delete - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 刪除贈品資料

        #endregion 後臺
    }
}
