﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Http;
using TecoModels;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using System.Threading.Tasks;
using System.Data.Entity;

namespace TecoAPI.Controllers
{
    public class DeliveryController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 取得貨運資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Status([FromBody] JObject DATA)
        {
            DeliveryResponseModel DeliveryResponse = new DeliveryResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                DeliveryRequestModel DeliveryRequest = JsonConvert.DeserializeObject<DeliveryRequestModel>(JsonConvert.SerializeObject(DATA));

                var Config = await TecoApp.Config.Where(p => p.Enable == "Y").AsNoTracking().ToListAsync();
                var DeliveryURL = "";
                var DeliveryFunction = "";
                var DeliveryUser = "";
                foreach (var ConfigItem in Config) 
                {
                    if (ConfigItem.Code == "DeliveryURL")
                    {
                        DeliveryURL = ConfigItem.Value;
                    }
                    else if (ConfigItem.Code == "DeliveryFunction") 
                    {
                        DeliveryFunction = ConfigItem.Value;
                    }
                    else if (ConfigItem.Code == "DeliveryUser")
                    {
                        DeliveryUser = ConfigItem.Value;
                    }
                }
                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == DeliveryRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    DeliveryRequestSendModel RequestSend = new DeliveryRequestSendModel();
                    RequestSend.HEADER.CREATEDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    RequestSend.BODY.QRY.QRYLIST.Add(new DeliveryRequestQryListModel() { 
                        CUSTNO = DeliveryUser,
                        EXTERNORDERKEY = DeliveryRequest.EXTERNORDERKEY
                    });

                    XNode RequestSendX = JsonConvert.DeserializeXNode(JsonConvert.SerializeObject(RequestSend), "DOC");

                    var GetData = HttpClientHelper.POST_XML(DeliveryURL + "/" + DeliveryFunction, "Xml=" + RequestSendX.ToString());

                    GetData = GetData.Substring(GetData.IndexOf("<DOC>"));
                    XmlDocument XDoc = new XmlDocument();
                    XDoc.LoadXml(GetData);
                    XmlNode XNode = XDoc.SelectSingleNode("DOC");
                
                    DeliveryResultModel ResultData = JsonConvert.DeserializeObject<DeliveryResultModel>(JsonConvert.SerializeXmlNode(XNode));

                    //判斷倉庫資料
                    if (ResultData.DOC.BODY.RETURNCODE == "000")
                    {
                        DeliveryResponse.data.Add(new DeliveryModel() { 
                            EXTERNORDERKEY = ResultData.DOC.BODY.QRYBAK.EXTERNORDERKEY,
                            WHNO = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.WHNO,
                            CUSTNO = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.CUSTNO,
                            SHTNO = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.SHTNO,
                            SSTS = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.SSTS,
                            DSTS = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.DSTS,
                            GIDT = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.GIDT,
                            RDEDT = ResultData.DOC.BODY.QRYBAK.QRYBAKLIST.RDEDT,
                            RETURNCODE = ResultData.DOC.BODY.RETURNCODE,
                            RETURNDESC = ResultData.DOC.BODY.RETURNDESC
                        });

                        DeliveryResponse.result = "1";
                        DeliveryResponse.message = "Success";
                    }
                    else
                    {
                        DeliveryResponse.result = "0";
                        DeliveryResponse.message = "Error:/Delivery/Status:" + ResultData.DOC.BODY.RETURNDESC;
                    }
                }
                else
                {
                    DeliveryResponse.result = "0";
                    DeliveryResponse.message = "Error:/Delivery/Status:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Delivery/Status - " + ex.Message);
                DeliveryResponse.result = "0";
                DeliveryResponse.message = "Exception:/Delivery/Status:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return DeliveryResponse;
        }

        #endregion 取得貨運資料
    }
}
