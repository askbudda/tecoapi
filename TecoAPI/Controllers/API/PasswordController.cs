﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class PasswordController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 修改密碼

        [HttpPost]
        [CorsHandle]
        public async Task<ResultModel> Modify([FromBody] JObject DATA)
        {
            ResultModel Result = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                PasswordModifyModel Password = JsonConvert.DeserializeObject<PasswordModifyModel>(JsonConvert.SerializeObject(DATA));

                var SignInList = await TecoApp.SignIn.Where(p => p.Account == Password.ACCOUNT && p.Password == Password.PASSWORDOLD).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (SignInList.Count() == 0) 
                {
                    Result.result = "0";
                    Result.message = "Error:/Password/Modify:帳號/密碼錯誤";
                }
                else
                {
                    foreach (var Data in SignInList) 
                    {
                        Data.Password = Password.PASSWORDNEW;
                        Data.UPDATE_TIME = System.DateTime.Now;
                        Data.UPDATE_USER = "System";
                        TecoApp.Entry(Data).State = EntityState.Modified;
                    }
                    await TecoApp.SaveChangesAsync();

                    Result.result = "1";
                    Result.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Password/Modify - " + ex.Message);
                Result.result = "0";
                Result.message = "Exception:/Password/Modify:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return Result;
        }

        #endregion 修改密碼

        #region 重置密碼

        [HttpPost]
        [CorsHandle]
        public async Task<ResultModel> Reset([FromBody] JObject DATA)
        {
            ResultModel Result = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                PasswordResetModel Account = JsonConvert.DeserializeObject<PasswordResetModel>(JsonConvert.SerializeObject(DATA));

                var SignInList = await TecoApp.SignIn.Where(p => p.Account == Account.ACCOUNT).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (SignInList.Count() == 0)
                {
                    Result.result = "0";
                    Result.message = "Error:/Password/Reset:帳號錯誤";
                }
                else
                {
                    foreach (var Data in SignInList) 
                    {
                        Data.Password = Account.ACCOUNT;
                        Data.UPDATE_TIME = System.DateTime.Now;
                        Data.UPDATE_USER = "System";
                        TecoApp.Entry(Data).State = EntityState.Modified;
                    }
                    await TecoApp.SaveChangesAsync();

                    Result.result = "1";
                    Result.message = "Success";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Password/Reset - " + ex.Message);
                Result.result = "0";
                Result.message = "Exception:/Password/Reset:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return Result;
        }

        #endregion 重置密碼
    }
}
