﻿using Newtonsoft.Json;
using System;
using System.Text;
using TecoModels;

namespace TecoAPI
{
    public class TokenManager
    {
        public TokenModel Get(UserModel User, string Key, int Exp)
        {
            //使用者資訊,過期時間
            var Payload = new PayloadModel
            {
                INFO = User,
                EXP = Convert.ToInt32((DateTime.Now.AddSeconds(Exp) - new DateTime(1970, 1, 1)).TotalSeconds)
            };

            var Json = JsonConvert.SerializeObject(Payload);
            var Base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(Json));
            var IV = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);

            //使用 AES 加密 Payload
            var ENCRYPT = TokenCrypto.AESEncrypt(Base64, Key.Substring(0, 16), IV);

            //取得簽章
            var SIGNATURE = TokenCrypto.ComputeHMACSHA256(IV + "." + ENCRYPT, Key.Substring(0, 64));

            return new TokenModel
            {
                //Token 為 iv + encrypt + signature，並用 . 串聯
                TOKEN = IV + "." + ENCRYPT + "." + SIGNATURE,
                //Refresh Token 使用 Guid 產生
                REFRESH = Guid.NewGuid().ToString().Replace("-", ""),
                EXP = Exp
            };
        }

        public UserModel Verify(string Token, string Key)
        {
            var TokenList = Token.Split('.');
            var IV = TokenList[0];
            var ENCRYPT = TokenList[1];
            var SIGNATURE = TokenList[2];

            //檢查簽章是否正確
            if (SIGNATURE != TokenCrypto.ComputeHMACSHA256(IV + "." + ENCRYPT, Key.Substring(0, 64)))
            {
                return null;
            }

            //使用 AES 解密 Payload
            var Base64 = TokenCrypto.AESDecrypt(ENCRYPT, Key.Substring(0, 16), IV);
            var Json = Encoding.UTF8.GetString(Convert.FromBase64String(Base64));
            var Payload = JsonConvert.DeserializeObject<PayloadModel>(Json);

            //檢查是否過期
            if (Payload.EXP < Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds))
            {
                return null;
            }

            return Payload.INFO;
        }
    }
}