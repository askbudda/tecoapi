import { AlertService } from './services/alertService';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(
        private alertService: AlertService) { }
    public isLogin: boolean;
    public loginData;
    ngOnInit() {
        this.isLogin = false;
        this.alertService.getLoginState().subscribe((isLogin) => {
            this.isLogin = isLogin;
            if (this.isLogin) {
                this.loginData = JSON.parse(localStorage.getItem('loginData'));
            }
        });
    }
    public logout() {
        this.isLogin = false;
    }
}
