export class Constant {
    public static TECO_API = 'http://203.75.177.118/api/';
    // public static TECO_API = 'http://10.211.55.3:44339/api/';

    public static NotificationType = {
        ALL: '0',
        LIST: '1'
    };
    public static NotificationState = {
        PREPARE: 1,
        PUSHING: 2,
        DONE: 3,
        CANCEL: 4,
        FAIL: 5,
    };
    public static NotificationPushType = {
        IMMEDIATE: '0',
        SCHEDULE: '1'
    };
    public static ContactStatus = {
        UNPROCESS: '未處理',
        INPROCESS: '處理中',
        PROCESSED: '已處理'
    };
    public static PushType = {
        IMMEDIATE: '0',
        SCHEDULE: '1'
    };
    public static ImageLimit = 256000;

    public static AdvertisementStateType = {
        STATE_NO_REMINDER_OUT_OPEN: 0,
        STATE_NO_REMINDER_IN_OPEN: 1,
        STATE_REMINDER_OUT_OPEN: 2,
        STATE_REMINDER_IN_OPEN: 3,
        STATE_CLOSE: 4
    };
}