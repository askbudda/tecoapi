import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AlertService } from '../services/alertService';
import { MenuService } from '../services/menuService';
import * as _ from "lodash";
import * as moment from 'moment';

@Injectable()
export class Auth implements CanActivate {
    constructor(
        private router: Router,
        private alertService: AlertService,
        private menuService: MenuService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('token') && localStorage.getItem('loginData')) {
            let loginData = JSON.parse(localStorage.getItem('loginData'));
            //let daysOfChangePassword = moment.duration(moment().diff(moment(loginData.passwordUpdateTime), 'days'));
            this.alertService.refreshLoginState(true);
            // if (daysOfChangePassword.milliseconds() > 30) {
            //     setTimeout(() => this.alertService.error('已超過30天未更改密碼，請修改後重新登入'), 0)
            //     if (state.url == '/changePassword') {
            //         return true;
            //     } else {
            //         this.router.navigate(['/changePassword']);
            //         return false;
            //     }
            // }
            this.menuService.refreshMenus(loginData.menus);
            if (_.find(loginData.menus, function (menu) { return menu.url == state.url; }) || state.url === '/home') {
                return true;
            } else {
                this.alertService.error('未授權的功能');
                return false;
            }
        }
        this.router.navigate(['/login']);
        return false;
    }
}