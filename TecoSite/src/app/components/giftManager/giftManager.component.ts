import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Inject } from '@angular/core';
import { Validators, ValidatorFn, FormBuilder, FormGroup, FormControl, AbstractControl, RadioControlValueAccessor } from '@angular/forms';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { IntlService } from '@progress/kendo-angular-intl';
import { Constant } from '../../app.constant';
//import { NotificationService } from '../../services/notificationService';
import { GiftService } from '../../services/giftService';
import { Gift } from '../../models/gift';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import swal from 'sweetalert2'

@Component({
	selector: 'app-root',
	templateUrl: './giftManager.component.html',
	styleUrls: ['./giftManager.component.css']
})
export class GiftManagerComponent implements OnInit {
	public gridState: State = {
		sort: [{ field: 'Id', dir: 'desc' }],
		skip: 0,
		take: 10
	};
	public active = false;
	private editedRowIndex: number;
	public editForm: FormGroup;
    public editDataItem: Gift;
    public isNew: boolean;
	public notifications: Gift[];
	public ScheduleTime: Date = new Date();
	public comfirm: Boolean = false;
	public files;

	//public gifts: Gift[];

	constructor(private giftService: GiftService, private spinner: NgxSpinnerService, public intl: IntlService) { }

	public ngOnInit(): void {
		/*this.gifts = [
			{ ID: 1, PRODUCTCODE: "AAA", PRODUCTNAME: "空氣清淨機", PRICE: "3000", CREATE_TIME: null, CREATE_USER: "system" },
			{ ID: 2, PRODUCTCODE: "BBB", PRODUCTNAME: "立扇", PRICE: "3000", CREATE_TIME: null, CREATE_USER: "system" },
			{ ID: 3, PRODUCTCODE: "CCC", PRODUCTNAME: "熱水瓶", PRICE: "3000", CREATE_TIME: null, CREATE_USER: "system" },
			{ ID: 4, PRODUCTCODE: "DDD", PRODUCTNAME: "手持吸塵器", PRICE: "3000", CREATE_TIME: null, CREATE_USER: "system" },
			{ ID: 5, PRODUCTCODE: "EEE", PRODUCTNAME: "氣炸鍋", PRICE: "3000", CREATE_TIME: null, CREATE_USER: "system" },
		];*/

		this.editForm = new FormGroup({
			'ID': new FormControl(''),
			'PRODUCTCODE': new FormControl('', Validators.required),
			'PRODUCTNAME': new FormControl('', Validators.required),
			'PRICE': new FormControl(0),
			'TYPE': new FormControl('', Validators.required),
			'START_TIME': new FormControl('', Validators.required),
			'END_TIME': new FormControl('', Validators.required),
		});

		/*this.editForm = new FormGroup({
			'creditLimit': new FormControl(12.5, Validators.compose([Validators.min(1), Validators.max(100), Validators.required])),
		});*/

		this.getNotifications();
	}

	getNotifications(): void {
		this.spinner.show();
		this.giftService.getNotifications()
			.subscribe(notifications => {
				console.log(notifications);
				this.spinner.hide();
				this.notifications = notifications
			});
	}

	addHandler(data) {
		this.editForm = new FormGroup({
			'ID': new FormControl(''),
			'PRODUCTCODE': new FormControl('', Validators.required),
			'PRODUCTNAME': new FormControl('', Validators.required),
			'PRICE': new FormControl(0),
			'TYPE': new FormControl("K000", Validators.required),
			'START_TIME': new FormControl('', Validators.required),
			'END_TIME': new FormControl('', Validators.required),
		});
		this.isNew = true;
		this.active = true;
    }

    editHandler({ dataItem }) {
		console.log("edit");
		
		/*console.log(dataItem);
		//this.editForm.reset(dataItem);
		this.editForm.get("ID").setValue(dataItem.ID);
		this.editForm.get("PRODUCTCODE").setValue(dataItem.PRODUCTCODE);
		this.editForm.get("PRODUCTNAME").setValue(dataItem.PRODUCTNAME);
		this.editForm.get("PRICE").setValue(dataItem.PRICE);
		this.editForm.get("TYPE").setValue(dataItem.TYPE);
		let tempStartTime = new Date(dataItem.START_TIME);
		console.log(tempStartTime);
		this.editForm.get("START_TIME").setValue(tempStartTime);
		let tempEndTime = new Date(dataItem.END_TIME);
		console.log(tempEndTime);
		this.editForm.get("END_TIME").setValue(tempEndTime);
		//this.editForm.get("END_TIME").setValue(this.intl.parseDate(dataItem.END_TIME));
		this.isNew = false;
		this.active = true;*/

		let tempData = new Gift;
		tempData.ID = dataItem.ID;
		tempData.PRODUCTCODE = dataItem.PRODUCTCODE;
		tempData.PRODUCTNAME = dataItem.PRODUCTNAME;
		tempData.PRICE = dataItem.PRICE;
		tempData.TYPE = dataItem.TYPE;
		tempData.START_TIME = new Date(dataItem.START_TIME);
		tempData.END_TIME = new Date(dataItem.END_TIME);
		this.editForm.reset(tempData);
		
		this.isNew = false;
		this.active = true;
		console.log("edit end");
    }

	removeHandler({ dataItem }) {
		console.log(dataItem);
		this.giftService.deleteNotification(dataItem)
			.subscribe(() => this.getNotifications());
	}

	onStateChange(state: State) {
		this.gridState = state;
	}

	private closeForm(): void {
		this.active = false;
		this.editForm.reset();
	}

	public onSave(e): void {

		if (this.isNew) {
			//this.comfirm = true;
			console.log("submit add");
			console.log(this.editForm.value);
			this.active = false;
			this.spinner.show();
			this.giftService.pushNotification(this.editForm.value, this.isNew)
				.subscribe((value) => {
					this.spinner.hide();
					this.getNotifications();
					this.active = false;
				});
		} else {
			console.log("submit edit");
			this.active = false;
			this.spinner.show();
			this.giftService.pushNotification(this.editForm.value, this.isNew)
				.subscribe((value) => {
					this.spinner.hide();
					this.getNotifications();
					this.active = false;
				});
		}
	}

	public onCancel(e): void {
		this.active = false;
	}

	public onComfirmCancel(e): void {
		e.preventDefault();
		this.closeComfirmForm();
	}

	private closeComfirmForm(): void {
		this.comfirm = false;
	}

	public onComfirmSave(e): void {
		console.log("submit");
		e.preventDefault();
		//this.editForm.get('Type').setValue(parseInt((this.editForm.value['Range'] + this.editForm.value['Way']), 2));
		//this.editForm.value['excelFile'] = this.files;
		//console.log(this.editForm.value);
		/*if (this.editForm.value['Way'] === Constant.PushType.IMMEDIATE) {
			delete this.editForm.value['ScheduleTime'];
		}*/
		this.spinner.show();
		/*this.notificationService.pushNotification(this.editForm.value, this.isNew)
			.subscribe((value) => {
				this.spinner.hide();
				if (value.Error) {
					swal({
						title: "推播失敗",
						type: "error",
					}).then((value) => {
						this.getNotifications();
					});
				} else {
					swal({
						title: "推播成功",
						type: "success",
					}).then((value) => {
						this.getNotifications();
					});
				}
			});*/
			
			/*this.workdayService.pushNotification(this.editForm.value, this.isNew)
			.subscribe((value) => {
				this.spinner.hide();
				if (value.Error) {
					swal({
						title: "推播失敗",
						type: "error",
					}).then((value) => {
						//this.getNotifications();
					});
				} else {
					swal({
						title: "推播成功",
						type: "success",
					}).then((value) => {
						//this.getNotifications();
					});
				}
			});*/
		this.active = false;
		this.comfirm = false;
	}
}
