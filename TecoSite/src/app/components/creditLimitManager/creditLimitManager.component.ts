import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Inject } from '@angular/core';
import { Validators, ValidatorFn, FormBuilder, FormGroup, FormControl, AbstractControl, RadioControlValueAccessor } from '@angular/forms';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { Constant } from '../../app.constant';
//import { NotificationService } from '../../services/notificationService';
import { CreditLimitService } from '../../services/creditLimitService';
import { CreditLimit } from '../../models/creditLimit';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import * as _ from "lodash";
import swal from 'sweetalert2'

@Component({
	selector: 'app-root',
	templateUrl: './creditLimitManager.component.html',
	styleUrls: ['./creditLimitManager.component.css']
})
export class CreditLimitManagerComponent implements OnInit {
	public gridState: State = {
		sort: [{ field: 'Id', dir: 'desc' }],
		skip: 0,
		take: 10
	};
	public active = false;
	private editedRowIndex: number;
	public editForm: FormGroup;
	public isNew: boolean;
	public notifications: CreditLimit[];
	public ScheduleTime: Date = new Date();
	public comfirm: Boolean = false;
	public files;

	public creditLimit;

	constructor(private creditLimitService: CreditLimitService, private spinner: NgxSpinnerService) { }

	public ngOnInit(): void {
		this.editForm = new FormGroup({
			'ID': new FormControl(''),
			'Value': new FormControl('', Validators.compose([Validators.max(1), Validators.required])),
			'Code': new FormControl(''),
			'Name': new FormControl(''),
			'Enable': new FormControl(''),
		});

		this.getNotifications();
	}

	getNotifications(): void {
		this.spinner.show();
		this.creditLimitService.getNotifications()
			.subscribe(notifications => {
				this.spinner.hide();
				this.notifications = notifications
				console.log(this.notifications);

				this.creditLimit = _.find(this.notifications, { 'Code': "Credit" });
				console.log(this.creditLimit);
				this.editForm.reset(this.creditLimit);
				//this.editForm.get('ID').setValue(this.creditLimit['ID']);
				//this.editForm.get('Value').setValue(this.creditLimit['Value']);
			});
	}


	/*removeHandler({ dataItem }) {
		console.log(dataItem);
		this.notificationService.deleteNotification(dataItem)
			.subscribe(() => this.getNotifications());
	}*/

	onFileChange(event) {
		if (event.target.files && event.target.files.length >= 1) {
			this.files = event.target.files;
			console.log(this.files[0].name);
			this.editForm.get('hiddenExcelFile').setValue(this.files[0].name);
		}
	}

	onStateChange(state: State) {
		this.gridState = state;
	}

	private closeForm(): void {
		this.active = false;
	}

	public onSave(e): void {
		console.log("on save");
		console.log(this.editForm);

		if (!this.editForm.valid) {
			return;
		}

		this.isNew = false;
		/*if (this.isNew) {
			this.comfirm = true;
		} else {
			this.spinner.show();
			this.notificationService.pushNotification(this.editForm.value, this.isNew)
				.subscribe((value) => {
					this.spinner.hide();
					this.getNotifications();
					this.active = false;
				});
		}*/

		this.spinner.show();
		this.creditLimitService.pushNotification(this.editForm.value, this.isNew)
		.subscribe((value) => {
			this.spinner.hide();
			if (value.Error) {
				swal({
					title: "設定失敗",
					type: "error",
				}).then((value) => {
					this.getNotifications();
				});
			} else {
				swal({
					title: "設定成功",
					type: "success",
				}).then((value) => {
					this.getNotifications();
				});
			}
		});
	}

	public onCancel(e): void {
		this.active = false;
	}

	public onComfirmCancel(e): void {
		e.preventDefault();
		this.closeComfirmForm();
	}

	private closeComfirmForm(): void {
		this.comfirm = false;
	}

	public onComfirmSave(e): void {
		e.preventDefault();
		//this.editForm.get('Type').setValue(parseInt((this.editForm.value['Range'] + this.editForm.value['Way']), 2));
		this.editForm.value['excelFile'] = this.files;
		console.log(this.editForm.value);
		/*if (this.editForm.value['Way'] === Constant.PushType.IMMEDIATE) {
			delete this.editForm.value['ScheduleTime'];
		}*/
		this.spinner.show();
		/*this.notificationService.pushNotification(this.editForm.value, this.isNew)
			.subscribe((value) => {
				this.spinner.hide();
				if (value.Error) {
					swal({
						title: "推播失敗",
						type: "error",
					}).then((value) => {
						this.getNotifications();
					});
				} else {
					swal({
						title: "推播成功",
						type: "success",
					}).then((value) => {
						this.getNotifications();
					});
				}
			});*/
			
			this.creditLimitService.pushNotification(this.editForm.value, this.isNew)
			.subscribe((value) => {
				this.spinner.hide();
				if (value.Error) {
					swal({
						title: "推播失敗",
						type: "error",
					}).then((value) => {
						//this.getNotifications();
					});
				} else {
					swal({
						title: "推播成功",
						type: "success",
					}).then((value) => {
						//this.getNotifications();
					});
				}
			});
		this.active = false;
		this.comfirm = false;
	}
}
