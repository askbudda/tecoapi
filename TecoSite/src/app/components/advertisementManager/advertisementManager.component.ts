import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { Constant } from '../../../app/app.constant';
import { Advertisement } from '../../models/advertisement';
import { AdvertisementService } from '../../services/advertisementService';
import { NgxSpinnerService } from 'ngx-spinner';
import * as _ from "lodash";

@Component({
    selector: 'app-advertisement-manager',
    templateUrl: './advertisementManager.component.html',
    styleUrls: ['./advertisementManager.component.css']
})
export class AdvertisementManagerComponent implements OnInit {
    public gridState: State = {
        sort: [{ field: 'id', dir: 'desc' }],
        skip: 0,
        take: 10
    };
	public active = false;
	public imagesPreview = [];
    public files;
    private editedRowIndex: number;
    public editDataItem: Advertisement;
    public isNew: boolean;
	public advertisements: Advertisement[];
	
	public editForm: FormGroup;
	
    constructor(private advertisementService: AdvertisementService, private spinner: NgxSpinnerService) {
    }

    public ngOnInit(): void {
		console.log("ngOnInit");
		this.editForm = new FormGroup({
			'ID': new FormControl(),
			'imgFile': new FormControl(),
			'HiddenExcelFile': new FormControl('', this.uploadFileValidator()),
			'Image': new FormControl(),
			'Name': new FormControl(),
			'Type': new FormControl(),
			//'priority': new FormControl('', Validators.compose([Validators.min(1), Validators.required])),
			'Link': new FormControl(),
			//'linkType': new FormControl('', Validators.compose([Validators.min(1), Validators.max(4), Validators.required])),
			//'redirectType': new FormControl(),
			'Enable': new FormControl(),
			//'work': new FormControl(),
			//'hint': new FormControl(),
			//'open': new FormControl(),
			//'startDate': new FormControl('', Validators.required),
			//'endDate': new FormControl('', Validators.required),
			//'startTime': new FormControl(''),
			//'endTime': new FormControl(''),
		});
        this.getAdvertisement();
    }

    getAdvertisement(): void {
        this.advertisementService.getAdvertisement()
		    .subscribe(advertisements => {
				this.advertisements = advertisements;
				console.log(advertisements);
			});
		/*this.advertisements = [
			{
				//createTime: "2020-07-08T16:48:21.093",
				//endTime: "2021-07-31T00:00:00",
				ID: 1,
				Image: "https://qa.mfs01.costco.com.tw/CostcoImage/Advertisement/app_frozenfood.jpg",
				//imgFile: "",
				Name: "測試一",
				Type: "促銷",
				//linkType: 2,
				Link: "https://tinyurl.com/rvfkrhl",
				//priority: 1,
				//startTime: "2020-07-07T00:00:00",
				Enable: "1",
			},
			{
				//createTime: "2020-07-08T16:49:04.61",
				//endTime: "2021-07-31T00:00:00",
				ID: 2,
				Image: "https://qa.mfs01.costco.com.tw/CostcoImage/Advertisement/1584698602581_Children'sDayAPP375x214px.jpg",
				//imgFile: "",
				Name: "測試二",
				Type: "公告",
				//linkType: 2
				Link: "https://www.costco.com.tw/EventC",
				//priority: 2,
				//startTime: "2020-07-07T00:00:00",
				Enable: "1",
			},
		];*/
    }

    saveHandler(advertisement: Advertisement) {
        this.advertisementService.saveAdvertisement(advertisement, this.isNew)
            .subscribe(() => this.getAdvertisement());
        this.editDataItem = undefined;
    }

    addHandler() {
        /*if (_.filter(this.advertisements, function (adv) {
            return adv.state != Constant.AdvertisementStateType.STATE_CLOSE;
        }).length >= 8) {
            this.active = true;
        } else {
            this.editDataItem = new Advertisement();
		};*/
		
		this.editForm = new FormGroup({
			'ID': new FormControl(),
			'imgFile': new FormControl(),
			'HiddenExcelFile': new FormControl('', this.uploadFileValidator()),
			'Image': new FormControl(),
			'Name' :new FormControl(), 
			'Type': new FormControl(),
			//'priority': new FormControl('', Validators.compose([Validators.min(1), Validators.required])),
			'Link': new FormControl(),
			//'linkType': new FormControl('', Validators.compose([Validators.min(1), Validators.max(4), Validators.required])),
			//'redirectType': new FormControl(),
			'Enable': new FormControl(),
			//'work': new FormControl(),
			//'hint': new FormControl(),
			//'open': new FormControl(),
			//'startDate': new FormControl('', Validators.required),
			//'endDate': new FormControl('', Validators.required),
			//'startTime': new FormControl(''),
			//'endTime': new FormControl(''),
		});
		this.isNew = true;
		this.active = true;
    }

    editHandler({ dataItem }) {
		console.log("edit");
		console.log(dataItem);

		this.editForm = new FormGroup({
			'ID': new FormControl(),
			'imgFile': new FormControl(),
			'HiddenExcelFile': new FormControl(),
			'Image': new FormControl(),
			'Name' :new FormControl(), 
			'Type': new FormControl(),
			'Link': new FormControl(),
			'Enable': new FormControl(),
		});
        //this.editDataItem = dataItem;
		this.isNew = false;
		this.editForm.reset(dataItem);
		this.active = true;
    }

    cancelHandler() {
        this.editDataItem = undefined;
    }

    removeHandler({ dataItem }) {
        this.advertisementService.deleteAdvertisement(dataItem)
            .subscribe(() => this.getAdvertisement());
    }

    onStateChange(state: State) {
        this.gridState = state;
    }

    public uploadFileValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let forbidden = false;
            if (control.value) {
                forbidden = true;
            }
            return forbidden ? null : { 'error': 'need file' };
        };
    }

    onFileChange(event) {
        this.imagesPreview = [];
        if (event.target.files && event.target.files.length >= 1) {
			this.files = event.target.files;
			
			//console.log(this.files[0].name);
			//this.editForm.get('HiddenExcelFile').setValue(this.files[0].name);

			//this.editForm.value['excelFile'] = event.target.files;
            for (let file of event.target.files) {
                var reader = new FileReader();
                this.editForm.get('HiddenExcelFile').setValue(file.name);
                reader.readAsDataURL(file);
                reader.onload = (event: any) => {
                    this.imagesPreview.push(event.target.result);
                }
            }
        }
    }

    private closeForm(): void {
		this.active = false;
		this.imagesPreview = [];
    }

    public onSave(e): void {
		console.log("submit");
        this.active = false;
		//this.editDataItem = new Advertisement();
		this.editForm.value['imgFile'] = this.files;

		this.spinner.show();
		this.advertisementService.saveAdvertisement(this.editForm.value, this.isNew)
			.subscribe((value) => {
				this.spinner.hide();
				this.getAdvertisement();
				this.active = false;
				this.imagesPreview = [];
			});
    }

    public onCancel(e): void {
		this.active = false;
		this.imagesPreview = [];
    }

}
