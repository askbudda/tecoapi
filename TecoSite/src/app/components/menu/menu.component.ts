import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menuService';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
    public menus: any[];
    constructor(private menuService: MenuService) { }

    ngOnInit() {
        this.menus = [{
            id: 1,
            name: "一般推播通知",
            url: "/notification"
        },{
            id: 2,
            name: "客戶部門歸屬資料表上傳",
            url: "/customerData"
        },{
            id: 3,
            name: "地址對應倉庫資料表上傳",
            url: "/addressMap"
        },{
            id: 4,
            name: "商品清單上傳",
            url: "/productList"
        },{
            id: 5,
            name: "工作天資料上傳",
            url: "/workday"
        },{
            id: 6,
            name: "贈品設定",
            url: "/gift"
        },{
            id: 7,
            name: "下單信限限制設定",
            url: "/creditLimit"
        },{
            id: 8,
            name: "廣告設定",
            url: "/advertisement"
        },{
            id: 9,
            name: "訂單查詢",
            url: "/order"
        }]
        // this.menuService.getRefreshMenus().subscribe(menus => this.menus = menus);
    }

}
