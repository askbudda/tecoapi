export class CreditLimit {
    ID: number;
    Code: string;
    Name: string;
    Value: string;
    Enable: string;
}