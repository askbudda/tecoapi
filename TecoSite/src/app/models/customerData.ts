export class CustomerData {
    ID: number;
    GROUPID: string;
    CUSTOMERNAME: string;
    CUSTOMERID: string;
    GROUPCODE: string;
    GROUPTYPE: string;
    GROUPNAME: string;
    DEPARTMENTID: string;
    MANAGEREMAIL: string;
    MANAGERNAME: string;
}