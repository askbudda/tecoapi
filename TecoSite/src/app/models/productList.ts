export class ProductList {
    ID: number;
    TYPENAME: string;
    REFRIGERANT: string;
    POWER: string;
    PRODUCTCODE: string;
    PRODUCTSIZE: string;
    RELATEDPRODUCTCODE: string;
    RELATEDPRODUCTSIZE: string;
    LIQUIDPIPINGSIZE: string;
    AIRPIPINGSIZE: string;
    OUTDOORFEETSIZE: string;
    CUSTOMERTYPE: string;
    TYPE: string;
    GIVEAWAY: string;
}