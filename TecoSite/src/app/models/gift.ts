export class Gift {
    ID: number;
    PRODUCTCODE: string;
    PRODUCTNAME: string;
    PRICE: string;
    TYPE: string;
    START_TIME: Date;
    END_TIME: Date;
    //CREATE_TIME: Date;
    //CREATE_USER: string;
}