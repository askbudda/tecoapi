export class Notification {
    Id: number;
    Title: string;
    Message: string;
    CreateTime: Date;
    ScheduleTime: Date;
    PushTime: Date;
    FilePath: string;
    PushNum: number;
    Type: any;
    Range: any;
    Way: any;
    State: any;
}