import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { ButtonGroupModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { UploadModule } from '@progress/kendo-angular-upload';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export'
import { LayoutModule } from '@progress/kendo-angular-layout';

import { NotificationService } from './services/notificationService';
import { WorkdayService } from './services/workdayService';
import { AddressMapService } from './services/addressMapService';
import { OrderService } from './services/orderService';
import { CustomerDataService } from './services/customerDataService';
import { ProductListService } from './services/productListService';
import { GiftService } from './services/giftService';
import { CreditLimitService } from './services/creditLimitService';
import { AuthenticationService } from './services/authenticationService';
import { MenuService } from './services/menuService';
import { AdvertisementService } from './services/advertisementService';
import { AlertService } from './services/alertService';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { HomeComponent } from './components/home/home.component';
import { AccountManagerComponent } from './components/accountManager/accountManager.component';
import { NotificationManagerComponent } from './components/notificationManager/notificationManager.component';
import { AddressMapManagerComponent } from './components/addressMapManager/addressMapManager.component';
import { OrderManagerComponent } from './components/orderManager/orderManager.component';
import { CustomerDataManagerComponent } from './components/customerDataManager/customerDataManager.component';
import { ProductListManagerComponent } from './components/productListManager/productListManager.component';
import { WorkdayManagerComponent } from './components/workdayManager/workdayManager.component';
import { CreditLimitManagerComponent } from './components/creditLimitManager/creditLimitManager.component';
import { GiftManagerComponent } from './components/giftManager/giftManager.component';
import { AdvertisementManagerComponent } from './components/advertisementManager/advertisementManager.component';
import { AlertComponent } from './directives/alert/alert.component';
import { NgxSpinnerModule } from 'ngx-spinner';
 
import { Auth } from './helpers/auth';
import { JwtInterceptor } from './helpers/jwtInterceptor';
import { DatePipe } from './pipes/utilPipe';
import { NamePipe } from './pipes/utilPipe';
import { SeatPipe } from './pipes/utilPipe';
import { SafePipe } from './pipes/utilPipe';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        MenuComponent,
        HomeComponent,
        AccountManagerComponent,
        NotificationManagerComponent,
        AddressMapManagerComponent,
        OrderManagerComponent,
        CustomerDataManagerComponent,
        ProductListManagerComponent,
        WorkdayManagerComponent,
        CreditLimitManagerComponent,
        GiftManagerComponent,
        AdvertisementManagerComponent,
        DatePipe,
        NamePipe,
        SeatPipe,
        SafePipe,
        AlertComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        HttpModule,
        HttpClientModule,
        GridModule,
        ExcelModule,
        FormsModule,
        GridModule,
        ExcelModule,
        UploadModule,
        DialogModule,
        ButtonModule, 
        ButtonGroupModule,
        DateInputsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        LayoutModule,
        InputsModule,
        DropDownsModule,
        TooltipModule,
        ExcelExportModule,
        NgxSpinnerModule
    ],
    providers: [NotificationService, WorkdayService, AddressMapService, OrderService, CustomerDataService, ProductListService, GiftService, CreditLimitService, AdvertisementService, AuthenticationService, MenuService, Auth, JwtInterceptor, AlertService, {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
    }],
    bootstrap: [AppComponent]
})
export class AppModule { }
