import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from '../../app/app.constant';
import * as moment from 'moment';
import * as _ from "lodash";

@Pipe({ name: 'dateFilter' })
export class DatePipe implements PipeTransform {
    transform(value: any, format: string): string {
        return value ? moment(value).format(format) : '';
    }
}

@Pipe({ name: 'nameFilter' })
export class NamePipe implements PipeTransform {
    transform(value: any): string {
        var newName = '';
        for (var i = 0; i < value.length; i++) {
            if (i == 4 || i == 5 || i == 6) {
                newName += "*";
            } else {
                newName += value[i];
            }
        }
        return newName;
    }
}

@Pipe({ name: 'seatFilter' })
export class SeatPipe implements PipeTransform {
    transform(value: any): string {
        let result = '';
        if (!value) {
            return result;
        }

        var seatArr = value.seat.split('||');
        for (var key in seatArr) {
            var seat = seatArr[key];
            if (!seat) continue;

            result += seat + " ";
        }

        return result;
    }
}

@Pipe({ name: 'safeUrl' })
export class SafePipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }
    transform(url: any) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}