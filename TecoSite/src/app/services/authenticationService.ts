import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';

import { Constant } from '../app.constant';
import { Login } from '../models/login';
import { MenuService } from './menuService';
import swal from 'sweetalert2';
@Injectable()
export class AuthenticationService {
    private authApiUrl = Constant.TECO_API + 'jwt/';
    constructor(
        private http: HttpClient,
        private menuService: MenuService) { }

    loginData(): Observable<any> {
        return this.http.get<Login>(this.authApiUrl)
            .pipe(catchError(this.handleError('getLogin', [])));
    }

    login(username: string, password: string) {
        return this.http.post<any>(this.authApiUrl + 'login', { username: username, password: password })
            .pipe(catchError(this.handleError('login', [])));
    };

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('loginData');
        localStorage.removeItem('ipAddress');
        this.menuService.refreshMenus([]);
    };

    private handleError<T>(operation, result?: T) {
        return (error: any): Observable<T> => {
            console.log(error);
            swal({
                type: 'error',
                title: '錯誤',
                text: "系統忙碌中"
            });
            return Observable.throw(error.error);
        };
    };
}
