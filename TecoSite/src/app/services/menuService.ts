import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Constant } from '../../app/app.constant';

import { Menu } from '../models/menu';
import swal from 'sweetalert2';
@Injectable()
export class MenuService {
    private menuApiUrl = Constant.TECO_API + 'menu/';
    private subject = new Subject<any>();
    constructor(private http: HttpClient) { }

    getMenus(): Observable<Menu[]> {
        return this.http.get<Menu[]>(this.menuApiUrl)
            .pipe(catchError(this.handleError('getMenu', [])));
    }

    refreshMenus(menus: any) {
        this.subject.next(menus);
    }

    getRefreshMenus() {
        return this.subject.asObservable();
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            swal({
                type: 'error',
                title: '錯誤',
                text: "系統忙碌中"
            });
            return of(result as T);
        };
    }
}
