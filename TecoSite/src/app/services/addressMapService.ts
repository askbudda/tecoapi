import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Constant } from '../../app/app.constant';
import { AddressMap } from '../models/addressMap';
import swal from 'sweetalert2';
import * as moment from 'moment';

@Injectable()
export class AddressMapService {
    private addressMapApiUrl = Constant.TECO_API + 'WareHouse/';

    constructor(private http: HttpClient) { }

    getNotifications(): Observable<AddressMap[]> {
        return this.http.get<AddressMap[]>(this.addressMapApiUrl + "Query")
            .pipe(catchError(this.handleError('Query', [])));
    }

    pushNotification(data: any, isNew?: boolean): Observable<any> {
        if (!isNew) {
            return this.http.put(this.addressMapApiUrl + "Import", data)
                .pipe(catchError(this.handleError('Import', [])));
        } else {
            if (data.ScheduleTime) {
               data.ScheduleTime = moment(data.ScheduleTime).format();
            }
            const formData: FormData = new FormData();
            formData.append("InfoData", JSON.stringify(data));
            if (data.excelFile) {
                for (let i = 0; i < data.excelFile.length; i++) {
                    formData.append(data.excelFile[i].name, data.excelFile[i]);
                }
            }
            return this.http.post(this.addressMapApiUrl + "Import", formData)
                .pipe(catchError(this.handleError('Import', [])));
        }
    }


    /*deleteNotification(notification: Notification | number): Observable<any> {
        const id = typeof notification === 'number' ? notification : notification.Id;
        return this.http.delete(this.notificationApiUrl + id)
            .pipe(catchError(this.handleError('deleteNotification', [])));
    }*/

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            swal({
                type: 'error',
                title: '錯誤',
                text: "系統忙碌中"
            });
            return of(result as T);
        };
    }
}
