import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Constant } from '../../app/app.constant';

import { Advertisement } from '../models/advertisement';

@Injectable()
export class AdvertisementService {
    private advertisementApiUrl = Constant.TECO_API + 'Advertisement/';

    constructor(private http: HttpClient) { }

    getAdvertisement(): Observable<Advertisement[]> {
        return this.http.get<Advertisement[]>(this.advertisementApiUrl + 'Query')
            .pipe(catchError(this.handleError('Query', [])));
    }

    saveAdvertisement(data: any, isNew?: boolean): Observable<any> {
        const formData: FormData = new FormData();
        if (isNew) {
            delete data['ID'];
            //delete data['Image'];
        }
        formData.append("InfoData", JSON.stringify(data));

        console.log(data);
        if (data.imgFile) {
            for (let i = 0; i < data.imgFile.length; i++) {
                console.log(data.imgFile[i].name);
                formData.append(data.imgFile[i].name, data.imgFile[i]);
            }
        }
        console.log(formData);
        if (isNew) {
            return this.http.post(this.advertisementApiUrl + 'Add', formData)
                .pipe(catchError(this.handleError('Add', [])));
        } else {
            return this.http.put(this.advertisementApiUrl + 'Modify', formData)
                .pipe(catchError(this.handleError('Modify', [])));
        }
    }

    deleteAdvertisement(advertisement: Advertisement | number): Observable<any> {
        const id = typeof advertisement === 'number' ? advertisement : advertisement.ID;
        return this.http.delete(this.advertisementApiUrl + 'Delete/' + id)
            .pipe(catchError(this.handleError('Delete', [])));
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
}
